<%-- 
    Document   : test
    Created on : Feb 24, 2023, 7:15:16 PM
    Author     : nguye
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                 font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 800px;*/
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
            .logo{
                margin-left:-10px;
                padding-top:35px;
                padding-bottom:40px;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                .noti-icon{
                    display:flex;
                    margin-left:1100px;
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                .report{
                    
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                   background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
                .report button{
                    margin-top:30px;
                    margin-left:1200px;
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                     /*width: 400px;*/
                }
                .main{
                    margin-left: 300px;
                    height:1080px;
                }
           
                .container-one{
                    /*display:flex;*/
                     border-radius: 5px;
                    height: 450px;
                    width:1400px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                      margin:-40px 30px ;
                }
                .container-one button{
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                     padding: 15px;
                     border:none;
                     color:white;
                     border-radius:5px;
                     float:right;
                     margin: 20px;
                     width:100px;
                }
                .co-right{
                    border-radius: 5px;
                    height: 640px;
                    width:450px;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 0px ;
                }
                .co-left{
                    /*padding:50px;*/
                    display:flex;
                   
                }
                .product{
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  20px;
                    padding: 0;
                    display: block;
                    background-color: white;
                    height:300px;
                    width:200px;
                    margin-right:15px;
                    display:block;
                    justify-content: center;
                    text-align: center;
                }
                #product-list{
                 display: flex;
                    flex-wrap: wrap;
                     height: auto;
                    background-color: #F5F5F5;
                    width:94%;
                    border-radius: 5px;
                    left:0px;
/*                    left
                    */margin:-40px 30px ;
                    padding:20px;
                    /*position: relative;*/
                    display:flex;
                }
                #product-list img{
                    height:200px;
                    width:200px;
                }
                .co-right button{
                    width:92%;
                    height:50px;
                    margin:10px;
                    border-radius: 50px;
                    border:none;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    color:white;
                    font-weight: bold;
                    font-size: 20px;
                }
                #product-img{
                    height:350px;
                }
                .info{
                    margin:100px 0 0 50px;
                }
                  button:hover{
                     cursor: pointer;
                 }
        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">List</a>
                    </li>
                  
                  
                </ul>

            </div>
            
            
            
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="products.jsp">Products</a>
                </div>                
                <ul>
                    <li><a class="active" href="products.jsp">Products</a></li>
                    <!--<li><a class="active" href="productsDetail.jsp">Product Detail</a></li>-->
            
                    
                </ul>
            </div>
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Edittor</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>
                  <input type="text" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                      <div class="link">
                        <h1>Product</h1>
                        <p> Chi tiết sản phẩm</p>
                      </div>
                    <div>
                        <a href="#"><button type="button">Create Report</button></a>
                    </div>
                    </div>
                   </div>
                   
                    <div class="container-one">
                        <!--//Chèn thông tin hiển thị-->
                    <div class="co-left">
                        <img id="product-img" src=""/>
                        
                        <div class="info">
                             <h2 id="product-name"></h2>
                             <p id="product-description"></p>
                        </div>
                        
                    </div>
                   <button>Delete</button>
                         <button>Edit</button>
                </div>            
            </div>
        </div> 
          <script src="javascript_admin/productDetail.js"></script>
    </body>
</html>
