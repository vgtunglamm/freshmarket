<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Giới thiệu về G10 Market</title>
    <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png">
    <!--reset css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
    <link rel="stylesheet" href="./assests/css/grid.css">
    <link rel="stylesheet" href="./assests/css/base.css">
    <link rel="stylesheet" href="./assests/css/main.css">
    <link rel="stylesheet" href="./assests/css/responsive.css">
    <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css">
    <link rel="stylesheet" href="./introduce_page/introduce.css">
    <link rel="stylesheet" href="./recruit_page/recruit.css">
    <link rel="stylesheet" href="./contact_page/contact.css">
    <link rel="stylesheet" href="./header/header.css">
    <link rel="stylesheet" href="./footer/footer.css">
    <link rel="stylesheet" href="./modal/modal.css" />
    <link rel="stylesheet" href="./chatbox/chatbox.css">
    <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/recruit_responsive.css">
    <!--Nhúng font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
        rel="stylesheet">
</head>

<body>
    <div class="app">
        <c:if test="${sessionScope.role != null && sessionScope.role != 'admin'}">
            <header class="header" id="header_user">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>
        <c:if test="${sessionScope.role == 'admin'}">
            <header class="header" id="header_admin">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>
        <c:if test="${sessionScope.role == null}">
            <header class="header" id="header">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>

        <!-- category mobile -->
        <div class="menu_mobile" id="menu_mobile">
            <div class="menu_close">
                <i class="menu_close-icon fa-solid fa-xmark" onclick="closeMenuMobile()"></i>
            </div>

            <div class="header__search-mobile">
                <div class="header__search-input-wrap">
                    <input type="text" class="header__search-input" placeholder="Tìm kiếm trên G10 Market">
                </div>

                <button class="header__search-btn">
                    <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
                </button>
            </div>

            <nav class="category_mobile">
                <div class="category__heading">
                    <h3 class="category__heading-tittle">DANH MỤC</h3>
                </div>
                <ul class="category__list">
                    <c:forEach items="${categories}" var="cate">
                        <li class="category__list-tittle">
                            <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
                        </li>
                    </c:forEach>
                </ul>
            </nav>
        </div>

        <div class="recruit_container">
            <div class="grid wide">
                <div class="row recruit_row">
                    <div class="col l-9 m-12 c-12">
                        <div class="contact_nav">
                            <a href="./" class="back_homepage-nav" style="font-size: 2.3rem;">Trang chủ</a>
                            <h3 class="contact_seperate" style="font-size: 2.3rem;">/</h3>
                            <a href="" class="contact_page-nav" style="font-size: 2.3rem;">Tuyển dụng</a>
                        </div>

                        <h3 class="recruit_tittle">Danh sách công việc đang tuyển dụng (9)</h3>
                    </div>

                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Nhà phân tích dữ liệu</a>
                            </div>
                            <p class="job_descr">Tư vấn, tạo và duy trì trang tổng quan, phân tích báo cáo cho Ban Giám
                                đốc và bộ phận kinh doanh chủ chốt ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Chuyên gia nghiên cứu thị trường</a>
                            </div>
                            <p class="job_descr">- Nghiên cứu nội bộ hàng tuần và hàng tháng: Tìm hiểu kỹ nhu cầu kinh
                                doanh của khách hàng và đề xuất ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Giám đốc thương hiệu</a>
                            </div>
                            <p class="job_descr">- Hỗ trợ Giám đốc Thương hiệu MKTG xây dựng các chiến lược thương hiệu
                                (chân dung khách hàng, định vị thương hiệu, ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Trưởng nhóm kỹ thuật</a>
                            </div>
                            <p class="job_descr">Làm việc với các thành viên khác trong nhóm (kỹ sư, người kiểm tra, nhà
                                thiết kế, PO) để thiết kế, xây dựng các ứng dụng mới ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Phát triển phần mềm (Golang)</a>
                            </div>
                            <p class="job_descr">Bạn là ai Hứng thú tìm hiểu và phát triển công nghệ mới (Chưa có kinh
                                nghiệm?) Luôn cập nhật ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Data Scientist</a>
                            </div>
                            <p class="job_descr">Chúng tôi hiện đang tìm kiếm các Nhà khoa học Dữ liệu để giúp G10
                                Market cải thiện hệ thống đề xuất của mình ....</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">System Engineer (DevOps)</a>
                            </div>
                            <p class="job_descr">Dịch vụ triển khai, vận hành, bảo trì và điều chỉnh hệ thống. Thiết kế
                                và triển khai hệ thống giám sát và phân tích...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Nhà phát triển di động (Flutter)</a>
                            </div>
                            <p class="job_descr">Duy trì và xây dựng các chức năng mới cho ứng dụng di động G10 Market
                                Cộng tác với các chức năng chéo bên trong ...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col l-4 m-6 c-12">
                        <div class="job_table">
                            <div class="job_tittle">
                                <i class="job_icon fa-solid fa-tag"></i>
                                <a href="" class="job_tittle-link">Frontend Developer (ReactJS)</a>
                            </div>
                            <p class="job_descr">Phát triển các tính năng hướng tới người dùng mới bằng cách sử dụng
                                Javascript và React.js. Xây dựng các thành phần có thể tái sử dụng...</p>
                            <div class="job_time-wrap">
                                <p class="job_time-title">Full-time</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="seperate_field"></div>
            </div>
        </div>

        <footer class="footer" id="footer">
            <!-- được render từ file footer.js  -->
        </footer>
    </div>

    <!-- modal  -->
    <div id="modal_wrap">
        <!-- render từ file modal.js  -->
    </div>
    <!-- chat message  -->
    <div id="chat-box">
        <!-- render từ file chatbox.js  -->
    </div>

    <!-- render header -->
    <script src="./header/header.js"></script>
    <script src="./header/header_admin.js"></script>
    <script src="header/header_user.js"></script>
    <!-- render toast message  -->
    <script src="./toast_messages/toast.js"></script>
    <!-- render modal  -->
    <script src="./modal/modal.js"></script>
    <!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
    <script src="./javascript/logIn_and_Signup.js"></script>
    <!-- import validator đăng ký, đăng nhập  -->
    <script src="./javascript/validator.js"></script>
    <!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
    <script>
        const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
        const headerMenu = document.querySelector('.header_menu');
        var emailReg;
        var passwordReg;
        var emailLog;
        var passwordLog;
        // Đăng ký 
        Validator({
            form: "#form-1",
            formGroupSelector: ".form-group",
            errorSelector: ".form-message",
            rules: [
                // Validator.isRequired('#fullname'),
                Validator.isRequired("#email"),
                Validator.isEmail("#email"),
                Validator.minLength("#password", 6),
                Validator.isRequired("#password_confirmation"),
                Validator.isConfirmed(
                    "#password_confirmation",
                    function () {
                        return document.querySelector("#form-1 #password").value;
                    },
                    "Mật khẩu nhập lại không khớp"
                ),
            ],
            onSubmit: async function (evt) {

                // console.log(evt);
                emailReg = evt.email;
                passwordReg = evt.password;

                let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
                const response = await fetch(url);
                const res = await response.json();

                if (res != "0") {
                    showSuccessSignUpExistEmailToast();
                    logModal.classList.add('open');
                    regModal.classList.remove('open');
                } else {
                    const data = {
                        email: emailReg,
                        passWord: passwordReg
                    };
                    const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data)
                    });
                    const res = await response.json();
                    console.log(response);
                    console.log(res);
                    if (res == "1") {
                        showSuccessSignUpToast();
                        logModal.classList.add('open');
                        regModal.classList.remove('open');
                        // window.location.reload();
                    } else {
                        showFaiedSignUpToast();
                        logModal.classList.add('open');
                        regModal.classList.remove('open');
                    }
                }
            },
        });
        //Đăng nhập
        Validator({
            form: "#form-2",
            formGroupSelector: ".form-group",
            errorSelector: ".form-message",
            rules: [
                Validator.isRequired("#email"),
                Validator.isEmail("#email"),
                Validator.minLength("#password", 6),
            ],
            onSubmit: function (data) {
                //Call api
                console.log(data);
                emailLog = data.email;
                passwordLog = data.password;

                if (emailReg === emailLog && passwordReg === passwordLog) {
                    showSuccessLogInToast();
                }
                else {
                    showErrorLogInToast();
                }
            },
        });
    </script>
    <!-- xử lý trượt slider  -->
    <script src="./javascript/slider.js"></script>
    <!-- Time sale countDown -->
    <script src="./javascript/countDownTime.js"></script>
    <!-- object product 1  -->
    <script src="./javascript/mainProduct.js"></script>
    <!-- Đóng mở chat hỗ trợ trực tuyến -->
    <script>
        function openChat() {
            document.getElementById("myChat").style.display = "block";
        }
        function closeChat() {
            document.getElementById("myChat").style.display = "none";
        }
    </script>
    <!-- Đóng mở menu mobile -->
    <script>
        function openMenuMobile() {
            document.getElementById("menu_mobile").style.display = "block";
        }
        function closeMenuMobile() {
            document.getElementById("menu_mobile").style.display = "none";
        }
    </script>
    <!-- render chatbox  -->
    <script src="./chatbox/chatbox.js"></script>
    <!-- render footer -->
    <script src="./footer/footer.js"></script>
</body>

</html>