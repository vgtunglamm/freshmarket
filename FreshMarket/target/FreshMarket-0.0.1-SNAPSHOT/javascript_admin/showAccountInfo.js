/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function showAccountInfo(row) {

    var idUser = document.getElementById('id');
    var nameUser = document.getElementById('name');
    var emailUser = document.getElementById('email');
    var roleUser = document.getElementById('role');
    var cells = row.getElementsByTagName('td');
    var rowData = [];
    for (var i = 0; i < cells.length; i++) {
        rowData.push(cells[i].innerHTML);
    }
    idUser.value = rowData[0];
    nameUser.value = rowData[1];
    emailUser.value = rowData[2];
    roleUser.value = rowData[3];
    var overlay = document.getElementById("overlay");
    var formContainer = document.getElementById("formContainer");
    overlay.style.display = "block";
    formContainer.style.display = "block";


}

 function hideForm() {
      var overlay = document.getElementById("overlay");
      var formContainer = document.getElementById("formContainer");
      overlay.style.display = "none";
      formContainer.style.display = "none";
    }