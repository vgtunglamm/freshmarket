<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
         <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                 font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                width:98%;
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
            .logo{
                margin-left:10px;
                padding-top:18px;
                padding-bottom:28px;
                color:#80BB35;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  width:100%;
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                .noti-icon{
                    display:flex;
                     position: absolute;
                    right:10px;
                    /*margin-left:700px;*/
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                .report{
                    width:101%;
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
                .report button{
                    margin-top:30px;
                     position: absolute;
                    right:60px;
                    /*margin-left:850px;*/
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 300px;
                    height:1080px;
                }
                .container-one{
                   background-color:#ccc;
                    top:0;
                  margin-top: -45px;
                  margin-left: 2%;
                  border-radius: 10px;
                  width:96%;
                  
                }
                .product{
                    
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối 
                   list-style-type: none;
                    margin-bottom:  10px;
                    border-radius: 10px;
                    padding: 0;
                    display: flex;;
                    background-color: white;
                    height:80px;
                    /*width:100px;*/
                    margin-right:20px;
              
                }
                .user-list{
                     margin-top: 20px;
                    background-color: #F5F5F5;
                    width:1490px;
                    border-radius: 5px;
                    left:0px;
                    margin:-40px 30px ;
                    /*padding-left:  50px;*/
                    padding:20px;
                    height: 400px;
                   
                }
                .user-info{
                    width: 800px;
                }
                
          
                .title{
                    font-weight: bold;
                    font-size:14px;
                    display:flex;
                    background-color: #F5F5F5;
                    width:1440px;
                    border-radius: 5px;
                    left:0px;
                    
                    margin:-40px 30px 20px 30px ;
                    /*margin-right: 300px;*/
                    padding:10px 50px 20px 50px;
                }
                .title p{
                      width: 23.1%;
                    /*padding-right: 250px;*/
                }

                td{
                    text-align: center;
                    /*margin-left: -20px;*/
/*                    display:block;
                    justify-content: center;
                    align-items: center;*/
                    width: 400px;
                    padding-top:10px;
                    height:30px;
                   
                    background-color: white;
                }
                tr{
                    /*width:1450px*/
                    padding-right:100px;
                }
/*                .phone{
                    margin-left: -40px;
                }*/
                .phonem{
                    /*padding-right: 100px;*/
                    margin-left: -60px;
                 }
                 .name{
                     padding-left: 60px;
                 }
                 .email{
                     padding-left: 45px;
                 }
                 tr:hover{
                     /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
                     color: linear-gradient(0, #80BB35, #80BB35);;
                     cursor: pointer;
                }
                 tr td button{
                     padding:10px;
                     color:white;
                    border:none;
                     background-image: linear-gradient(0, #80BB35, #80BB35);
                     cursor: pointer;
                 }
/*                  .hidden{
                     display:none;
                 }*/
                  .info{
                     transition:all 0.1s linear;
                     display:none;
                 }
/*                  #overlay{
                     position: fixed;
                     z-index:1;
                     display:none;
                     top:0;
                     left:0;
                     bottom:0;
                     right:0;
                     background-color: rgba(0,0,0,0.3);
                 }*/
                .select{
                    display: flex;
                    justify-content: right;
                    align-items: center;
                    padding: -20px;
                    padding-bottom: 10px;
                }

                .select button{
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    padding: 10px;
                    border:none;
                    color:white;
                    border-radius:12px;
                    margin-right: 30px;
                    width: 70px;
                    height: 45px;
                }

                .done-button:hover{
                    /*padding:20px;*/
                    /*display:block;*/
                    cursor: pointer;
                }

                button:hover{
                    cursor: pointer;
                }



         
                 @media only screen and (min-width: 580px){
                    /* Thêm các quy tắc CSS của bạn ở đây */
                    #user-list{
                        color:red;
                    }
                    
                    
                  }
                  
                  .highlight{
                       padding-left: 8px;
                       padding-right: 8px;
                  }
                .highlight td:hover {
                    cursor: pointer;
                    background-color: #80BB35;
                }
                thead tr{
                    height:40px;
                    font-weight: bold;
                }
                
              .overlay {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }

    /* CSS cho form */
    #formContainer {
      position: fixed;
      height:450px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
    
             .overlay1 {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }

    /* CSS cho form */
    #formContainer1 {
      position: fixed;
      height:450px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
 
    /* CSS cho nút đóng */
    #closeButton {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input{
        display:flex;
    }
  
    .form-left{
        width:500px;
    }
    .form-right{
        width:500px;
    }
    .title-user{
        display:flex;
    }
    .ctr-input input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    .ctr-button{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button button{
         padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px; 
        height: 40px;
        margin:5px;
    }
    .ctr-button input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
    .manager-user{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    
    /*CSS cho add account*/
    #closeButton1 {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input1{
        display:flex;
    }
  
    .form-left1{
        width:500px;
    }
    .form-right1{
        width:500px;
    }
    .title-user1{
        display:flex;
    }
    .ctr-input1 input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    .ctr-button1{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button1 input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
    .ctr-button1 input:hover{
        cursor: pointer;
    }
    
     .ctr-button1 button{
           padding: 5px;
    height: 36px;
    border: 2px solid red;
    color: red;
    /* margin-top: 10px; */
    margin-right: 60px;
    width: 100px;
    border-radius: 10px;
        
    }
    .ctr-button1 button:hover{
        border:none;
    }
    .manager-user1{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    } 
    li a:hover{
        color:#80BB35;
        font-size:20px;
        transition: 0.3s;
        /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
        /*color: white;*/
        
    }
    span{
        color:red;
        padding-left: 20px;
    }
    li a:hover{
            transition: all 0.5s ease;
            display: flex;
            padding: 12px 12px;
            font-weight: bold;
            font-size: 18px;
            color: #80bb35;
            background-color: rgba(0, 0, 0, 0.05);
            border-left: 3px solid #80bb35;
    }
     
 
        </style>
    </head>
    <body>
         <div class="menu">
            <a href="index.jsp">
                <div class="logo">
<!--                    <img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>-->
                        <h2>G10 MARKET</h2>
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Users</a>
                    </li>
                  
                </ul>

            </div>
            
            
            
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="#">Categories</a>
                </div>                
                <ul>
                    <c:forEach items="${categories}" var="cate">
                        <li><a class="active" href="products.jsp?cateId=${cate.id}">${cate.name}</a></li>
                    </c:forEach>
                </ul>
            </div>
             
            
             <div class="brands">
                <div class="b-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="listBrand.jsp">Brand</a>
                </div>                
              
            </div>
             
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                     <li>
                        <a class="active" href="wallet_admin.jsp">Wallet</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>
                  <input type="text" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                        <h1>Account</h1>
                        <p> Tất cả Account </p>
                        </div>
                    <div>
                        <!--button thêm User-->
                        <button onclick="addAccount()" type="button">Add Account</button>
                    </div>
                      
                      <!--box add account-->
                        <div id="overlay1" class="overlay1"></div>

                            <div id="formContainer1">
                               <div class="title-user1">
                                    <h2>Add Customer</h2>
                                    <span id="closeButton1" onclick="hideForm1()">&#x2716;</span>
                                   
                                </div>
                                <hr>
                              <form class="manager-user1" >
                                  <div class="ctr-input1">
                                      <div class="form-left1">
                                         <label for="name1">Username</label>
                                        
                                        <input type="text" id="name1" name="name">
                                        <br>
                                        <span id="notiUser"></span>
                                        <br><br>
                                     
                                        <label for="email1">Email</label>
                                          
                                        <input type="email" id="email1" name="email"><br>
                                         <span id="notiEmail"></span>
                                         <br><br>
                                       </div>
                                   <div class="form-right1">

                                        <label for="pass1">Password</label>
                                          
                                        <input type="password" id="pass1" name="pass">
                                        <br> 
                                         <span id="notiPw"></span>
                                        <br><br>

                                       <label for="cfpass1">Confirm Password</label>

                                       <input type="password" id="cfpass1" name="pass">
                                       <br> 
                                         <span id="notiCfPw"></span>
                                       <br><br>

                                         <label for="role1">Role</label>
                                          
                                        <input type="text" id="role1" name="role">
                                         <br>
                                        <span id="notiRole"></span>
                                        <br><br>
                                  </div>
                                  </div>
                                  <div class="ctr-button1">
                                      <button onclick="validateForm()" type="button">Add</button>
                                      <input onclick="hideForm1()" type="submit" value="Cancel">
                                  </div>

                                 
                              </form>
                            </div>
                        
                        
                        
                    </div>
                   </div>

                <div class="container-one">
                    <%--  render tài khoản  --%>
                    <table class="highlight">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Username</th>
                              <th>Email</th>
                              <th>Role</th>
                          </tr>
                      </thead>

                      <tbody id="user-list"></tbody>

                      </table>
                    
                       <div id="overlay" class="overlay"></div>

                            <div id="formContainer">
                                <div class="title-user">
                                    <h2>Customer Detail</h2>
                                    <span id="closeButton" onclick="hideForm()">&#x2716;</span>
                                   
                                </div>
                                <hr>
                              <form class="manager-user">
                                  <div class="ctr-input">
                                      <div class="form-left">
                                       <label for="id">ID</label>
                                        <input type="text" id="id" name="id" readonly><br><br>

                                         <label for="name">Username</label>
                                        <input type="text" id="name" name="name"><br><br>

                                        <label for="email">Email</label>
                                        <input type="email" id="email" name="email"><br><br>
                                      </div>
                                   <div class="form-right">
                                         <label for="role">Role</label>
                                        <input type="text" id="role" name="role"><br><br>
                                  </div>
                                  </div>
                                  <div class="ctr-button">
                                      <button onclick="callUpdateAPI()" type="button">Update</button>
                                      <button onclick="callDeleteAPI()" type="button">Delete</button>
                                      <button onclick="hideForm()" type="submit" >Cancel</button>
                                  </div>
                              </form>
                            </div>

                </div>
                
            </div>
        </div>
                    <script src="./javascript_admin/showAccountInfo.js"></script>
                <script src="./javascript_admin/addAccount.js"></script>
                <script src="./javascript_admin/validationAddAccount.js"></script>
    </body>
</html>

<script>
    let url = "https://localhost:8080/FreshMarket/api/users/get-all" ;
    fetch(url, {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        })
        .then(response => response.json())
        .then(data => {
            let userList = document.getElementById("user-list");
            data.forEach(user => {
                let id = user.id;
                let name = user.userName;
                let email = user.email;
                let role = user.role;

                let row = document.createElement("tr");
                row.onclick = function () {
                    showAccountInfo(this);
                };

                let idCell = document.createElement("td");
                idCell.innerText = id;
                row.appendChild(idCell);

                let nameCell = document.createElement("td");
                nameCell.innerText = name;
                row.appendChild(nameCell);

                let emailCell = document.createElement("td");
                emailCell.innerText = email;
                row.appendChild(emailCell);

                let roleCell = document.createElement("td");
                roleCell.innerText = role;
                row.appendChild(roleCell);

                userList.appendChild(row);
            });
        })
        .catch(error => {
            console.error(error);
        });
</script>

<script>
    async function callUpdateAPI(event) {
        // Get the form input values
        let id = document.getElementById("id").value;
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let role = document.getElementById("role").value;
        const updatedData = {
            id: id,
            userName: name,
            email: email,
            role: role
        };
        const response = await fetch('https://localhost:8080/FreshMarket/api/users/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(updatedData)
        });
        const res = await response.json();
        if (res == "1") {
            alert("Update successful");
            location.reload();
        } else {
            alert("Update failed");
        }
    }
    async function callDeleteAPI(event) {

        // Get the form input values
        let id = document.getElementById("id").value;

        const response = await fetch('https://localhost:8080/FreshMarket/api/users/delete/?id=' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await response.json();
        if (res == "1") {
            alert("Delete successful");
            location.reload();
        } else {
            alert("Delete failed");
        }
    }
    async  function callCreateAPI(){
        let name = document.getElementById("name1").value;
        let email = document.getElementById("email1").value;
        let password = document.getElementById("pass1").value;
        let cfpassword = document.getElementById("cfpass1").value;
        let role = document.getElementById("role1").value;

        let url = "https://localhost:8080/FreshMarket/api/user/?email=" + email;
        const response = await fetch(url);
        const res = await response.json();

        if(cfpassword.toString() != password.toString()){
            alert("the password does not match!!");
        }
        else{
            if (res != "0") {
                alert("Email has been existed!!");
            } else {
                const data = {
                    email: email,
                    passWord: password,
                    userName: name,
                    role: role
                };
                const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    },
                    body: JSON.stringify(data)
                });
                const res = await response.json();
                if (res == "1") {
                    alert("Create successful!!");
                    location.reload();
                } else {
                    alert("Create failed!!");
                }
            }
        }
    }
</script>
