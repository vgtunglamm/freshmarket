<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>G10 MARKET</title>
    <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png" />
    <!--reset css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
    <link rel="stylesheet" href="./assests/css/grid.css" />
    <link rel="stylesheet" href="./assests/css/base.css" />
    <link rel="stylesheet" href="./assests/css/main.css" />
    <link rel="stylesheet" href="./assests/css/responsive.css" />
    <link rel="stylesheet" href="./modal/modal.css" />
    <link rel="stylesheet" href="./header/header.css">
    <link rel="stylesheet" href="./footer/footer.css">
    <link rel="stylesheet" href="./chatbox/chatbox.css">
    <link rel="stylesheet" href="./toast_messages/toast.css">
    <link rel="stylesheet" href="./contact_page/contact.css">
    <link rel="stylesheet" href="./assests/css/policy.css">
    <link rel="stylesheet" href="./assests/css/help_center.css">
    <link rel="stylesheet" href="./assests/css/news.css">
    <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/news_responsive.css">
    <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css" />
    <!--Nhúng font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
        rel="stylesheet" />
</head>

<body>
    <div class="app">
        <c:if test="${sessionScope.role != null && sessionScope.role != 'admin'}">
            <header class="header" id="header_user">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>
        <c:if test="${sessionScope.role == 'admin'}">
            <header class="header" id="header_admin">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>
        <c:if test="${sessionScope.role == null}">
            <header class="header" id="header">
                <!-- được render từ file header.js  -->
            </header>
        </c:if>

        <!-- category mobile -->
        <div class="menu_mobile" id="menu_mobile">
            <div class="menu_close">
                <i class="menu_close-icon fa-solid fa-xmark" onclick="closeMenuMobile()"></i>
            </div>

            <div class="header__search-mobile">
                <div class="header__search-input-wrap">
                    <input type="text" class="header__search-input" placeholder="Tìm kiếm trên G10 Market">
                </div>

                <button class="header__search-btn">
                    <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
                </button>
            </div>

            <nav class="category_mobile">
                <div class="category__heading">
                    <h3 class="category__heading-tittle">DANH MỤC</h3>
                </div>
                <ul class="category__list">
                    <c:forEach items="${categories}" var="cate">
                        <li class="category__list-tittle">
                            <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
                        </li>
                    </c:forEach>
                </ul>
            </nav>
        </div>

        <div class="app_container" style="background-color: #fff;">
            <!-- Ngăn cách với header  -->
            <div style="padding-top: 15px; background-color: #fff"></div>
            <div class="grid wide">
                <div class="col l-12 m-12 c-12">
                    <div class="contact_nav">
                        <a href="./" class="back_homepage-nav news_nav">Trang chủ</a>
                        <h3 class="contact_seperate" style="font-size: 2.3rem; margin: 0 8px 26px 8px;">/</h3>
                        <a href="" class="contact_page-nav" style="font-size: 2.3rem; margin: 0 0 26px 0;">Tin tức</a>
                    </div>
                </div>

                <div class="row news_rose">
                    <!-- tin nổi bật  -->
                    <div class="col l-4 m-12 c-12">
                        <nav class="news_category">
                            <div class="news_category__heading">
                                <h3 class="news_category__heading-tittle">TIN NỔI BẬT</h3>
                            </div>
                            <ul class="news_category__list">
                                <li class="news_category__list-item">
                                    <img src="./assests/img/news/1.jpg" alt="" class="news_category__list-item-img">
                                    <div class="news_category__list-item-wrap">
                                        <p class="news_category__list-item-title">Tự chế món thạch sữa chua thanh long
                                            lung linh sắc màu</p>
                                        <p class="news_category__list-item-date">08/10/2022</p>
                                    </div>
                                </li>
                                <li class="news_category__list-item">
                                    <img src="./assests/img/news/3.jpg" alt="" class="news_category__list-item-img">
                                    <div class="news_category__list-item-wrap">
                                        <p class="news_category__list-item-title">Vì sao hoa quả Việt thất thế trước
                                            “cơn lốc” hàng nhập ngoại?</p>
                                        <p class="news_category__list-item-date">11/10/2022</p>
                                    </div>
                                </li>
                                <li class="news_category__list-item">
                                    <img src="./assests/img/news/2.jpg" alt="" class="news_category__list-item-img">
                                    <div class="news_category__list-item-wrap">
                                        <p class="news_category__list-item-title">Kỹ thuật trồng rau sạch trong chậu xốp
                                            tại nhà đơn giản</p>
                                        <p class="news_category__list-item-date">12/10/2022</p>
                                    </div>
                                </li>
                                <li class="news_category__list-item">
                                    <img src="./assests/img/news/4.webp" alt="" class="news_category__list-item-img">
                                    <div class="news_category__list-item-wrap">
                                        <p class="news_category__list-item-title">Những loại trái cây Nhật đắt như vàng
                                            ròng đổ bộ về Việt Nam</p>
                                        <p class="news_category__list-item-date">14/10/2022</p>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- tin tức  -->
                    <div class="col l-8 m-12 c-12">
                        <div class="news_title">TIN TỨC</div>
                        <ul class="news_list">
                            <li class="news_list-item">
                                <img src="./assests/img/news/1.jpg" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Tự chế món thạch sữa chua thanh long
                                        lung linh sắc màu</p>
                                    <p class="news_list-item-date">08/10/2022</p>
                                    <p class="news_list-item-content">Thạch sữa chua thanh long là món ăn tráng miệng
                                        tuyệt vời cho các mẹ. Đặc biệt là các bạn trẻ. Bởi vì món ăn này rất thanh mát,
                                        dễ ăn, đẹp da và trông rất màu sắc bắt mắt nữa. Nguyên...</p>
                                </div>
                            </li>
                            <li class="news_list-item">
                                <img src="./assests/img/news/3.jpg" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Vì sao hoa quả Việt thất thế trước “cơn lốc” hàng
                                        nhập ngoại?</p>
                                    <p class="news_list-item-date">11/10/2022</p>
                                    <p class="news_list-item-content">Rau củ quả Việt Nam đang dần khẳng định tên tuổi
                                        của mình trên thị trường quốc tế khi nhiều mặt hàng đã có “visa” vào các thị
                                        trường khó tính như Mỹ, Nhật Bản, Australia… Nhưng hiện có một...</p>
                                </div>
                            </li>
                            <li class="news_list-item">
                                <img src="./assests/img/news/2.jpg" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Kỹ thuật trồng rau sạch trong chậu xốp tại nhà đơn
                                        giản</p>
                                    <p class="news_list-item-date">12/10/2022</p>
                                    <p class="news_list-item-content">Tự trồng rau trong thùng xốp tại nhà là sự lựa
                                        chọn của rất nhiều gia đình trong thành phố bởi phương pháp trồng rau đơn giản,
                                        dễ trồng, dễ quản lý, an toàn và tiện lợi. Nhưng người trồng cũng...</p>
                                </div>
                            </li>
                            <li class="news_list-item">
                                <img src="./assests/img/news/4.webp" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Những loại trái cây Nhật đắt như vàng ròng đổ bộ về
                                        Việt Nam</p>
                                    <p class="news_list-item-date">14/10/2022</p>
                                    <p class="news_list-item-content">Nhật Bản là đất nước có nhiều loại hoa quả có chất
                                        lượng thuộc hàng ngon nhất thế giới nhưng cũng vô cùng đắt đỏ. Đáng chú ý, nhiều
                                        loại quả do nông dân Nhật Bản trồng được đem bán...</p>
                                </div>
                            </li>
                            <li class="news_list-item">
                                <img src="./assests/img/news/5.jpg" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Nhập khẩu rau quả vượt mốc 1 tỷ USD, Thái Lan chiếm
                                        60% thị phần</p>
                                    <p class="news_list-item-date">14/10/2022</p>
                                    <p class="news_list-item-content">Theo báo cáo từ Bộ NN&PTNT, giá trị xuất khẩu hàng
                                        rau quả tháng 9 năm 2017 ước đạt 294 triệu USD, đưa giá trị xuất khẩu hàng rau
                                        quả 9 tháng đầu năm 2017 ước đạt 2,64 tỷ USD,...</p>
                                </div>
                            </li>
                            <li class="news_list-item">
                                <img src="./assests/img/news/6.jpg" alt="" class="news_list-item-img">
                                <div class="news_list-item-wrap">
                                    <p class="news_list-item-title">Rau xanh tăng giá mạnh vì trời mưa, người dân nội
                                        thành lao đao</p>
                                    <p class="news_list-item-date">14/10/2022</p>
                                    <p class="news_list-item-content">Khoảng gần một tuần nay, do ảnh hưởng của những
                                        cơn mưa lớn kéo dài liên tiếp nên nguồn cung rau xanh cho các chợ bắt đầu khan
                                        hiếm, giá tăng mạnh, thậm chí nhiều loại giá tăng gần gấp...</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Ngăn cách content với footer -->
        <div style="padding-bottom: 70px; background-color: #fff"></div>

        <footer class="footer" id="footer">
            <!-- được render từ file footer.js  -->
        </footer>
    </div>

    <!-- modal  -->
    <div id="modal_wrap">
        <!-- render từ file modal.js  -->
    </div>
    <!-- chat message  -->
    <div id="chat-box">
        <!-- render từ file chatbox.js  -->
    </div>
    <!-- toast message  -->
    <div id="toast">
        <!-- render từ file toast.js  -->
    </div>

    <!-- render header -->
    <script src="./header/header.js"></script>
    <script src="./header/header_admin.js"></script>
    <script src="header/header_user.js"></script>
    <!-- render toast message  -->
    <script src="./toast_messages/toast.js"></script>
    <!-- render modal  -->
    <script src="./modal/modal.js"></script>
    <!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
    <script src="./javascript/logIn_and_Signup.js"></script>
    <!-- import validator đăng ký, đăng nhập  -->
    <script src="./javascript/validator.js"></script>
    <!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
    <script>
        const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
        const headerMenu = document.querySelector('.header_menu');
        var emailReg;
        var passwordReg;
        var emailLog;
        var passwordLog;
        // Đăng ký 
        Validator({
            form: "#form-1",
            formGroupSelector: ".form-group",
            errorSelector: ".form-message",
            rules: [
                // Validator.isRequired('#fullname'),
                Validator.isRequired("#email"),
                Validator.isEmail("#email"),
                Validator.minLength("#password", 6),
                Validator.isRequired("#password_confirmation"),
                Validator.isConfirmed(
                    "#password_confirmation",
                    function () {
                        return document.querySelector("#form-1 #password").value;
                    },
                    "Mật khẩu nhập lại không khớp"
                ),
            ],
            onSubmit: async function (evt) {

                // console.log(evt);
                emailReg = evt.email;
                passwordReg = evt.password;

                let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
                const response = await fetch(url);
                const res = await response.json();

                if (res != "0") {
                    showSuccessSignUpExistEmailToast();
                    logModal.classList.add('open');
                    regModal.classList.remove('open');
                } else {
                    const data = {
                        email: emailReg,
                        passWord: passwordReg
                    };
                    const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data)
                    });
                    const res = await response.json();
                    console.log(response);
                    console.log(res);
                    if (res == "1") {
                        showSuccessSignUpToast();
                        logModal.classList.add('open');
                        regModal.classList.remove('open');
                        // window.location.reload();
                    } else {
                        showFaiedSignUpToast();
                        logModal.classList.add('open');
                        regModal.classList.remove('open');
                    }
                }
            },
        });
        //Đăng nhập
        Validator({
            form: "#form-2",
            formGroupSelector: ".form-group",
            errorSelector: ".form-message",
            rules: [
                Validator.isRequired("#email"),
                Validator.isEmail("#email"),
                Validator.minLength("#password", 6),
            ],
            onSubmit: function (data) {
                //Call api
                console.log(data);
                emailLog = data.email;
                passwordLog = data.password;

                if (emailReg === emailLog && passwordReg === passwordLog) {
                    showSuccessLogInToast();
                    logModal.classList.remove('open');
                }
                else {
                    showErrorLogInToast();
                }
            },
        });
    </script>
    <!-- xử lý trượt slider  -->
    <script src="./javascript/slider.js"></script>
    <!-- Time sale countDown -->
    <script src="./javascript/countDownTime.js"></script>
    <!-- object product 1  -->
    <script src="./javascript/mainProduct.js"></script>
    <!-- Đóng mở chat hỗ trợ trực tuyến -->
    <script>
        function openChat() {
            document.getElementById("myChat").style.display = "block";
        }
        function closeChat() {
            document.getElementById("myChat").style.display = "none";
        }
    </script>
    <!-- Đóng mở menu mobile -->
    <script>
        function openMenuMobile() {
            document.getElementById("menu_mobile").style.display = "block";
        }
        function closeMenuMobile() {
            document.getElementById("menu_mobile").style.display = "none";
        }
    </script>
    <!-- render chatbox  -->
    <script src="./chatbox/chatbox.js"></script>
    <!-- render footer -->
    <script src="./footer/footer.js"></script>
</body>

</html>