/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


const fruits = [
  {
    name: 'Apple',
    description: 'A sweet and juicy fruit that is high in fiber and Vitamin C.',
    image: 'https://example.com/apple.jpg'
  },
  {
    name: 'Banana',
    description: 'A long, curved fruit that is high in potassium and Vitamin B6.',
    image: 'https://example.com/banana.jpg'
  },
  // ... more fruits ...
];


const fruitContainer = document.querySelector('#fruit-container');

fruits.forEach(fruit => {
  const fruitItem = document.createElement('div');
  fruitItem.classList.add('fruit-item');

  const fruitName = document.createElement('h2');
  fruitName.classList.add('fruit-name');
  fruitName.textContent = fruit.name;
  fruitItem.appendChild(fruitName);

  const fruitDescription = document.createElement('p');
  fruitDescription.classList.add('fruit-description');
  fruitDescription.textContent = fruit.description;
  fruitItem.appendChild(fruitDescription);

  const fruitImage = document.createElement('img');
  fruitImage.classList.add('fruit-image');
  fruitImage.src = fruit.image;
  fruitImage.alt = fruit.name;
  fruitItem.appendChild(fruitImage);

  fruitContainer.appendChild(fruitItem);
});