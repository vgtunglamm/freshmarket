<%-- 
    Document   : test
    Created on : Feb 24, 2023, 7:15:16 PM
    Author     : nguye
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                font-family: comfortaa,cursive;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 800px;*/
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
            .logo{
                margin-left:-10px;
                padding-top:35px;
                padding-bottom:40px;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                .noti-icon{
                    display:flex;
                    margin-left:1100px;
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                .report{
                    
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                   background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
                .report button{
                    margin-top:30px;
                    margin-left:1200px;
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 300px;
                    height:1080px;
                }
                .container-one{
/*                    background-color: #F0DFDF;*/
                    
                    /*margin-left: -5px;*/
                    /*position: ;*/
                    top:0;
                }
                .container-one{
                    display:flex;
                }
                .co-left-top{
                    height:370px;
                    background-color: #F5F5F5;
                    width:1000px;
                    border-radius: 5px;
                    left:0px;
/*                    left
                    */margin:-40px 30px ;
                    padding:20px;
                }
                .co-right{
                    border-radius: 5px;
                    height: 570px;
                    width:450px;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 0px ;
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 130px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                  button:hover{
                     cursor: pointer;
                 }
                
        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Dashboard
                </a>                
                <ul>
                    <li>
                        <a class="active" href="sale.jsp">Sales</a>
                    </li>
                    <li>
                        <a class="active" href="Default.jsp">Default</a>
                    </li>
                  
                </ul>

            </div>
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Edittor</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            
            
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="products.jsp">Products</a>
                </div>                
                <ul>
                    <li><a class="active" href="products.jsp">Products</a></li>
                    <!--<li><a class="active" href="productsDetail.jsp">Product Detail</a></li>-->
            
                    <li><a class="active" href="admin_site.jsp">Checkout</a></li>
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="index.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="index.jsp">Login</a></li>
                    <li><a class="active" href="index.jsp">Register</a></li>
                    <li><a class="active" href="index.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>
                  <input type="text" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                        <h1>Dashboard</h1>
                        <p> Salessa > Dashboard > Sales </p>
                    </div>
                    <div>
                        <a href="#"><button type="button">Create Report</button></a>
                    </div>
                    </div>
                   </div>
                   
                    <div class="container-one">
                    <div class="co-left">
                        <div class="co-left-top">
                             Biểu đồ doanh thu
                        </div>
                        <div class="co-left-bottom">
                            <div >
                                <h2>Download your earnings report</h2>
                            
                            <p>There are many variations of passages.</p>   
                            </div>
                            
                            <div class="in" >
                                <a href="#"><button type="button">Create Report</button></a>
                            </div>
                        </div>

                    </div>
                    <div class="co-right">
                        ---
                    </div>
                </div>

                
                    
                    
                

            </div>
        </div> 
    </body>
</html>
