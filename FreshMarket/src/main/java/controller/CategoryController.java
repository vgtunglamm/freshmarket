package controller;

import model.Brand;
import model.Product;
import service.impl.BrandService;
import service.impl.CategoryService;
import service.impl.ProductService;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/category")
public class CategoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CategoryController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BrandService brandService = new BrandService(request,response);
		ProductService productService = new ProductService(request,response);
		request.setAttribute("brands",brandService.findAll());
		productService.findByCategoryCode(request,response);

	}


}
