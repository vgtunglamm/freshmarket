package request;

public class GetCategoryRequest {
	
	private String name;

	public GetCategoryRequest(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
