package exception;

import enums.Result;

public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	protected int code;

    public BaseException() {
        super("SYSTEM_ERROR");
        this.code = 999;
    }

    public BaseException(Result result) {
        super(result.getDesc());
        this.code = result.getCode();
    }

    public BaseException(String message) {
        super(message);
        this.code = 999;
    }

    public BaseException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
