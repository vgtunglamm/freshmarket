package exception;

import enums.Result;

public class CategoryNotFoundException extends BaseException {

	private static final long serialVersionUID = 1L;
	
	public CategoryNotFoundException() {
		super(Result.ITEM_NOT_FOUND_ERROR.getDesc());
		this.code = Result.ITEM_NOT_FOUND_ERROR.getCode();
	}

	public CategoryNotFoundException(String message) {
		super(message);
		this.code = Result.ITEM_NOT_FOUND_ERROR.getCode();
	}
	
}
