package exception;

import enums.Result;

public class ParamInvalidException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public ParamInvalidException() {
        super(Result.PARAM_INVALID_ERROR.getDesc());
        this.code = Result.PARAM_INVALID_ERROR.getCode();
    }

    public ParamInvalidException(String msg) {
        super(msg);
        this.code = Result.PARAM_INVALID_ERROR.getCode();
    }

}
