package mapper.impl;

import mapper.IMapper;
import model.Wallet;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WalletMapper implements IMapper {
    @Override
    public Wallet mapRow(ResultSet resultSet) {
        Wallet walletModel = new Wallet();
        try {
            walletModel.setId(resultSet.getString("Id"));
            walletModel.setUserId(resultSet.getString("UserId"));
            walletModel.setMainAccount(resultSet.getDouble("MainAccount"));
            return walletModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
