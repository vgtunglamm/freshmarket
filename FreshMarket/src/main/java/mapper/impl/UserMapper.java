package mapper.impl;

import mapper.IMapper;
import model.Category;
import model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements IMapper {
    @Override
    public User mapRow(ResultSet resultSet) {
        User userModel = new User();
        try {
            userModel.setId(resultSet.getString("Id"));
            userModel.setUserName(resultSet.getString("UserName"));
            userModel.setEmail(resultSet.getString("Email"));
            userModel.setRole(resultSet.getString("Role"));
            userModel.setPassWord(resultSet.getString("PassWord"));
            return userModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
