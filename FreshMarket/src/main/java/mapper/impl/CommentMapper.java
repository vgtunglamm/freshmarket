package mapper.impl;

import mapper.IMapper;
import model.Category;
import model.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentMapper implements IMapper {
    @Override
    public Comment mapRow(ResultSet resultSet) {
        Comment commentModel = new Comment();
        try {
            commentModel.setId(resultSet.getString("Id"));
            commentModel.setDetail(resultSet.getString("Detail"));
            commentModel.setCreateDate(resultSet.getDate("CreateDate"));
            commentModel.setRating(resultSet.getInt("Rating"));
            commentModel.setUserId(resultSet.getString("UserId"));
            commentModel.setProductId(resultSet.getString("ProductId"));
            return commentModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
