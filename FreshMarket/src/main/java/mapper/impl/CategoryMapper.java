package mapper.impl;

import mapper.IMapper;
import model.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements IMapper<Category> {
    @Override
    public Category mapRow(ResultSet resultSet) {
        Category categoryModel = new Category();
        try {
            categoryModel.setId(resultSet.getString("Id"));
            categoryModel.setName(resultSet.getString("Name"));
            return categoryModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
