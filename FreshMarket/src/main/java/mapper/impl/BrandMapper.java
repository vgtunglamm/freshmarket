package mapper.impl;

import mapper.IMapper;
import model.Brand;
import model.Cart;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class BrandMapper implements IMapper {
    @Override
    public Brand mapRow(ResultSet resultSet) {
        Brand brandModel = new Brand();
        try {
            brandModel.setId(UUID.fromString(resultSet.getString("Id")));
            brandModel.setName(resultSet.getString("Name"));
            return brandModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
