package mapper.impl;

import mapper.IMapper;
import model.Cart;
import model.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CartMapper implements IMapper {
    @Override
    public Cart mapRow(ResultSet resultSet) {
        Cart cartModel = new Cart();
        try {
            cartModel.setId(resultSet.getString("Id"));
            cartModel.setProductId(resultSet.getString("ProductId"));
            cartModel.setUserId(resultSet.getString("UserId"));
            cartModel.setQuantity(resultSet.getInt("Quantity"));
            cartModel.setIsBuy(resultSet.getBoolean("IsBuy"));
            return cartModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
