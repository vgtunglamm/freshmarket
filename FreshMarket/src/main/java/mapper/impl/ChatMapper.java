package mapper.impl;

import mapper.IMapper;
import model.Category;
import model.Chat;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ChatMapper implements IMapper<Chat> {
    @Override
    public Chat mapRow(ResultSet resultSet) {
        Chat chatModel = new Chat();
        try {
            chatModel.setId(resultSet.getString("Id"));
            chatModel.setFrom(resultSet.getString("From"));
            chatModel.setTo(resultSet.getString("To"));
            chatModel.setMessage(resultSet.getString("Message"));
            chatModel.setCreateDate(resultSet.getTimestamp("CreateDate"));
            return chatModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
