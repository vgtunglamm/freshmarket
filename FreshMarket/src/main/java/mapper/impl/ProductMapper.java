package mapper.impl;

import mapper.IMapper;
import model.Product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ProductMapper implements IMapper {
    @Override
    public Product mapRow(ResultSet resultSet) {
        Product productModel = new Product();
        try {
            productModel.setId(resultSet.getString("Id"));
            productModel.setProductName(resultSet.getString("ProductName"));
            productModel.setCateId(resultSet.getString("CateId"));
            productModel.setPrice(resultSet.getDouble("Price"));
            productModel.setDescription(resultSet.getString("Description"));
            productModel.setBrand(resultSet.getString("Brand"));
            productModel.setTransportMethod(resultSet.getString("TransportMethod"));
            productModel.setStatus(resultSet.getBoolean("Status"));
            productModel.setQuantity(resultSet.getInt("Quantity"));
            productModel.setImage(resultSet.getString("Image"));
            productModel.setColor(resultSet.getString("Color"));
            productModel.setBrandId(resultSet.getString("BrandId"));
            return productModel;
        } catch (SQLException e) {
            return null;
        }
    }
}
