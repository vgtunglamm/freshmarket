package filter;

import jakarta.servlet.http.HttpFilter;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin_site.jsp")
public class AuthenFilter extends HttpFilter implements Filter {
       
    public AuthenFilter() {
        super();
    }

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		HttpSession httpSession = httpRequest.getSession(false);
		
//		boolean loggedIn = httpSession != null && httpSession.getAttribute("role") != null;

		Object role = httpSession.getAttribute("role");

		if(role == null || !"admin".equals(role.toString())){
			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
			dispatcher.forward(request, response);
		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
