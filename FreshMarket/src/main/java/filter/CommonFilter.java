package filter;

import model.Brand;
import service.impl.BrandService;
import service.impl.CategoryService;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebFilter("*")
public class CommonFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletRespone = (HttpServletResponse) response;
        CategoryService categoryService = new CategoryService(httpServletRequest,httpServletRespone);

        request.setAttribute("categories",categoryService.findAll());

        chain.doFilter(request, response);
    }
}
