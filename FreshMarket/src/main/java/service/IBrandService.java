package service;

import model.Brand;
import java.util.List;

public interface IBrandService {
    List<Brand> findAll();
}
