package service;

import model.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IProductService {
    void findByCategoryCode(HttpServletRequest request, HttpServletResponse response);
    void findByCategoryIdRest(HttpServletRequest request, HttpServletResponse response);

    void updateProduct(HttpServletRequest request, HttpServletResponse response) throws IOException;
    void create(HttpServletRequest request, HttpServletResponse response) throws IOException;

    void delete(HttpServletRequest request, HttpServletResponse response);

    void getDetail(HttpServletRequest request, HttpServletResponse response);

    void getSuggest(HttpServletRequest request, HttpServletResponse response);

    void searchProduct(HttpServletRequest request, HttpServletResponse response);
    void getAll();

}
