package service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IWalletService {
    void findById(HttpServletRequest request, HttpServletResponse response);

    void addFund(HttpServletRequest request,HttpServletResponse response);

}
