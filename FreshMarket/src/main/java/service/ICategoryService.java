package service;

import model.Category;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface ICategoryService {
    List<Category> findAll();

    void create(HttpServletRequest request, HttpServletResponse response) throws IOException;

    void delete(HttpServletRequest request, HttpServletResponse response) ;

}
