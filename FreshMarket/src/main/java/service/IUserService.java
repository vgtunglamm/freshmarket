package service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface IUserService {
    void createUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    void updateUserOwn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    void changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    void findByEmail(HttpServletRequest request, HttpServletResponse response);
    void findById(HttpServletRequest request, HttpServletResponse response);
    void Login(HttpServletRequest request, HttpServletResponse response);

    void Logout(HttpServletRequest request, HttpServletResponse response);

    void getAll();

    void delete(HttpServletRequest request, HttpServletResponse response);

}
