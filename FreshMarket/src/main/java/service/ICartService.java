package service;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
public interface ICartService {
    void createCart(HttpServletRequest request, HttpServletResponse response) throws IOException;
    void getAll(HttpServletRequest request, HttpServletResponse response) ;
    void delete(HttpServletRequest request,HttpServletResponse response);
    void deleteAll(HttpServletRequest request,HttpServletResponse response);

    void payment(HttpServletRequest request,HttpServletResponse response) throws IOException;


}
