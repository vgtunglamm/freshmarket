package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.ICategoryDAO;
import dao.IProductDAO;
import dao.impl.CategoryDAO;
import dao.impl.ProductDAO;
import model.Product;
import model.User;
import service.IProductService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ProductService implements IProductService {
    private IProductDAO productDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Gson GSON = new GsonBuilder().create();

    public ProductService(HttpServletRequest request, HttpServletResponse response) {
        super();
        this.request = request;
        this.response = response;
        productDAO = new ProductDAO();
    }


    @Override
    public void findByCategoryCode(HttpServletRequest request, HttpServletResponse response) {
        try{
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            // id , brand , color
            List<Product> products = null;
            if(request.getParameter("id") != null){
                products = productDAO.findByCategoryId(request.getParameter("id"));

            }
            else if (request.getParameter("brand") != null){
                products = productDAO.findByBrand(request.getParameter("brand"));
            }
            else{
                products = productDAO.findByColor(request.getParameter("color"));
            }
            request.setAttribute("products",products);
            String nameFix = "Sản phẩm";
            request.setAttribute("nameFix",nameFix);
            RequestDispatcher dispatcher = request.getRequestDispatcher("categoryProduct.jsp");
            dispatcher.forward(request, response);

        } catch (Exception e){
            System.out.println(e);
        }
    }
    @Override
    public void delete(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Id = request.getParameter("id");
            Long res = productDAO.delete(Id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (res == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));
        } catch (Exception ex) {
            //log
        }
    }
    @Override
    public void findByCategoryIdRest(HttpServletRequest request, HttpServletResponse response) {
        try{
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            // id , brand , color
            List<Product> products = productDAO.findByCategoryId(request.getParameter("id"));
            if(products == null){
                response.getOutputStream().println(GSON.toJson(null));
            }
            else {
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(products, writer);
                writer.flush();
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void updateProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String payload = buffer.toString();
        Product model = GSON.fromJson(payload, Product.class);
        var res = productDAO.update(model);
        if(res == 1){
            response.getOutputStream().println(GSON.toJson("1"));
        }
        else {
            response.getOutputStream().println(GSON.toJson("0"));
        }
    }

    @Override
    public void create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json; charset=UTF-8");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String payload = buffer.toString();
        Product model = GSON.fromJson(payload, Product.class);
        var res = productDAO.create(model);
        if(res == 1){
            response.getOutputStream().println(GSON.toJson("1"));
        }
        else {
            response.getOutputStream().println(GSON.toJson("0"));
        }
    }
    @Override
    public void getDetail(HttpServletRequest request, HttpServletResponse response) {
        try{
            Product product = productDAO.findById(request.getParameter("productId"));
            List<Product> productSuggest = productDAO.getRandomSuggest();
            request.setAttribute("productDetail",product);
            request.setAttribute("productSuggests",productSuggest);
            RequestDispatcher dispatcher = request.getRequestDispatcher("productInformation.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void getSuggest(HttpServletRequest request, HttpServletResponse response) {
        try{
            List<Product> productSuggest = productDAO.getRandomSuggest();
            request.setAttribute("productSuggests",productSuggest);
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void searchProduct(HttpServletRequest request, HttpServletResponse response) {
        try{
            List<Product> productSearch = productDAO.searchProduct(request.getParameter("search"));
            request.setAttribute("productSearches",productSearch);
            String nameFix = "KeyWord = " + request.getParameter("search");
            request.setAttribute("nameFix",nameFix);
            RequestDispatcher dispatcher = request.getRequestDispatcher("resultSearch.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void getAll() {
        try{
            List<Product> products = productDAO.getAll();
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (products == null)
                response.getOutputStream().println(GSON.toJson(null));
            else {
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(products, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }

}
