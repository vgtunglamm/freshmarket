package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.IUserDAO;
import dao.IWalletDAO;
import dao.impl.UserDAO;
import dao.impl.WalletDAO;
import model.User;
import model.Wallet;
import service.IUserService;
import org.json.JSONObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class UserService implements IUserService {

    private IUserDAO userDAO;

    private IWalletDAO walletDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final Gson GSON = new GsonBuilder().create();



    public UserService(HttpServletRequest request, HttpServletResponse response) {
        super();
        this.request = request;
        this.response = response;
        userDAO = new UserDAO();
        walletDAO = new WalletDAO();
    }

    @Override
    public void createUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json;charset=UTF-8");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        User model = GSON.fromJson(payload, User.class);

        User existUser = userDAO.findByEmail(model.getEmail());
        if(existUser != null) {
            response.getOutputStream().println(GSON.toJson("0"));
        } else {
            User user = new User();
            user.setPassWord(model.getPassWord());
            if(model.getUserName() == null){
                user.setUserName(model.getEmail());
            }
            else{
                user.setUserName(model.getUserName());
            }
            user.setEmail(model.getEmail());
            if(model.getRole() == null){
                user.setRole("");
            }
            else{
                user.setRole(model.getRole());
            }
            String userId = UUID.randomUUID().toString();
            user.setId(userId);
            userDAO.create(user);

            /// tao vi cho nguoi dung
            Wallet wallet = new Wallet();
            wallet.setMainAccount(0);
            wallet.setUserId(userId);
            walletDAO.create(wallet);
            /*
            try {
                /// gui mail cam on dang ky
                JSONObject obj = new JSONObject();
                obj.put("data", "a///b///c");
                obj.put("method", "Email");
                obj.put("methodData", user.getEmail());
                obj.put("methodType", 0);
                obj.put("type", 6);

                HttpRequest postRequest = HttpRequest.newBuilder()
                        .uri(new URI("https://mailsender-test.eztek.net/send/data"))
                        .header("Content-Type","application/json")
                        .header("accept","text/plain")
                        .header("Client_Id","ZDU1YWZmZWUtNGZmYS00NDc1LWFmNjgtODE2YmY1ZWFmNmE1")
                        .header("Client_Secret","aHR0cHM6Ly9tZS50cnVlY29ubmVjdC52bg==")
                        .POST(HttpRequest.BodyPublishers.ofString(obj.toString()))
                        .build();
                HttpClient httpClient = HttpClient.newHttpClient();

                httpClient.send(postRequest, HttpResponse.BodyHandlers.ofString());

            } catch (URISyntaxException | InterruptedException e) {
                throw new RuntimeException(e);
            }
            */
            response.getOutputStream().println(GSON.toJson("1"));
        }
    }

    @Override
    public void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        User model = GSON.fromJson(payload, User.class);

        User existUser = userDAO.findById(model.getId());
        if(existUser == null) {
            response.getOutputStream().println(GSON.toJson("0"));
        } else {
            userDAO.update(model);
            response.getOutputStream().println(GSON.toJson("1"));
        }
    }

    @Override
    public void updateUserOwn(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        User model = GSON.fromJson(payload, User.class);
        model.setId(request.getSession().getAttribute("userId").toString());
        User existUser = userDAO.findById(model.getId());
        if(existUser == null) {
            response.getOutputStream().println(GSON.toJson("0"));
        } else {

            userDAO.updateOwn(model);
            response.getOutputStream().println(GSON.toJson("1"));
        }
    }

    @Override
    public void changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String payload = buffer.toString();
        JSONObject jsonObject = new JSONObject(payload);
        String password = jsonObject.getString("password");
        String newpassword = jsonObject.getString("newpassword");

        User model = new User();
        model.setPassWord(newpassword);
        model.setId(request.getSession().getAttribute("userId").toString());
        User existUser = userDAO.findById(model.getId());
        if(existUser == null) {
            response.getOutputStream().println(GSON.toJson("0"));
        } else {
            if(!existUser.getPassWord().equals(hashPassword(password))){
                response.getOutputStream().println(GSON.toJson("-1"));
            }
            else{
                userDAO.changePassword(model);
                response.getOutputStream().println(GSON.toJson("1"));
            }
        }
    }
    @Override
    public void findByEmail(HttpServletRequest request, HttpServletResponse response) {
        try {
            String email = request.getParameter("email");
            User user = userDAO.findByEmail(email);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (user == null)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));

        } catch (Exception ex) {
            //log
        }
    }
    @Override
    public void findById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id = request.getSession().getAttribute("userId").toString();
            User user = userDAO.findById(id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (user == null)
                response.getOutputStream().println(GSON.toJson(null));
            else{
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(user, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }
    @Override
    public void delete(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Id = request.getParameter("id");
            Long res = userDAO.delete(Id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (res == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));
        } catch (Exception ex) {
            //log
        }
    }
    @Override
    public void Login(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");

            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String payload = buffer.toString();
            User model = GSON.fromJson(payload, User.class);

            User user = userDAO.Login(model.getEmail(),model.getPassWord());
            if (user == null)
                response.getOutputStream().println(GSON.toJson("0"));
            else
            {
                String role = user.getRole();
                String userId = user.getId();
                request.getSession().setAttribute("userId", userId);
                if("admin".equals(role)){
                    request.getSession().setAttribute("role", role);
                    response.getOutputStream().println(GSON.toJson("2"));
                }
                else {
                    request.getSession().setAttribute("role", "user");
                    response.getOutputStream().println(GSON.toJson("1"));

                }
            }

        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void Logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getSession().removeAttribute("role");
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void getAll() {
        try {
            List<User> user = userDAO.Getall();
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json; charset=utf-8");
            if (user == null)
                response.getOutputStream().println(GSON.toJson(null));
            else{
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(user, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }
    private String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
