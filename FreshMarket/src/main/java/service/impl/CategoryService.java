package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.ICategoryDAO;
import dao.impl.CategoryDAO;
import model.Cart;
import model.Category;
import org.json.JSONObject;
import service.ICategoryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class CategoryService implements ICategoryService {

    private ICategoryDAO categoryDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Gson GSON = new GsonBuilder().create();

    public CategoryService(HttpServletRequest request, HttpServletResponse response){
        super();
        this.request = request;
        this.response = response;
        categoryDAO = new CategoryDAO();
    }
    @Override
    public List<Category> findAll() {
        return categoryDAO.findAll();
    }

    @Override
    public void create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        JSONObject jsonObject = new JSONObject(payload);
        String productId = jsonObject.getString("cateName");

        Category model = new Category();
        model.setName(productId);

        var res = categoryDAO.create(model);
        if(res == 1){
            response.getOutputStream().println(GSON.toJson("1"));
        }
        else {
            response.getOutputStream().println(GSON.toJson("0"));
        }

    }

    @Override
    public void delete(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Id = request.getParameter("id");
            Long res = categoryDAO.delete(Id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (res == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));
        } catch (Exception ex) {
            //log
        }
    }


}
