package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.IUserDAO;
import dao.IWalletDAO;
import dao.impl.UserDAO;
import dao.impl.WalletDAO;
import model.User;
import model.Wallet;
import org.json.JSONObject;
import service.IWalletService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public class WalletService implements IWalletService {
    private IWalletDAO walletDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final Gson GSON = new GsonBuilder().create();



    public WalletService(HttpServletRequest request, HttpServletResponse response) {
        super();
        this.request = request;
        this.response = response;
        walletDAO = new WalletDAO();
    }

    @Override
    public void findById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id = request.getSession().getAttribute("userId").toString();
            Wallet wallet = walletDAO.findById(id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (wallet == null)
                response.getOutputStream().println(GSON.toJson(null));
            else{
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(wallet, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void addFund(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id = request.getSession().getAttribute("userId").toString();
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");

            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            String payload = buffer.toString();
            JSONObject jsonObject = new JSONObject(payload);
            Double totalCost = jsonObject.getDouble("total");

            var wallet = walletDAO.addFund(totalCost,id);

            if (wallet == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else{
                response.getOutputStream().println(GSON.toJson("1"));
            }
        } catch (Exception ex) {
            //log
        }
    }
}
