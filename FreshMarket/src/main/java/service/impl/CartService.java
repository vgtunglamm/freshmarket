package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.ICartDAO;
import dao.IProductDAO;
import dao.IUserDAO;
import dao.IWalletDAO;
import dao.impl.CartDAO;
import dao.impl.ProductDAO;
import dao.impl.UserDAO;
import dao.impl.WalletDAO;
import dto.CartDTO;
import model.Cart;
import model.Product;
import model.User;
import org.json.JSONArray;
import org.json.JSONObject;
import service.ICartService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CartService implements ICartService {
    private ICartDAO cartDAO;

    private IWalletDAO walletDAO;
    private IProductDAO productDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final Gson GSON = new GsonBuilder().create();

    public CartService(HttpServletRequest request, HttpServletResponse response) {
        super();
        this.request = request;
        this.response = response;
        cartDAO = new CartDAO();
        productDAO = new ProductDAO();
        walletDAO = new WalletDAO();
    }
    @Override
    public void createCart(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getSession().getAttribute("userId").toString();
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        JSONObject jsonObject = new JSONObject(payload);
        String productId = jsonObject.getString("productId");
        int quantity = jsonObject.getInt("quantity");

        Cart model = new Cart();
        model.setProductId(productId);
        model.setQuantity(quantity);
        model.setUserId(id);

        Cart existCart = cartDAO.checkExist(id,productId);
        if(existCart != null) {
            response.getOutputStream().println(GSON.toJson("0"));
        } else {
            var temp = cartDAO.create(model);
            if(temp == 1){
                productDAO.changeQuantity(productId,quantity);
                response.getOutputStream().println(GSON.toJson("1"));
            }
        }
    }

    @Override
    public void getAll(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id = request.getSession().getAttribute("userId").toString();

            List<Cart> carts = cartDAO.Getall(id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json; charset=utf-8");
            if (carts == null)
                response.getOutputStream().println(GSON.toJson(null));
            else{
                List<CartDTO> res = new ArrayList<>();
                for(int i = 0 ; i< carts.size() ; i++){
                    CartDTO tmp = new CartDTO();
                    Product product = productDAO.findById(carts.get(i).getProductId());
                    tmp.id = carts.get(i).getId();
                    tmp.quantity = carts.get(i).getQuantity();
                    tmp.price = product.getPrice();
                    tmp.productName = product.getProductName();
                    tmp.image = product.getImage();
                    res.add(tmp);
                }
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(res, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void delete(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Id = request.getParameter("id");
            Long res = cartDAO.delete(Id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (res == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void deleteAll(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Id = request.getSession().getAttribute("userId").toString();
            Long res = cartDAO.deleteAll(Id);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");
            if (res == 0)
                response.getOutputStream().println(GSON.toJson("0"));
            else
                response.getOutputStream().println(GSON.toJson("1"));
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void payment(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        response.setStatus(200);
        response.setHeader("Content-Type", "application/json");

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = request.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String payload = buffer.toString();
        JSONObject jsonObject = new JSONObject(payload);
        Double totalCost = jsonObject.getDouble("total");
        var temp = walletDAO.buy(totalCost,userId);
        if(temp == 0){
            response.getOutputStream().println(GSON.toJson("0"));
        }
        else{
            JSONArray productIdsArray = jsonObject.getJSONArray("productIds");
            for (int i = 0; i < productIdsArray.length(); i++) {
                String productId = productIdsArray.getString(i);
                cartDAO.updateCartBuy(productId);
            }
            response.getOutputStream().println(GSON.toJson("1"));
        }

    }
}
