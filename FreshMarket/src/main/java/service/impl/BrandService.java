package service.impl;

import dao.IBrandDAO;
import dao.ICategoryDAO;
import dao.impl.BrandDAO;
import dao.impl.CategoryDAO;
import model.Brand;
import service.IBrandService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class BrandService implements IBrandService {

    private IBrandDAO brandDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;
    public BrandService(HttpServletRequest request, HttpServletResponse response){
        super();
        this.request = request;
        this.response = response;
        brandDAO = new BrandDAO();
    }
    @Override
    public List<Brand> findAll() {
        return brandDAO.findAll();
    }
}
