package service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.IChatDAO;
import dao.IProductDAO;
import dao.impl.ChatDAO;
import dao.impl.ProductDAO;
import dto.CartDTO;
import model.Cart;
import model.Chat;
import model.Product;
import org.json.JSONObject;
import service.IChatService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ChatService implements IChatService {
    private IChatDAO chatDAO;
    private HttpServletRequest request;
    private HttpServletResponse response;

    private static final Gson GSON = new GsonBuilder().create();

    public ChatService(HttpServletRequest request, HttpServletResponse response) {
        super();
        this.request = request;
        this.response = response;
        chatDAO = new ChatDAO();
    }

    @Override
    public void getConversation(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");

            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String payload = buffer.toString();
            JSONObject jsonObject = new JSONObject(payload);
            String from = jsonObject.getString("from");
            String to = jsonObject.getString("to");

            List<Chat> chats = chatDAO.getConversation(from,to);
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json; charset=utf-8");
            if (chats == null)
                response.getOutputStream().println(GSON.toJson(null));
            else{
                OutputStream outputStream = response.getOutputStream();
                Writer writer = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
                GSON.toJson(chats, writer);
                writer.flush();
            }
        } catch (Exception ex) {
            //log
        }
    }

    @Override
    public void sendMessage(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setStatus(200);
            response.setHeader("Content-Type", "application/json");

            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String payload = buffer.toString();
            JSONObject jsonObject = new JSONObject(payload);
            String from = jsonObject.getString("from");
            String to = jsonObject.getString("to");
            String mess = jsonObject.getString("mess");

            chatDAO.sendMessage(from,to,mess);
        } catch (Exception ex) {
            //log
        }
    }
}
