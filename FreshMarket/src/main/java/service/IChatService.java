package service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IChatService {
    void getConversation(HttpServletRequest request, HttpServletResponse response);
    void sendMessage(HttpServletRequest request, HttpServletResponse response);
}
