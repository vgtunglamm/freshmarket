package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FileUtil {
    public static void writeByteToImageFile(String dataImage, String fileNameImage) throws Exception {
        byte[] bImg64 = dataImage.getBytes();
        byte[] bImg = Base64.getDecoder().decode(bImg64);
        try (FileOutputStream fos = new FileOutputStream(fileNameImage)) {
            fos.write(bImg);
        } catch (IOException ex) {
            throw new Exception("Write file to " + fileNameImage + " failed !!!");
        }
    }

    //data:image/jpeg;base64,/
    public static String extractMimeTypeBase64(final String encoded) {
        final Pattern mime = Pattern.compile("^data:([a-zA-Z0-9]+/[a-zA-Z0-9]+).*");
        final Matcher matcher = mime.matcher(encoded);
        if (!matcher.find())
            return "";
        return matcher.group(1).toLowerCase();
    }


    public static int deleteFile(String filePath) {
        try {
            File file = new File(filePath);
            return deleteFile(file);
        } catch (Exception e) {
        }
        return -1;
    }

    public static int deleteFile(File file) {
        try {
            if (file != null) {
                if (file.delete()) {
                    //xóa thành công
                    return 0;
                } else {
                    return 1;
                }
            }
        } catch (Exception e) {
        }
        return -1;
    }

    public static String getFileNameSuccess(Date time, int packageId) {
        return String.format("Success_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameError(Date time, int packageId) {
        return String.format("Error_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }
    
    public static String getFileNameError(String time, int packageId) {
        return String.format("Error_%d_%s.txt", packageId, time);
    }

    public static String getFileNameErrorPackNotFound(Date time, int packageId) {
        return String.format("Error_PackNotFound_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameErrorUserNotFound(Date time, int packageId) {
        return String.format("Error_UserNotFound_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

    public static String getTrancodeFileNameError(Date time, String infoId) {
        return String.format("Trancode_Error_%s_%s.txt", infoId, DateUtil.getStringPartTime(time));
    }

    public static String getFileNameRenewalError(Date time, int packageId) {
        return String.format("RenewalError_%d_%s.txt", packageId, DateUtil.getStringPartTime(time));
    }

}
