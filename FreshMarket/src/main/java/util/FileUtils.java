package util;

import java.io.*;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils {

    static final Logger LOGGER = Logger.getLogger(FileUtils.class.getName());

    public static void write(String target, InputStream src) throws IOException {
        OutputStream os = new FileOutputStream(target);
        byte[] buf = new byte[4096];
        int len;
        while (-1 != (len = src.read(buf))) {
            os.write(buf, 0, len);
        }
        os.flush();
        os.close();
    }

    public static void writeWithBlok(String target, Long targetSize, InputStream src, Long srcSize, Integer chunks, Integer chunk) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(target, "rw");
        randomAccessFile.setLength(targetSize);
        if (chunk == chunks - 1 && chunk != 0) {
            randomAccessFile.seek(chunk * (targetSize - srcSize) / chunk);
        } else {
            randomAccessFile.seek(chunk * srcSize);
        }
        byte[] buf = new byte[4096];
        int len;
        while (-1 != (len = src.read(buf))) {
            randomAccessFile.write(buf, 0, len);
        }
        randomAccessFile.close();
    }

    public static String generateFileName(String prefix) {
        return prefix + "-" + UUID.randomUUID().toString();
    }

    public static String generateFileName() {
        return UUID.randomUUID().toString();
    }

    public static boolean checkFolder(String path) {
        File fp = new File(path);

        if (fp.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean createOutFolder(String outpath) {
        StringTokenizer st = new StringTokenizer(outpath, "/");
        String tmpPath = "";
        File dirfp;

        while (st.hasMoreElements()) {
            if (tmpPath.equalsIgnoreCase("")) {
                tmpPath = "/" + st.nextToken();
            } else {
                tmpPath = tmpPath + "/" + st.nextToken();
            }

            dirfp = new File(tmpPath);
            if (!dirfp.exists()) {
                if (dirfp.mkdirs()) {
                    if (LOGGER.isLoggable(Level.INFO)) {
                    	LOGGER.info("Create folder " + tmpPath);
                    }

                    dirfp = null;
                } else {
                    return false;
                }

            }
        }

        return true;
    }
}
