package dto;

public class CartDTO {
    public String id;

    public int quantity;

    public String image;
    public double price;

    public String productName;

}
