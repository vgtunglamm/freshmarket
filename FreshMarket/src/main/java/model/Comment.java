package model;

import java.util.Date;

public class Comment {
    private String id;
    private String productId;
    private String userId;
    private Date createDate;
    private int rating;
    private String Detail;

    public Comment(String id, String productId, String userId, Date createDate, int rating, String detail) {
        this.id = id;
        this.productId = productId;
        this.userId = userId;
        this.createDate = createDate;
        this.rating = rating;
        Detail = detail;
    }
    public Comment(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }
}
