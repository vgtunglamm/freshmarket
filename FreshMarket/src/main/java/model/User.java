package model;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class User {
    private String id;
    private String userName;
    private String email;
    private String passWord;
    private String role;

    public User(String id, String userName, String fullName, String passWord, String role) {
        this.id = id;
        this.userName = userName;
        this.email = fullName;
        this.passWord = passWord;
        this.role = role;
    }

    public User(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
