package model;

import java.util.UUID;

public class Brand {
    private UUID id ;

    private String name;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Brand(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Brand() {
    }

    public void setName(String name) {
        this.name = name;
    }

}
