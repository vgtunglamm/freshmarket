package model;

import java.util.Date;

public class Chat {
    private String id;
    private String from;
    private String to;
    private Date createDate;
    private String message;

    public Chat(String id, String from, String to, Date createDate, String message) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.createDate = createDate;
        this.message = message;
    }

    public Chat() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
