package model;

import java.util.UUID;

public class Product {
    private String id ;
    private String cateId;

    private String brandId;

    private String color;

    private String productName;
    private double price;
    private String description;
    private String brand;
    private String transportMethod;
    private Boolean Status;
    private int quantity;

    private String image;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Product(String id, String cateId, String productName, double price, String description, String brand, String transportMethod, Boolean status, int quantity, String image, String brandId, String color) {
        this.id = id;
        this.cateId = cateId;
        this.productName = productName;
        this.price = price;
        this.description = description;
        this.brand = brand;
        this.transportMethod = transportMethod;
        this.Status = status;
        this.quantity = quantity;
        this.image = image;
        this.brandId = brandId;
        this.color = color;

    }

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
