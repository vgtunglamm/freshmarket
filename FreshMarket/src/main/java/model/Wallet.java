package model;

public class Wallet {
    private String id;
    private String userId;

    private double mainAccount;

    public Wallet(String id, String userId, double mainAccount) {
        this.id = id;
        this.userId = userId;
        this.mainAccount = mainAccount;
    }

    public Wallet(){}
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(double mainAccount) {
        this.mainAccount = mainAccount;
    }
}
