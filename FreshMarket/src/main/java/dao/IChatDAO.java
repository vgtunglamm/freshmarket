package dao;

import model.Cart;
import model.Chat;

import java.util.List;

public interface IChatDAO {
    List<Chat> getConversation(String From,String To);
    Long sendMessage(String From,String To,String Mess);
}
