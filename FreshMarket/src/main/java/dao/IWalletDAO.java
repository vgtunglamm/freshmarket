package dao;

import model.User;
import model.Wallet;

public interface IWalletDAO {
    Wallet findById(String Id);
    Long create(Wallet newWallet);

    Long buy(Double totalCost,String userId);

    Long addFund(Double totalCost,String userId);

}
