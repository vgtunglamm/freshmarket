package dao;

import model.Product;

import java.util.List;

public interface IProductDAO {

    List<Product> findByCategoryId(String categoryId);
    List<Product> findByBrand(String brandId);
    List<Product> findByColor(String Color);

    List<Product> getRandomSuggest();
    Product findById(String productId);
    List<Product> findAll();
    Long update(Product newProduct);
    Long create(Product newProduct);
    Long delete(String Id);
    List<Product> searchProduct(String keyword);

    List<Product> getAll();

    void changeQuantity(String id,int quangtity);

}
