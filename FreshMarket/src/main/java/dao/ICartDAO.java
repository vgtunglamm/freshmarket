package dao;

import model.Cart;
import model.User;

import java.util.List;

public interface ICartDAO {
    Long create(Cart newCart);
    Long deleteAll(String userId);
    Long delete(String id);
    List<Cart> Getall(String userId);
    Cart checkExist(String userId,String productId);

    Long updateCartBuy(String id);

}
