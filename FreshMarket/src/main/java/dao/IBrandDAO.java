package dao;

import model.Brand;
import model.Category;

import java.util.List;

public interface IBrandDAO {
    List<Brand> findAll();
}
