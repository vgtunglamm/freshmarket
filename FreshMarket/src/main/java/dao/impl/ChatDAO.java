package dao.impl;

import dao.IChatDAO;
import dao.IProductDAO;
import mapper.impl.ChatMapper;
import mapper.impl.ProductMapper;
import model.Chat;
import model.Product;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ChatDAO extends AbstractDAO<Chat> implements IChatDAO {
    private ChatMapper chatMapper = new ChatMapper();
    @Override
    public List<Chat> getConversation(String From, String To) {
        List<Chat> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.chats" +
                " where (\"From\" = ? and \"To\" = ?) or (\"From\" = ? and \"To\" = ?)"+
                "ORDER BY \"CreateDate\"";

        listProduct = super.query(sql, chatMapper, UUID.fromString(From),UUID.fromString(To),UUID.fromString(To),UUID.fromString(From));
        return listProduct;
    }

    @Override
    public Long sendMessage(String From, String To, String Mess) {
        String sql = "INSERT INTO public.chats( \"Id\",  \"From\",  \"To\",  \"Message\",  \"CreateDate\")" +
                " values(?, ?, ?, ?, current_timestamp)";

        return super.create(sql, UUID.randomUUID(),UUID.fromString(From), UUID.fromString(To), Mess);
    }
}
