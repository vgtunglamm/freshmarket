package dao.impl;

import dao.IBrandDAO;
import mapper.impl.BrandMapper;
import mapper.impl.CategoryMapper;
import model.Brand;
import model.Category;

import java.util.ArrayList;
import java.util.List;

public class BrandDAO extends AbstractDAO<Brand> implements IBrandDAO {
    private BrandMapper brandMapper = new BrandMapper();
    @Override
    public List<Brand> findAll() {
        List<Brand> listAllBrand = new ArrayList<>();
        String sql = "SELECT * FROM public.brands";

        listAllBrand = super.query(sql, brandMapper);

        return listAllBrand;
    }
}
