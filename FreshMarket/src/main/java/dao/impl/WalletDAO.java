package dao.impl;

import dao.IUserDAO;
import dao.IWalletDAO;
import mapper.impl.UserMapper;
import mapper.impl.WalletMapper;
import model.User;
import model.Wallet;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WalletDAO extends AbstractDAO<Wallet> implements IWalletDAO {
    private WalletMapper userMapper = new WalletMapper();

    @Override
    public Wallet findById(String Id) {
        List<Wallet> wallets = new ArrayList<>();
        String sql = "SELECT * FROM public.wallets" +
                " WHERE \"UserId\" = ?";
        wallets = super.query(sql, userMapper, UUID.fromString(Id));
        if(wallets == null || wallets.isEmpty()){
            return null;
        }
        return wallets.get(0);
    }

    @Override
    public Long create(Wallet newWallet) {
        String sql = "INSERT INTO public.wallets( \"Id\",  \"UserId\",  \"MainAccount\")" +
                " values(?, ?, ?)";
        return super.create(sql, UUID.randomUUID(),UUID.fromString(newWallet.getUserId()), newWallet.getMainAccount());
    }

    @Override
    public Long buy(Double totalCost, String userId) {
        String sql = "UPDATE wallets SET \"MainAccount\" = \"MainAccount\" - ?" +
                "  WHERE \"UserId\" = ?";

        List<Wallet> wallets = new ArrayList<>();
        String temp = "SELECT * FROM public.wallets" +
                " WHERE \"UserId\" = ?";
        wallets = super.query(temp, userMapper, UUID.fromString(userId));

        Double mainAccount = wallets.get(0).getMainAccount();

        if (mainAccount < totalCost) {
            return 0L;
        }
        return super.update(sql,totalCost,UUID.fromString(userId));

    }

    @Override
    public Long addFund(Double totalCost, String userId) {
        String sql = "UPDATE wallets SET \"MainAccount\" = \"MainAccount\" + ?" +
                "  WHERE \"UserId\" = ?";
        return super.update(sql,totalCost,UUID.fromString(userId));
    }

}
