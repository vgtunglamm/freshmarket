package dao.impl;

import dao.IUserDAO;
import mapper.impl.ProductMapper;
import mapper.impl.UserMapper;
import model.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


import java.util.UUID;

public class UserDAO extends AbstractDAO<User> implements IUserDAO {
    private UserMapper userMapper = new UserMapper();
    @Override
    public Long create(User newUser) {
        String sql = "INSERT INTO public.users( \"Id\",  \"UserName\",  \"Email\",  \"PassWord\",  \"Role\")" +
                " values(?, ?, ?, ?, ?)";
        return super.create(sql, UUID.fromString(newUser.getId()), newUser.getUserName(), newUser.getEmail(), hashPassword(newUser.getPassWord()),newUser.getRole());
    }

    @Override
    public Long update(User user) {
        String sql = "UPDATE public.users " +
                "SET \"UserName\" = ?, \"Email\" = ?, \"Role\" = ? " +
                "WHERE \"Id\" = ?";

        return super.update(sql,user.getUserName(), user.getEmail(),user.getRole(),UUID.fromString(user.getId()));
    }

    @Override
    public Long updateOwn(User user) {
        String sql = "UPDATE public.users " +
                "SET \"UserName\" = ?, \"Email\" = ?" +
                "WHERE \"Id\" = ?";

        return super.update(sql,user.getUserName(), user.getEmail(),UUID.fromString(user.getId()));
    }

    @Override
    public Long changePassword(User user) {
        String sql = "UPDATE public.users " +
                "SET \"PassWord\" = ? " +
                "WHERE \"Id\" = ?";

        return super.update(sql, hashPassword(user.getPassWord()),UUID.fromString(user.getId()));
    }

    @Override
    public User findByEmail(String email) {
        List<User> user = new ArrayList<>();
        String sql = "SELECT * FROM public.users" +
                " WHERE \"Email\" = ?";
        user = super.query(sql, userMapper,email);
        if(user == null || user.isEmpty()){
            return null;
        }
        return user.get(0);
    }

    @Override
    public Long delete(String Id) {
        String sql = "DELETE from public.users" +
                " WHERE \"Id\" = ?";
        return super.update(sql,UUID.fromString(Id));
    }
    @Override
    public User findById(String Id) {
        List<User> user = new ArrayList<>();
        String sql = "SELECT * FROM public.users" +
                " WHERE \"Id\" = ?";
        user = super.query(sql, userMapper,UUID.fromString(Id));
        if(user == null || user.isEmpty()){
            return null;
        }
        return user.get(0);
    }
    @Override
    public User Login(String email, String password) {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM public.users" +
                " WHERE \"Email\" = ?";
        users = super.query(sql, userMapper, email);

        if (users == null || users.isEmpty()) {
            return null; // User not found
        }

        User user = users.get(0);

        // Hash the provided password
        String hashedPassword = hashPassword(password);

        // Compare the hashed passwords
        if (hashedPassword.equals(user.getPassWord())|| password.equals(user.getPassWord())) {
            return user; // Passwords match, login successful
        } else {
            return null; // Passwords do not match, login failed
        }
    }

    @Override
    public List<User> Getall() {
        List<User> user = new ArrayList<>();
        String sql = "SELECT * FROM public.users" ;
        user = super.query(sql, userMapper);
        if(user == null || user.isEmpty()){
            return null;
        }
        return user;
    }
    private String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
