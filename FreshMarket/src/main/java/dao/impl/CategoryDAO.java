package dao.impl;

import dao.ICategoryDAO;
import mapper.impl.CategoryMapper;
import model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CategoryDAO extends AbstractDAO<Category> implements ICategoryDAO {
    private CategoryMapper categoryMapper = new CategoryMapper();

    @Override
    public List<Category> findAll() {
        List<Category> listAllCategory = new ArrayList<>();
        String sql = "SELECT * FROM public.categories";

        listAllCategory = super.query(sql, categoryMapper);

        return listAllCategory;
    }

    @Override
    public Long delete(String Id) {
        String sql = "Delete  FROM public.categories " +
                "WHERE \"Id\" = ? ";

        return super.update(sql, UUID.fromString(Id));
    }

    @Override
    public Long create(Category newCate) {
        String sql = "INSERT INTO public.categories( \"Id\",  \"Name\")" +
                " values(?, ?)";
        return super.create(sql, UUID.randomUUID(),newCate.getName());
    }

//    @Override
//    public CategoryModel get(Long categoryId) {
//        String sql = "SELECT * FROM category WHERE category_id = ?";
//        List<CategoryModel> list = new ArrayList<>();
//        list = query(sql, new CategoryMapper(), categoryId);
//
//        return list.isEmpty() ? null : list.get(0);
//    }
//

}
