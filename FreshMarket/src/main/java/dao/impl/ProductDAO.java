package dao.impl;

import dao.ICategoryDAO;
import dao.IProductDAO;
import mapper.impl.CategoryMapper;
import mapper.impl.ProductMapper;
import model.Category;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductDAO extends AbstractDAO<Product> implements IProductDAO {
    private ProductMapper productMapper = new ProductMapper();
    @Override
    public List<Product> findByCategoryId(String categoryId) {
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " where \"CateId\" = ? ";

        listProduct = super.query(sql, productMapper,UUID.fromString(categoryId));
        return listProduct;
    }
    @Override
    public Long delete(String Id) {
        String sql = "DELETE from public.products" +
                " WHERE \"Id\" = ?";
        return super.update(sql,UUID.fromString(Id));
    }
    @Override
    public List<Product> findByBrand(String brandId) {
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " where \"BrandId\" = ? ";

        listProduct = super.query(sql, productMapper,UUID.fromString(brandId));
        return listProduct;
    }

    @Override
    public List<Product> findByColor(String categoryColor) {
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " where \"Color\" = ? ";

        listProduct = super.query(sql, productMapper,categoryColor);
        return listProduct;
    }

    @Override
    public List<Product> getRandomSuggest() {
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " order by random() limit 5 ";

        listProduct = super.query(sql, productMapper);
        return listProduct;
    }

    @Override
    public Product findById(String productId) {
        List<Product> listById = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " where \"Id\" = ? ";

        listById = super.query(sql, productMapper,UUID.fromString(productId));

        Product product = listById.get(0);

        return  product;
    }
    @Override
    public List<Product> findAll() {
        String sql = "SELECT * FROM news";
        List<Product> listNew = new ArrayList<>();
        listNew = query(sql, productMapper);
        return listNew;
    }

    @Override
    public void changeQuantity(String id, int quangtity) {
        String sql = "UPDATE products SET \"Quantity\" = \"Quantity\" - ?" +
                "  WHERE \"Id\" = ?";
        super.update(sql,quangtity,UUID.fromString(id) );
    }

    @Override
    public Long update(Product newProduct) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE products SET \"ProductName\" = ? , \"Color\" = ?");
        sql.append(", \"Price\" = ?, \"Description\" = ?");
        sql.append(", \"Brand\" = ?, \"TransportMethod\" = ?, \"Status\" = ?, \"Quantity\" = ?, \"Image\" = ?");
        sql.append(" WHERE \"Id\" = ?");
        return super.update(sql.toString(), newProduct.getProductName(), newProduct.getColor(),
                newProduct.getPrice(), newProduct.getDescription(), newProduct.getBrand(), newProduct.getTransportMethod(),
                newProduct.getStatus(), newProduct.getQuantity(), newProduct.getImage(),UUID.fromString(newProduct.getId()));
    }

    @Override
    public Long create(Product newProduct) {
        String sql = "INSERT INTO public.products(\"Id\", \"ProductName\", \"CateId\", \"Price\", \"Description\", \"Brand\", \"TransportMethod\",\"Status\",\"Quantity\",\"Image\")" +
                " values(?, ?, ?, ?, ?, ?, ?,?,?,?)";
        return super.create(sql, UUID.randomUUID(), newProduct.getProductName(),UUID.fromString(newProduct.getCateId()),
                newProduct.getPrice(), newProduct.getDescription(), newProduct.getBrand(), newProduct.getTransportMethod(),
                newProduct.getStatus(), newProduct.getQuantity(), newProduct.getImage());
    }

    @Override
    public List<Product> searchProduct(String keyword) {
        keyword = "%" + keyword.toLowerCase() + "%";
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products" +
                " where lower(\"ProductName\") like ? ";

        listProduct = super.query(sql, productMapper,keyword);
        return listProduct;
    }

    @Override
    public List<Product> getAll() {
        List<Product> listProduct = new ArrayList<>();
        String sql = "SELECT * FROM public.products";
        listProduct = super.query(sql, productMapper);
        return listProduct;
    }

    public void delete(Long newId) {
        // delete all cmt da
        String sql = "DELETE FROM public.products where \"Id\" = ?";
        super.update(sql, newId);
    }
}
