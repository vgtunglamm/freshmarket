package dao.impl;

import java.io.FileNotFoundException;
import java.security.Timestamp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Logger;

import dao.GenericDAO;
import mapper.IMapper;
import util.DBConnectionUtil;

public class AbstractDAO<T> implements GenericDAO<T> {
	
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    public Connection getConnection() {
        Connection conn = null;

        try {
            conn = DBConnectionUtil.getConnection();
        } catch (Exception e) {
            System.out.println(e);
//            LOGGER.error("Fail to connect to database: {}", e.getMessage());
        }

        return conn;
    }
    
    /**
     * Auto set parameters by object type 
     * Add more if needed 
     */
    private void setParameter(PreparedStatement preparedStatement, Object... parameters) throws SQLException {
        for (int i = 0; i < parameters.length; i++) {
            Object parameter = parameters[i];
            int index = i + 1;
            if (parameter instanceof Long) {
                preparedStatement.setLong(index, (long) parameter);
            } else if (parameter instanceof String) {
                preparedStatement.setString(index, (String) parameter);
            } else if (parameter instanceof Integer) {
                preparedStatement.setInt(index, (Integer) parameter);
            }
            else if (parameter instanceof Date) {
                preparedStatement.setDate(index, (java.sql.Date) parameter);
            }
            else if (parameter instanceof UUID) {
                preparedStatement.setObject(index, parameter);
            }
            else if (parameter instanceof Boolean) {
                preparedStatement.setObject(index, parameter);
            }
            else if (parameter instanceof Double) {
                preparedStatement.setObject(index,(Double) parameter);
            }
        }
    }

    @Override
    public <T> List<T> query(String sql, IMapper<T> mapper, Object... parameters) {
        List<T> result = new ArrayList<T>();
        Connection conn = getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = conn.prepareStatement(sql);
            setParameter(preparedStatement, parameters);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result.add(mapper.mapRow(resultSet));
            }

        } catch (SQLException e) {
//            LOGGER.error("Fail to query: {}", e.getMessage());
            return null;

        } finally {
            try {
                if(resultSet != null) resultSet.close();

                if(preparedStatement != null) preparedStatement.close();

                if (conn != null ) conn.close();

            } catch (SQLException e) {
//                LOGGER.error("Fail to close connection: {}", e.getMessage());
                return null;
            }
        }

        return result;
    }

    @Override
    public <T> T get(long id) {
        return null;
    }

    @Override
    public Long update(String sql, Object... parameters) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;

        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            preparedStatement = conn.prepareStatement(sql);
            setParameter(preparedStatement, parameters);
            preparedStatement.executeUpdate();
            conn.commit();
            return 1L;
        } catch (SQLException e) {
//            LOGGER.error("Can not update: {}", e.getMessage());

            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
//                    LOGGER.error("Fail to rollback update: {}", e1.getMessage());
                    return 0L;
                }
            }
            return 0L;
        } finally {
            try {
                if(preparedStatement != null) preparedStatement.close();

                if (conn != null ) conn.close();
//                return 0L;
            } catch (SQLException ex) {
//                LOGGER.error("Fail to close connection: {}", ex.getMessage());
            }
        }


    }

    @Override
    public Long create(String sql, Object... parameters) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            preparedStatement = conn.prepareStatement(sql);
            setParameter(preparedStatement, parameters);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            conn.commit();
            return 1L;

        } catch (SQLException e) {
//            LOGGER.error("Fail to create entity: {}", e.getMessage());
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e1) {
//                    LOGGER.error("Fail to rollback update: {}", e1.getMessage());
                }
            }
            return 0L;

        } finally {
            try {
                if(resultSet != null) resultSet.close();

                if(preparedStatement != null) preparedStatement.close();

                if (conn != null ) conn.close();
            } catch (SQLException e) {
//                LOGGER.error("Fail to close connection: {}", e.getMessage());
            }
        }
    }

    @Override
    public int count(String sql, Object... parameters) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int count = 0;

        try {
            conn = getConnection();
            preparedStatement = conn.prepareStatement(sql);
            setParameter(preparedStatement, parameters);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                count = resultSet.getInt(1);
            }

        } catch (SQLException e) {
//            LOGGER.error("Fail to execute query: {}", e.getMessage());
            return 0;

        } finally {
            try {
                if(resultSet != null) resultSet.close();

                if(preparedStatement != null) preparedStatement.close();

                if (conn != null ) conn.close();

            } catch (SQLException e) {
//                LOGGER.error("Fail to close connection: {}", e.getMessage());
                return 0;
            }
        }

        return count;
    }

}
