package dao.impl;

import dao.ICartDAO;
import mapper.impl.CartMapper;
import mapper.impl.UserMapper;
import model.Cart;
import model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CartDAO extends AbstractDAO<Cart> implements ICartDAO {
    private CartMapper cartMapper = new CartMapper();
    @Override
    public Long create(Cart newCart) {
        String sql = "INSERT INTO public.carts( \"Id\",  \"ProductId\",  \"UserId\",  \"Quantity\" , \"IsBuy\")" +
                " values(?, ?, ?, ?,?)";
        return super.create(sql, UUID.randomUUID(),UUID.fromString(newCart.getProductId()),UUID.fromString(newCart.getUserId()), newCart.getQuantity(),false);
    }
    @Override
    public Long delete(String Id) {
        String sql = "DELETE from public.carts" +
                " WHERE \"Id\" = ?";
        return super.update(sql,UUID.fromString(Id));
    }

    @Override
    public Long deleteAll(String userId) {
        String sql = "DELETE from public.carts" +
                " WHERE \"UserId\" = ?";
        return super.update(sql,UUID.fromString(userId));
    }
    @Override
    public List<Cart> Getall(String userId) {
        List<Cart> carts = new ArrayList<>();
        String sql = "SELECT * FROM public.carts" +
                " WHERE \"UserId\" = ? and \"IsBuy\" = false";
        carts = super.query(sql, cartMapper,UUID.fromString(userId));
        if(carts == null ){
            return null;
        }
        return carts;
    }

    @Override
    public Cart checkExist(String userId, String productId) {
        List<Cart> carts = new ArrayList<>();
        String sql = "SELECT * FROM public.carts" +
                " WHERE \"UserId\" = ? and \"ProductId\" = ? ";
        carts = super.query(sql, cartMapper,UUID.fromString(userId),UUID.fromString(productId));
        if(carts == null || carts.isEmpty()){
            return null;
        }
        return carts.get(0);
    }

    @Override
    public Long updateCartBuy(String id) {
        String sql = "UPDATE carts SET \"IsBuy\" =true" +
                "  WHERE \"Id\" = ?";
        return super.update(sql,UUID.fromString(id));
    }
}
