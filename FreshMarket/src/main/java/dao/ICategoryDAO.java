package dao;

import model.Category;

import java.util.List;

public interface ICategoryDAO {
    List<Category> findAll();

    Long delete(String Id);
    Long create(Category newCate);
}
