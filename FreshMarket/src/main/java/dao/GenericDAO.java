package dao;

import java.util.List;

import mapper.IMapper;

public interface GenericDAO<T> {
	
    <T> List<T> query(String sql, IMapper<T> rowMapper, Object... parameters);

	<T> T get(long id);

    Long update(String sql, Object... parameters);

    Long create(String sql, Object... parameters);

    int count(String sql, Object... parameters);
    
}
