package dao;

import model.User;

import java.util.List;

public interface IUserDAO {
    Long create(User newUser);
    Long update(User user);
    Long updateOwn(User user);

    Long changePassword(User user);
    User findByEmail(String email);
    Long delete(String Id);
    User findById(String Id);
    User Login(String email,String password);
    List<User> Getall();
}
