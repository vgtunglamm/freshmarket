package rest;

import service.impl.CartService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/carts/get-all")
public class CartGetAllController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CartGetAllController() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CartService cartService = new CartService(request, response);
        cartService.getAll(request, response);

    }
}