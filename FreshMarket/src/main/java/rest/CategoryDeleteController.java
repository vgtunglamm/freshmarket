package rest;

import service.impl.CategoryService;
import service.impl.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/categories/delete/*")
public class CategoryDeleteController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CategoryDeleteController() {
        super();
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CategoryService categoryService = new CategoryService(request, response);
        categoryService.delete(request, response);
    }
}