package rest;

import service.impl.CartService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/api/carts/payment")
public class CartPaymentController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CartPaymentController() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CartService cartService = new CartService(request, response);
        cartService.payment(request, response);

    }
}