package rest;

import service.impl.WalletService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/wallets/add-fund")
public class WalletAddFundController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public WalletAddFundController() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WalletService walletService = new WalletService(request, response);
        walletService.addFund(request, response);

    }
}