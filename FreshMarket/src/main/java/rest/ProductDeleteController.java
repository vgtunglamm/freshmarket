package rest;

import service.impl.ProductService;
import service.impl.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/products/delete/*")
public class ProductDeleteController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ProductDeleteController() {
        super();
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductService productService = new ProductService(request, response);
        productService.delete(request, response);
    }
}