package rest;

import service.impl.CartService;
import service.impl.CategoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/categories/add")
public class CategoryCreateController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CategoryCreateController() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CategoryService categoryService = new CategoryService(request, response);
        categoryService.create(request, response);

    }
}