package rest;

import service.impl.CartService;
import service.impl.ChatService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/chats/get-conversation")
public class ChatGetConverController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ChatGetConverController() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ChatService chatService = new ChatService(request, response);
        chatService.getConversation(request, response);

    }
}