package rest;

import service.impl.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/api/products/get-by-cate/*")
public class ProductByCateController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ProductByCateController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductService productService = new ProductService(request, response);
        productService.findByCategoryIdRest(request,response);

    }
}