<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>G10 MARKET</title>
  <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png" />
  <!--reset css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
  <link rel="stylesheet" href="./assests/css/grid.css" />
  <link rel="stylesheet" href="./assests/css/base.css" />
  <link rel="stylesheet" href="./assests/css/main.css" />
  <link rel="stylesheet" href="./assests/css/responsive.css" />
  <link rel="stylesheet" href="./modal/modal.css" />
  <link rel="stylesheet" href="./categoryProduct.css">
  <link rel="stylesheet" href="./assests/css/productInformation.css">
  <link rel="stylesheet" href="./header/header.css">
  <link rel="stylesheet" href="./footer/footer.css">
  <link rel="stylesheet" href="./contact_page/contact.css">
  <link rel="stylesheet" href="./chatbox/chatbox.css">
  <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/productInformation_responsive.css">
  <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css" />
  <!--Nhúng font-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
    rel="stylesheet" />
</head>

<body>
  <div class="app">
    <c:if test="${sessionScope.role != null && sessionScope.role != 'admin'}">
      <header class="header" id="header_user">
        <!-- được render từ file header.js  -->
      </header>
    </c:if>
    <c:if test="${sessionScope.role == 'admin'}">
      <header class="header" id="header_admin">
        <!-- được render từ file header.js  -->
      </header>
    </c:if>
    <c:if test="${sessionScope.role == null}">
      <header class="header" id="header">
        <!-- được render từ file header.js  -->
      </header>
    </c:if>

    <div class="app_container">
      <!-- Ngăn cách với header  -->
      <div style="padding-top: 15px; background-color: #f5f5f5"></div>

      <div class="grid wide">
        <!-- tiêu đề category (một vài thẻ dùng chung với phần contact)  -->
        <div class="col l-12 m-12 c-12">
          <div class="contact_nav">
            <a href="./" class="back_homepage-nav info_page">Trang
              chủ</a>
            <h3 class="contact_seperate info_page">/</h3>
            <a href="" class="contact_page-nav info_page">Thông tin sản phẩm</a>
          </div>
        </div>
        <!-- product information  -->
        <div class="row product_infomation">
          <div class="col l-4 m-12 c-12">
            <img src="${productDetail.image}" alt="" class="product_info-img" style="padding:30px 26px 26px 26px;">
          </div>

          <!-- thông tin sản phẩm  -->
          <div class="col l-7 m-12 c-12" style="padding: 0 20px 0 20px;">
            <p class="product_info-title">${productDetail.productName}</p>
            <h3 class="product_info-price">${productDetail.price} ₫</h3>
            <p class="product_info-des">${productDetail.description}</p>

            <div class="product_info-wrap">
              <p class="product_info-status">Trạng thái</p>
              <span class="product_info-status-content">
                  <i class="fa-solid fa-check"></i>
                  <c:choose>
                    <c:when test="${productDetail.status=='true'}">
                      Còn hàng
                      <br />
                    </c:when>
                    <c:otherwise>
                      Hết hàng
                      <br />
                    </c:otherwise>
                  </c:choose>
                </span>
            </div>

            <p class="product_info-ship">Vận chuyển
              <i class="fa-solid fa-truck-fast" style="color: #00BFA5; margin-left: 20px;"></i>
              <span class="product_info-ship-content">${productDetail.transportMethod}</span>
            </p>

            <p class="product_info-brand" style="margin-bottom: 8px;">Thương hiệu
              <span class="product_info-brand-name" style="margin-left: 16px;">${productDetail.brand}</span>
            </p>

            <div class="product_info-amount-wrap">
              <p class="product_info-amount-name">Số lượng</p>
              <div class="box-cal" style="margin-left: 16px;">
                <p style="font-size: 1.8rem; cursor: pointer;" onclick="decreaseQuantity()">-</p>
              </div>
              <input type="text" class="product_info-amount-input" id="quantity-input" placeholder="1" value="1">
              <div class="box-cal" style="margin-right: 16px;">
                <p style="font-size: 1.8rem; cursor: pointer;" onclick="increaseQuantity()">+</p>
              </div>
              <p class="product_info-amount">Còn lại ${productDetail.quantity} sản phẩm</p>
            </div>

            <button onclick="callAddCart()"  type="button" class="product_info-btn btn btn--primary">Thêm vào giỏ hàng</button>
          </div>
        </div>

        <!-- row sản phẩm gợi ý  -->
        <div class="row" style="background-color: var(--white-color);margin-top: 16px;">
          <div class="col l-12 m-12 c-12">
            <p class="same_product-tittle">Sản phẩm gợi ý</p>
<%--            <div class="same_product" style="overflow-x: auto;">--%>
<%--              <c:forEach items="${productSuggests}" var="suggest">--%>
<%--                <div class="col l-2-4 m-4 c-6">--%>
<%--                  <a href="productInformation?productId=${suggest.id}" class="home__product-item">--%>
<%--                    <img src="${suggest.image}" alt='' class="home__product-item-img">--%>
<%--                    <h3 class="home__product-item-tittle">${suggest.productName}</h3>--%>
<%--                    <h4 class="home__product-item-price">${suggest.price} ₫</h4>--%>
<%--                    <div class="home__product-item-btn-wrap">--%>
<%--                      <button class="home__product-item-btn btn btn--primary ">Xem chi tiết</button>--%>
<%--                    </div>--%>
<%--                  </a>--%>
<%--                </div>--%>
<%--              </c:forEach>--%>
<%--            </div>--%>
          </div>
        </div>

        <!-- Đánh giá nhận xét từ khách hàng -->
        <div class="row product_rate">
          <div class="col l-12">
            <p class="product_rate-title">Đánh Giá - Nhận Xét Từ Khách Hàng</p>
          </div>

          <div class="col l-4" style="margin-left: 30px;">
            <!-- rating, số sao, số cmt  -->
            <div class="number_star-wrap">
              <p class="product_rate-number">4.2</p>
              <div class="star_countcmt-wrap">
                <div class="product_rate-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.8rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.8rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.8rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.8rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.8rem;"></i>
                </div>
                <p class="count_comment">5 nhận xét</p>
              </div>
            </div>

            <!-- 5 hàng star  -->
            <div class="star_row">
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>

              <div class="star-progress_bar">
                <div class="star-progress_bar-active" style="width: 80%;"></div>
              </div>

              <p class="star_row-count">4</p>
            </div>
            <div class="star_row">
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>

              <div class="star-progress_bar">
                <div class="star-progress_bar-active" style="width: 0;"></div>
              </div>

              <p class="star_row-count">0</p>
            </div>
            <div class="star_row">
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>

              <div class="star-progress_bar">
                <div class="star-progress_bar-active" style="width: 0;"></div>
              </div>

              <p class="star_row-count">0</p>
            </div>
            <div class="star_row">
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>

              <div class="star-progress_bar">
                <div class="star-progress_bar-active" style="width: 0;"></div>
              </div>

              <p class="star_row-count">0</p>
            </div>
            <div class="star_row">
              <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>
              <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.3rem;"></i>

              <div class="star-progress_bar">
                <div class="star-progress_bar-active" style="width: 20%;"></div>
              </div>

              <p class="star_row-count">1</p>
            </div>
          </div>

          <div class="col l-6">
            <div class="img_name_star_cmt-wrap">
              <img src="./assests/img/avtcmt.png" alt="" class="user_cmt-avt">
              <div class="name_star_cmt-wrap">
                <p class="user_cmt-name">Trần Văn Định</p>
                <div class="user_cmt-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                </div>
                <p class="user_cmt-date">2022-11-20 16:41</p>
                <p class="user_cmt-content">Sản phẩm chất lượng tốt, thực phẩm tươi ngon, giao hàng đúng thời gian</p>
              </div>
            </div>
            <div class="img_name_star_cmt-wrap">
              <img src="./assests/img/avtcmt.png" alt="" class="user_cmt-avt">
              <div class="name_star_cmt-wrap">
                <p class="user_cmt-name">Lê Mai Phương</p>
                <div class="user_cmt-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                </div>
                <p class="user_cmt-date">2022-11-18 12:31</p>
                <p class="user_cmt-content">Sản phẩm chất lượng tốt, thực phẩm tươi ngon, giao hàng đúng thời gian</p>
              </div>
            </div>
            <div class="img_name_star_cmt-wrap">
              <img src="./assests/img/avtcmt.png" alt="" class="user_cmt-avt">
              <div class="name_star_cmt-wrap">
                <p class="user_cmt-name">Đỗ Quốc Bảo</p>
                <div class="user_cmt-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                </div>
                <p class="user_cmt-date">2022-11-16 21:51</p>
                <p class="user_cmt-content">Sản phẩm chất lượng tốt, thực phẩm tươi ngon, giao hàng đúng thời gian</p>
              </div>
            </div>
            <div class="img_name_star_cmt-wrap">
              <img src="./assests/img/avtcmt.png" alt="" class="user_cmt-avt">
              <div class="name_star_cmt-wrap">
                <p class="user_cmt-name">Nguyễn Thu An</p>
                <div class="user_cmt-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                </div>
                <p class="user_cmt-date">2022-11-15 12:21</p>
                <p class="user_cmt-content">Sản phẩm chất lượng tốt, thực phẩm tươi ngon, giao hàng đúng thời gian</p>
              </div>
            </div>
            <div class="img_name_star_cmt-wrap" style="padding-bottom: 30px;">
              <img src="./assests/img/avtcmt.png" alt="" class="user_cmt-avt">
              <div class="name_star_cmt-wrap">
                <p class="user_cmt-name">Mai Văn Linh</p>
                <div class="user_cmt-star">
                  <i class="fa-solid fa-star" style="color: #FFD52E;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.2rem;"></i>
                  <i class="fa-solid fa-star" style="color: #EFEFEF;font-size: 1.2rem;"></i>
                </div>
                <p class="user_cmt-date">2022-11-11 11:34</p>
                <p class="user_cmt-content">Giao hàng chậm, sản phẩm không giống trên ảnh</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Ngăn cách content với footer -->
    <div style="padding-bottom: 30px; background-color: #f5f5f5"></div>

    <footer class="footer" id="footer">
      <!-- được render từ file footer.js  -->
    </footer>
  </div>

  <!-- modal  -->
  <div id="modal_wrap">
    <!-- render từ file modal.js  -->
  </div>
  <!-- chat message  -->
  <div id="chat-box">
    <!-- render từ file chatbox.js  -->
  </div>

  <!-- render header -->
  <script src="./header/header.js"></script>
  <script src="./header/header_admin.js"></script>
  <script src="header/header_user.js"></script>
  <!-- render toast message  -->
  <script src="./toast_messages/toast.js"></script>
  <!-- render modal  -->
  <script src="./modal/modal.js"></script>
  <!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
  <script src="./javascript/logIn_and_Signup.js"></script>
  <!-- import validator đăng ký, đăng nhập  -->
  <script src="./javascript/validator.js"></script>
  <!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
  <script>
    const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
    const headerMenu = document.querySelector('.header_menu');
    var emailReg;
    var passwordReg;
    var emailLog;
    var passwordLog;
    // Đăng ký
    Validator({
      form: "#form-1",
      formGroupSelector: ".form-group",
      errorSelector: ".form-message",
      rules: [
        // Validator.isRequired('#fullname'),
        Validator.isRequired("#email"),
        Validator.isEmail("#email"),
        Validator.minLength("#password", 6),
        Validator.isRequired("#password_confirmation"),
        Validator.isConfirmed(
                "#password_confirmation",
                function () {
                  return document.querySelector("#form-1 #password").value;
                },
                "Mật khẩu nhập lại không khớp"
        ),
      ],
      onSubmit: async function (evt) {

        // console.log(evt);
        emailReg = evt.email;
        passwordReg = evt.password;

        let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
        const response = await fetch(url);
        const res = await response.json();

        if (res != "0") {
          showSuccessSignUpExistEmailToast();
          logModal.classList.add('open');
          regModal.classList.remove('open');
        } else {
          const data = {
            email: emailReg,
            passWord: passwordReg
          };
          const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          });
          const res = await response.json();
          console.log(response);
          console.log(res);
          if (res == "1") {
            showSuccessSignUpToast();
            logModal.classList.add('open');
            regModal.classList.remove('open');
            // window.location.reload();
          } else {
            showFaiedSignUpToast();
            logModal.classList.add('open');
            regModal.classList.remove('open');
          }
        }
      },
    });
    //Đăng nhập
    Validator({
      form: "#form-2",
      formGroupSelector: ".form-group",
      errorSelector: ".form-message",
      rules: [
        Validator.isRequired("#email"),
        Validator.isEmail("#email"),
        Validator.minLength("#password", 6),
      ],
      onSubmit: function (data) {
        //Call api
        console.log(data);
        emailLog = data.email;
        passwordLog = data.password;

        if (emailReg === emailLog && passwordReg === passwordLog) {
          showSuccessLogInToast();
        }
        else {
          showErrorLogInToast();
        }
      },
    });
  </script>
  <!-- xử lý trượt slider  -->
  <script src="./javascript/slider.js"></script>
  <!-- Time sale countDown -->
  <script src="./javascript/countDownTime.js"></script>
  <!-- object product 1  -->
  <script src="./javascript/mainProduct.js"></script>
  <!-- Đóng mở chat hỗ trợ trực tuyến -->
  <script>
    function openChat() {
      document.getElementById("myChat").style.display = "block";
    }
    function closeChat() {
      document.getElementById("myChat").style.display = "none";
    }
  </script>
  <!-- Đóng mở menu mobile -->
  <script>
    function openMenuMobile() {
      document.getElementById("menu_mobile").style.display = "block";
    }
    function closeMenuMobile() {
      document.getElementById("menu_mobile").style.display = "none";
    }
  </script>
  <!-- render chatbox  -->
  <script src="./chatbox/chatbox.js"></script>
  <!-- render footer -->
  <script src="./footer/footer.js"></script>
  <script>
    function decreaseQuantity() {
      let quantityInput = document.getElementById('quantity-input');
      let quantity = parseInt(quantityInput.value);

      if (quantity > 1) {
        quantityInput.value = quantity - 1;
      }
    }

    function increaseQuantity() {
      let quantityInput = document.getElementById('quantity-input');
      let quantity = parseInt(quantityInput.value);
      quantityInput.value = quantity + 1;
    }
  </script>
  <script>
    async function  callAddCart(){

        let productId = '${productDetail.id}';
        let quantityInput = document.querySelector('.product_info-amount-input');
        let quantity = quantityInput.value;

      const data = {
        productId: productId,
        quantity: quantity
      };
      const response = await fetch('https://localhost:8080/FreshMarket/api/carts/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      const res = await response.json();
      if (res == "1") {
        alert("Create successful!!");
        location.reload();
      } else {
        alert("the Product has already been added to Cart!!");
      }
    }

  </script>
</body>

</html>