document.getElementById('header_user').innerHTML = `
<div class="grid wide">
<nav class="header__navbar">
    <ul class="header__navbar-list">
        <li class="header__navbar-item">
            <span class="header__navbar-item-tittle">Kết nối với Fresh-MARKET</span>
            <a href="https://www.facebook.com/thanh.ngoc512/" class="header__navbar-item-link">
                <i class="header__navbar-icon fa-brands fa-facebook"></i>
            </a>
            <a href="https://www.instagram.com" class="header__navbar-item-link">
                <i class="header__navbar-icon fa-brands fa-instagram"></i>
            </a>
        </li>
        <li class="header__navbar-phone">
            <i class="fa-solid fa-phone"></i>
            <span class="header__navbar-phone-number">0352 306 136</span>
        </li>
    </ul>

    <div style="display: flex">
     <!-- before log in  -->
    <ul class="header__navbar-list-two">
        <a href="./help_center.jsp" class="header__suport">
            <i class="fa-regular fa-circle-question"></i>
            Hỗ Trợ Khách Hàng
        </a>
    </ul>

    <!-- after log in  -->
    <div class="header_menu">
        <div class="img_name-box">
            <img src="https://creazilla-store.fra1.digitaloceanspaces.com/icons/7916040/business-man-icon-sm.png" alt="" class="img_header">
            <p class="username_header" id="username"></p>
        </div>

        <ul class="header__navbar-user-menu">
            <li class="header__navbar-user-item">
                <a href="./profile.jsp">Tài khoản của tôi</a>
            </li>
            <li class="header__navbar-user-item header__navbar-user-item--separate">
                <a id="logout-user"">Đăng xuất</a>
            </li>
        </ul>
    </div>
    </div>
   
</nav>

<!-- header body  -->
<div class="header__body">
    <div class="header-bars" onclick="openMenuMobile()">
        <i class="header-bars-icon fa-solid fa-bars"></i>
    </div>

    <div class="header__logo">
        <a href="./" class="header__logo-link">
            <img src="./assests/img/logo_test.png" alt="" class="header__logo-img" />
        </a>
    </div>

    <div class="header__search">
        <div class="header__search-input-wrap">
            <input type="text" class="header__search-input" id="search-input-user" placeholder="Tìm kiếm trên G10-Market " />
        </div>

        <button class="header__search-btn" id="search-user">
            <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
        </button>
    </div>

    <a href="./cart.jsp" class="header__cart">
        <i class="header__cart-icon fa-solid fa-cart-plus"></i>
        <div class="header__cart-list">
            <img src="./assests/img/empty_cart_2.png" alt="" class="header__cart-list-img-empty" />
            <span class="header__cart-list-img-empty-tittle">Oops, giỏ hàng trống</span>
        </div>
    </a>
</div>
</div>
`

const searchBox_user = document.getElementById('search-input-user');
searchBox_user.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) { // Enter key pressed
        search_user();
    }
});

const searchButton_user = document.getElementById('search-user');
searchButton_user.addEventListener('click', search_user);

function search_user() {
    const keyword = document.getElementById('search-input-user').value;
    if (keyword) {
        window.location.href = 'resultSearch?search=' + encodeURIComponent(keyword);
    }
}

const logout_user = document.getElementById('logout-user');
logout_user.addEventListener('click', logout);
async function logout() {
    const logoutUrl = 'https://localhost:8080/FreshMarket/api/authen/logout';
    await fetch(logoutUrl);
    window.location.href = './';
}