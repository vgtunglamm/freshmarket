<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>G10 MARKET</title>
    <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png" />
    <!--reset css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
    <link rel="stylesheet" href="./assests/css/grid.css" />
    <link rel="stylesheet" href="./assests/css/base.css" />
    <link rel="stylesheet" href="./assests/css/main.css" />
    <link rel="stylesheet" href="./assests/css/responsive.css" />
    <link rel="stylesheet" href="./modal/modal.css" />
    <link rel="stylesheet" href="./header/header.css">
    <link rel="stylesheet" href="./footer/footer.css">
    <link rel="stylesheet" href="./chatbox/chatbox.css">
    <link rel="stylesheet" href="./toast_messages/toast.css">
    <link rel="stylesheet" href="./contact_page/contact.css">
    <link rel="stylesheet" href="./assests/css/cart.css">
    <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/cart_responsive.css">
    <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css" />
    <!--Nhúng font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
          rel="stylesheet" />
    <style>
        table{
            font-size: 18px;
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            padding: 20px;

        }

        h1 {
            text-align: center;
        }

        #cart-table {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 20px;
        }

        #cart-table th{
            border: 1px solid #ddd;
            padding: 10px 8px;
            text-align: center;
        }
        #cart-table td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        #cart-table th {
            background-color: #80BB35;
            color:white;
            /*padding:5px 0px;*/
            /*height:30px;*/
        }

        #cart-table td img {
            max-width: 100px;
            max-height: 100px;
        }

        #cart-table tr:hover {
            background-color: #eaf2ff;
        }

        #cart-table td:hover {
            cursor: pointer;
        }

        #total-price {
            font-size:20px;
            text-align: right;
            justify-content:end;
            display:flex;
            margin-bottom: 20px;
            background-color: #f2f2f2;
            height:45px;
            width:100%;
            float:right;
            padding: 15px 80px 15px 30px;
            color:#80BB35;
        }
        #pay {
            font-size:20px;
            text-align: right;
            justify-content:end;
            display:flex;
            margin-bottom: 20px;
            /*background-color: #f2f2f2;*/
            /*height:45px;*/
            width:100%;
            /*float:right;*/
            margin-right: 75px;
            /*color:#80BB35;*/
        }


        #total-price strong {
            font-weight: bold;
            padding-right: 5px;

        }

        #pay strong {
            border-radius: 10px;
            /*padding: 10px ;*/
            width: 130px;
            height:50px;
            font-weight: bold;
            padding-right: 10px;
            background-color: #80BB35;
            color:white;
            padding-left: 10px;;
            padding-top:10px;
            padding-bottom: 10px;
            margin-top:20px;

        }
        #pay strong:hover{
            cursor: pointer;
            background-color: #8dbe4d;
        }
        input[type="number"] {
            width: 60px;
        }

        input[type="checkbox"] {
            transform: scale(1.2);
        }

        .quantity{
            border:1px solid #ccc;
            padding:4px;
        }
        .xoa:hover{
            cursor: pointer;
        }
        a{
            text-decoration: none;
        }
        #remove-all button{
            border: 1px solid orangered;
            border-radius: 10px;
            /*padding: 10px ;*/
            width: 200px;
            height:40px;
            font-weight: bold;
            padding-right: 5px;
            /*background-color: #80BB35;*/
            margin-bottom: 20px;
            color:orangered;
            /*padding-left: 5px;;*/
            padding-top:3px;
        }
        #remove-all button:hover{
            cursor: pointer;
            /*background-color: #8dbe4d;*/
            background-color: #80BB35;
            color:white;
            border:none;
        }
    </style>
</head>

<body>
<div class="app">
    <c:if test="${sessionScope.role != null && sessionScope.role != 'admin'}">
        <header class="header" id="header_user">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>
    <c:if test="${sessionScope.role == 'admin'}">
        <header class="header" id="header_admin">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>
    <c:if test="${sessionScope.role == null}">
        <header class="header" id="header">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>

    <!-- category mobile -->
    <div class="menu_mobile" id="menu_mobile">
        <div class="menu_close">
            <i class="menu_close-icon fa-solid fa-xmark" onclick="closeMenuMobile()"></i>
        </div>

        <div class="header__search-mobile">
            <div class="header__search-input-wrap">
                <input type="text" class="header__search-input" placeholder="Tìm kiếm trên G10 Market">
            </div>

            <button class="header__search-btn">
                <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
            </button>
        </div>

        <nav class="category_mobile">
            <div class="category__heading">
                <h3 class="category__heading-tittle">DANH MỤC</h3>
            </div>
            <ul class="category__list">
                <c:forEach items="${categories}" var="cate">
                    <li class="category__list-tittle">
                        <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </div>

    <div class="app_container">
        <!-- Ngăn cách với header  -->
        <div style="padding-top: 15px; background-color: #f5f5f5"></div>

        <div class="grid wide">
            <div class="col l-12">
                <div class="contact_nav">
                    <a href="./" class="back_homepage-nav cart_nav">Trang chủ</a>
                    <h3 class="contact_seperate" style="font-size: 2.3rem; margin: 0 8px 26px 8px;">/</h3>
                    <a href="" class="contact_page-nav" style="font-size: 2.3rem; margin: 0 0 26px 0;">Giỏ hàng của
                        bạn</a>
                </div>
            </div>

            <div class="row cart_wrap">
                <c:choose>
                    <c:when test="${sessionScope.userId == null}">
                        <div class="cart_info">
                            <img src="./assests/img/empty-order.png" alt="" class="cart_img">
                            <p class="cart_title">Không có sản phẩm nào trong giỏ hàng</p>
                            <a href="./" class="cart_link">
                                <div class="btn btn--primary cart_btn">Tiếp tục mua sắm</div>
                            </a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table id="cart-table">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Hình ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng</th>
                                <th>Xóa sản phẩm</th>

                            </tr>
                            </thead>
                            <tbody id="cart-items">
                            <!-- Các sản phẩm trong giỏ hàng sẽ được thêm vào đây -->
                            </tbody>
                        </table>
                        <div id="remove-all">
                            <button onclick="deleteAllProduct()"> Xóa tất cả  sản phẩm</button>
                        </div>
                        <div id="total-price">
                            <strong>Tổng tiền:  </strong>
                            <span id="cart-total">0</span>
                        </div>
                        <div id="pay">
                            <strong onclick="payment()"> Thanh toán  </strong>
                        </div>

                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>

    <!-- Ngăn cách content với footer -->
    <div style="padding-bottom: 70px; background-color: #f5f5f5"></div>

    <footer class="footer" id="footer">
        <!-- được render từ file footer.js  -->
    </footer>
</div>

<!-- modal  -->
<div id="modal_wrap">
    <!-- render từ file modal.js  -->
</div>
<!-- chat message  -->
<div id="chat-box">
    <!-- render từ file chatbox.js  -->
</div>
<!-- toast message  -->
<div id="toast">
    <!-- render từ file toast.js  -->
</div>

<!-- render header -->
<script src="./header/header.js"></script>
<script src="./header/header_admin.js"></script>
<script src="header/header_user.js"></script>
<!-- render toast message  -->
<script src="./toast_messages/toast.js"></script>
<!-- render modal  -->
<script src="./modal/modal.js"></script>
<!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
<script src="./javascript/logIn_and_Signup.js"></script>
<!-- import validator đăng ký, đăng nhập  -->
<script src="./javascript/validator.js"></script>
<!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
<script>
    const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
    const headerMenu = document.querySelector('.header_menu');
    var emailReg;
    var passwordReg;
    var emailLog;
    var passwordLog;
    // Đăng ký
    Validator({
        form: "#form-1",
        formGroupSelector: ".form-group",
        errorSelector: ".form-message",
        rules: [
            // Validator.isRequired('#fullname'),
            Validator.isRequired("#email"),
            Validator.isEmail("#email"),
            Validator.minLength("#password", 6),
            Validator.isRequired("#password_confirmation"),
            Validator.isConfirmed(
                "#password_confirmation",
                function () {
                    return document.querySelector("#form-1 #password").value;
                },
                "Mật khẩu nhập lại không khớp"
            ),
        ],
        onSubmit: async function (evt) {

            emailReg = evt.email;
            passwordReg = evt.password;

            let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
            const response = await fetch(url);
            const res = await response.json();

            if (res != "0") {
                showSuccessSignUpExistEmailToast();
                regModal.classList.add('open');
            } else {
                const data = {
                    email: emailReg,
                    passWord: passwordReg
                };
                const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const res = await response.json();
                if (res == "1") {
                    showSuccessSignUpToast();
                    logModal.classList.add('open');
                    regModal.classList.remove('open');
                } else {
                    showFaiedSignUpToast();
                    // regModal.classList.add('open');
                }
            }
        },
    });
    //Đăng nhập
    Validator({
        form: "#form-2",
        formGroupSelector: ".form-group",
        errorSelector: ".form-message",
        rules: [
            Validator.isRequired("#email"),
            Validator.isEmail("#email"),
            Validator.minLength("#password", 6),
        ],
        onSubmit: async function (data) {
            emailLog = data.email;
            passwordLog = data.password;

            const obj = {
                email: emailLog,
                passWord: passwordLog
            };
            const response = await fetch('https://localhost:8080/FreshMarket/api/authen/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(obj)
            });
            const res = await response.json();
            if (res == "0") {
                showErrorLogInToast();
                logModal.classList.add('open');
            }
            else {
                showSuccessLogInToast();
                window.location.href = "https://localhost:8080/FreshMarket/";
            }
        },
    });
</script>
<!-- xử lý trượt slider  -->
<script src="./javascript/slider.js"></script>
<!-- Time sale countDown -->
<script src="./javascript/countDownTime.js"></script>
<!-- object product 1  -->
<script src="./javascript/mainProduct.js"></script>
<!-- Đóng mở chat hỗ trợ trực tuyến -->
<script>
    function openChat() {
        document.getElementById("myChat").style.display = "block";
    }
    function closeChat() {
        document.getElementById("myChat").style.display = "none";
    }
</script>
<!-- Đóng mở menu mobile -->
<script>
    function openMenuMobile() {
        document.getElementById("menu_mobile").style.display = "block";
    }
    function closeMenuMobile() {
        document.getElementById("menu_mobile").style.display = "none";
    }
</script>
<!-- render chatbox  -->
<script src="./chatbox/chatbox.js"></script>
<!-- render footer -->
<script src="./footer/footer.js"></script>
<script>
    // Dữ liệu mẫu giỏ hàng
    var cartItems = [];
    let catrUrl = "https://localhost:8080/FreshMarket/api/carts/get-all" ;
    fetch(catrUrl, {
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            data.forEach(cart => {
                let id = cart.id;
                let quantity = cart.quantity;
                let image = cart.image;
                let price = cart.price;
                let productName = cart.productName;
                cartItems.push({id: id, name: productName, price: price, quantity: quantity ,image : image,buy: false});
            });
            // Hiển thị giỏ hàng khi trang được tải
            displayCart();
        })
        .catch(error => {
            console.error(error);
        });
    // Hiển thị giỏ hàng
    function displayCart() {
        var cartTable = document.getElementById("cart-items");
        var cartTotal = document.getElementById("cart-total");
        var totalPrice = 0;

        // Xóa các dòng hiện tại trong bảng
        while (cartTable.firstChild) {
            cartTable.removeChild(cartTable.firstChild);
        }

        // Thêm dữ liệu từ giỏ hàng vào bảng
        cartItems.forEach(function(item, index) {
            var row = document.createElement("tr");
            var buyCell = document.createElement("td");
            var imageCell = document.createElement("td");
            var nameCell = document.createElement("td");
            var priceCell = document.createElement("td");
            var quantityCell = document.createElement("td");
            var totalCell = document.createElement("td");
            var removeCell = document.createElement("td");


            imageCell.innerHTML = "<img src='" + item.image + "' alt='Hình ảnh sản phẩm'>";
            nameCell.textContent = item.name;
            priceCell.textContent = item.price;

            // Input số lượng
            var quantityInput = document.createElement("input");
            quantityInput.classList.add("quantity");
            quantityInput.type = "number";
            quantityInput.value = item.quantity;
            quantityInput.min = 0;
            quantityInput.dataset.index = index;
            quantityInput.addEventListener("input", updateQuantity);
            quantityCell.appendChild(quantityInput);

            // Tính toán và hiển thị tổng tiền cho sản phẩm
            var total = 0;
            if (item.buy) {
                total = item.price * item.quantity;
            }
            totalCell.textContent = total;
            totalPrice += total;

            // Checkbox mua
            var buyCheckbox = document.createElement("input");
            buyCheckbox.type = "checkbox";
            buyCheckbox.checked = item.buy;
            buyCheckbox.dataset.index = index;
            buyCheckbox.addEventListener("change", updateBuy);
            buyCell.appendChild(buyCheckbox);

            // button remove sản phẩm
            var removeSp= document.createElement("button");
            removeSp.textContent ="Xóa";
            removeSp.id ='remove';
            removeSp.onclick = function() {
                deleteProduct(item.id);
            };
            removeCell.appendChild(removeSp);

            row.appendChild(buyCell);
            row.appendChild(imageCell);
            row.appendChild(nameCell);
            row.appendChild(priceCell);
            row.appendChild(quantityCell);
            row.appendChild(totalCell);
            row.appendChild(removeCell);

            cartTable.appendChild(row);
        });

        // Cập nhật tổng tiền
        cartTotal.textContent = totalPrice;
    }

    // Cập nhật số lượng
    function updateQuantity(event) {
        var index = event.target.dataset.index;
        var quantity = parseInt(event.target.value);

        if (quantity >= 0) {
            cartItems[index].quantity = quantity;
            displayCart();
        }
    }

    // Cập nhật trạng thái mua
    function updateBuy(event) {
        var index = event.target.dataset.index;
        var buy = event.target.checked;

        cartItems[index].buy = buy;
        displayCart();
    }

    async function deleteProduct(id){
        const response = await fetch('https://localhost:8080/FreshMarket/api/carts/delete/?id=' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await response.json();
        if (res == "1") {
            alert("Delete successful");
            location.reload();
        } else {
            alert("Delete failed");
        }
    }

    async function deleteAllProduct(){
        const response = await fetch("https://localhost:8080/FreshMarket/api/carts/delete-all", {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await response.json();
        if (res == "1") {
            alert("Delete successful");
            location.reload();
        } else {
            alert("Delete failed");
        }
    }

    function payment(){
        window.location.href = "./payment.jsp";
    }

</script>
</body>

</html>