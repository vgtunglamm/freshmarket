<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 800px;*/
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
            .logo{
                margin-left:10px;
                padding-top:18px;
                padding-bottom:28px;
                color:#80BB35;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                 .noti-icon{
                    display:flex;
                     position: absolute;
                    right:10px;
                    /*margin-left:700px;*/
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                 .report{
                    width:101%;
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
               .report button{
                    margin-top:30px;
                     position: absolute;
                    right:60px;
                    /*margin-left:850px;*/
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 300px;
                    height:1080px;
                }

                .co-left-top{
                   
                }
                .co-right{
                    border-radius: 5px;
                    height: 640px;
                    width:450px;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 0px ;
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 150px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                .product{
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  20px;
                    padding: 0;
                    display: block;
                    background-color: white;
/*                    height:300px;
                    */width:100%;
                    margin-right:15px;
                    display:block;
                    justify-content: center;
                    text-align: center;
                }
                .product-list{
                    
                 background-color: #F5F5F5;
                    width:1490px;
                    border-radius: 5px;
                    left:0px;
                    margin:-40px 30px ;
                    /*padding-left:  50px;*/
                    padding:20px;
                    height: 400px;
                }
                #product-list img{
                    height:200px;
                    width:200px;
                }
                .co-right button{
                    width:92%;
                    height:50px;
                    margin:10px;
                    border-radius: 50px;
                    border:none;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    color:white;
                    font-weight: bold;
                    font-size: 20px;
                }
                .product{
                    cursor: pointer;
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  10px;
                    border-radius: 10px;
                    padding-top: 5px;
                    display: flex;;
                    background-color: white;
                    height:60px;
                    /*width:100px;*/
                    margin-right:20px;
               
                }
                .product:hover{
                     background-image: linear-gradient(0, #80BB35, #80BB35);
                     color:white;
                }
              
                
                  button:hover{
                     cursor: pointer;
                 }
                .title{
                    font-weight: bold;
                    font-size:14px;
                    display:flex;
                    background-color: #F5F5F5;
                    width:1440px;
                    border-radius: 5px;
                    left:0px;
                    
                    margin:-40px 30px 20px 30px ;
                    /*margin-right: 300px;*/
                    padding:10px 50px 20px 50px;
                }
                .title p{
                      width: 23.1%;
                    /*padding-right: 250px;*/
                }

                .container-one{
                   background-color:#5C9A2D;
                    top:0;
                  margin-top: -40px;
                  margin-left: 2%;
                  border-radius: 10px;
                  width:96%;
                }
                
                .list{
                    background-image: linear-gradient(to bottom, #F2EFE8, #F0FDE4);
                }
                
                th{
                    color: #fff;
                }
                
                td{
                    text-align: center;
                    /*margin-left: -20px;*/
                    /*display:block;
                    justify-content: center;
                    align-items: center;*/
                    padding-top:10px;
                    border-radius: 5px;
                    height:40px;
                }
                
               
                .product-list tr{
                    /*width:1450px*/
                    padding-right:100px;
                }
                
                thead tr{
                    height:40px;
                    font-weight: bold;
                }
               
                
                .highlight tbody tr:hover {
                    background-color: #BDD89D;
                    cursor: pointer;
                }
                td img{
                    border-radius: 50%;
                }
                .list-p{
                    justify-content: space-between;
                    display: flex;
                }
                .progress-bar {
                    border-radius: 10px;
                    height: 10px;
                    width: 80%;
                    margin-left: 10%;
                    margin-top: -10px;
                    margin-bottom: 10px;
                }
/*                .phone{
                    margin-left: -40px;
                }*/
                .phonem{
                    /*padding-right: 100px;*/
                    margin-left: -60px;
                 }
                 .name{
                     padding-left: 60px;
                 }
                 .email{
                     padding-left: 45px;
                 }
                 thead{
                     padding-bottom: 20px;
                 }
                 .ma{
                     padding-right:250px;
                     /*border-right: 1px solid #ccc;*/
                 }
                  .gia{
                      width:100px;
                 }
                  .mt{
                      width:100px;
                 }
                 #product-title{
                       height: auto;
                    background-color: #F5F5F5;
                    width:94%;
                    border-radius: 5px;
                    left:0px;
/*                    left
                    */margin:-40px 30px ;
                    padding:20px;
                    /*position: relative;*/
                    display:flex;
                    font-weight: bold;
                }
                .idtitle{
                     padding:  0 250px 0 180px;
                }
                .nametitle{
                     padding-right: 190px;
                }
                .pricetitle{
                     width:380px;
                     
                }
                .mttitle{
                     width:400px;
                }
                .highlight{
                    padding-left: 8px;
                }
                
                .select{
                    display: flex;
                    justify-content: right;
                    align-items: center;
                    padding: -20px;
                    padding-bottom: 10px;
                }

                .select button{
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    padding: 10px;
                    border:none;
                    color:white;
                    border-radius:12px;
                    margin-right: 30px;
                    width: 70px;
                    height: 45px;
                }

                .done-button:hover{
                    /*padding:20px;*/
                    /*display:block;*/
                    cursor: pointer;
                }
                

                button:hover{
                    cursor: pointer;
                }

                /*///////////////////*/
                 .overlay {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }
    
     .overlay1 {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }

    /* CSS cho form */
    #formContainer {
      position: fixed;

      height:340px;

      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
    
    #formContainer1 {
        position: fixed;
        height:350px;
        width:1000px;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: #fff;
        padding: 10px 20px 20px 20px;
        z-index: 10000;
        display: none;
    }
 
    /* CSS cho nút đóng */
    #closeButton {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input{
        display:flex;
    }
  
   .form-left1{
        width:500px;
        height: auto;
    }
    .form-right1{
        width:500px;
        height: auto;
    }
    .title-product{
        display:flex;
    }
    .ctr-input input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    .ctr-button{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
    .ctr-button input:hover{
        cursor: pointer;
    }
    .manager-user{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload{
        display:none;
        padding-bottom:5px;
    }
    
    /**CSS cho add product*/
     #closeButton1 {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input1{
        /*display:flex;*/
        height:auto;
    }
  
    .form-left1{
        width:500px;
    }
    .form-right1{
        width:500px;
    }
    .title-product1{
        display:flex;
    }
    .ctr-input1 input{
        display: flex;
        padding-right:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:80%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
      .ctr-button1 button{
           padding: 5px;
    height: 36px;
    border: 2px solid red;
    color: red;
    /* margin-top: 10px; */
    margin-right: 60px;
    width: 100px;
    border-radius: 10px;
        
    }
    .ctr-button1 button:hover{
        border:none;
    }
    .ctr-button1{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button1 input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
    .ctr-button1 input:hover{
        cursor: pointer;
    }
    .manager-user1{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload1{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload1{
        display:none;
        padding-bottom:5px;
    }
    .desss{
         width: 300px; /* Độ rộng của ô (cell) */
         height:auto;
   display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
    }
     li a:hover{
        color:#80BB35;
        font-size:20px;
        transition: 0.3s;
        /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
        /*color: white;*/
        
    }
     #brandID{
        height:35px;
         /*width:93%;*/
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    .progress-bar{
        /*width:700px;*/
        background: linear-gradient(to right, #6DAE3C 0%, #6DAE3C 90%,#fff 0%, #fff 100%);
    }
    .list-p{
      
    }
    .list-p1{
        margin-left: 10%;
    }
    .list-p2{
        margin-right: 10%;
    }
   
   
    .content img{
        height:50px;
        padding: 0px 10px 0px 10px;
    }
    table tr td:nth-child(1) {
  /* Áp dụng CSS cho ô thứ tự 1 */
        padding:0px 20px 0px 20px;
      }
      table tr td:nth-child(3) {
  /* Áp dụng CSS cho ô thứ tự 1 */
        width:auto;
         padding:0px 20px 0px 20px;
      }
       table tr td:nth-child(4) {
  /* Áp dụng CSS cho ô thứ tự 1 */
        width:1090px;;
         padding:0px 20px 0px 20px;
      }
       #brandID{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    li a:hover{
            transition: all 0.5s ease;
            display: flex;
            padding: 12px 12px;
            font-weight: bold;
            font-size: 18px;
            color: #80bb35;
            background-color: rgba(0, 0, 0, 0.05);
            border-left: 3px solid #80bb35;
    }
        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <h2>G10 MARKET</h2>
                    <!--<img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>-->
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Users</a>
                    </li>
                </ul>

            </div>
            
            
            
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="products.jsp">Categories</a>
                </div>                
                <ul>
                    <c:forEach items="${categories}" var="cate">
                        <li><a class="active" href="products.jsp?cateId=${cate.id}">${cate.name}</a></li>
                    </c:forEach>
                </ul>
            </div>
            
            
              <div class="brands">
                <div class="b-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="listBrand.jsp">Brand</a>
                </div>                
             
            </div>
            
            
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                    <li>
                        <a class="active" href="wallet_admin.jsp">Wallet</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>
                  <input type="text" id="search" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                        <h1>Brand</h1>

                        <p> Tất cả nhãn hàng</p>

                    </div>
                    <div>
                        <!--Button thêm product-->
                        <button onclick="addBrand()" type="button">Add Brand</button>
                    </div>
                        
                        
                        <div id="overlay1" class="overlay1"></div>

                <div id="formContainer1">
                    <div class="title-product1">
                        <h2> Add Brand</h2>
                        <span id="closeButton1" onclick="hideForm1()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user1">
                      <div class="ctr-input1">
                            
                            
                         

                            <label for="brandID1">Brand Id</label>


                            <input type="text" id="brandID1" name="number">

                            <label for="brandName1">Brand Name</label>

                            <input type="text" id="brandName1" name="phone"><br><br>

                           

                      </div>
                      <div class="ctr-button1">
                          <button onclick="callCreateAPI()" type="button">Add</button>
                     
                          <input onclick="hideForm1()" type="submit" value="Cancel">
                      </div>


                  </form>
                </div>
                        
                        
                    </div>
                   </div>
                </div>

                <div class="container-one">
                <table class="highlight">
                    <thead>
                        <tr>
                            <th>Brand Id</th>
                            <th> Brand Flag </th>
                            <th>Brand Name</th>  
                            <th> %</th>
                        </tr>
                    </thead>
                     
                    <!--Render brand-->
                    <tbody id="brand-list" class="list">
                         <tr onclick="showBrandInfo(this)">
                            <td >ID1234567</td>
                            <td>
                                <div class="content">
                                    <img src="https://png.pngtree.com/png-clipart/20220923/ourmid/pngtree-vietnam-flag-png-image_6212677.png"> 
                                   
                                </div>
                            </td>
                            <td> TEN BRAND </td>
                            <td>
                                  <p class="list-p2">25%</p>
                                        
                                <div class="progress-bar" >   </div>
                            </td>
                        </tr>
                        <tr onclick="showBrandInfo(this)">
                            <td >ID1234567</td>
                            <td>
                                <div class="content">
                                    <img src="https://png.pngtree.com/png-clipart/20220923/ourmid/pngtree-vietnam-flag-png-image_6212677.png"> 
                                   
                                </div>
                            </td>
                            <td>
                              TEN BRAND                          
                            </td>
                            <td>
                                  <p class="list-p2">25%</p>
                                        
                                <div class="progress-bar" >   </div>
                            </td>
                        </tr>
                    </tbody>
                    

                </table>
                    

                <div id="overlay" class="overlay"></div>

                <div id="formContainer">
                    <div class="title-product">
                        <h2>Brand Detail</h2>
                        <span id="closeButton" onclick="hideForm()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user">
                      <div class="ctr-input">
                          <div class="form-left">

                            <label for="brandID">Brand Id</label>
                            <br>
                            <input type="text" id="brandID" name="number" readonly>
                            
                            
                            <br><br>

                            <label for="brand">Brand Name</label>

                            <input type="text" id="brand" name="phone"><br><br>

                           

                      </div>
                      </div>
                      <div class="ctr-button">
                          <input type="submit" value="Update">
                          <input type="submit" value="Delete">
                          <input onclick="hideForm()" type="submit" value="Cancel">
                      </div>


                  </form>
                </div>
                    
             
            </div>
          <script src="./javascript_admin/showBrandInfo.js"></script>
           <script src="./javascript_admin/addBrand.js"></script>
           <script src="./javascript_admin/dynamicPagination.js"></script>
        </div>
               
    </body>
</html>
<script>
    // Change brand by brandID   
    const data = [
  { id: 'option1', brand: 'Nike' },
  { id: 'option2', brand: 'Adidas' },
  { id: 'option3', brand: 'Puma' },
  { id: 'Option 5', brand: 'Reebok' },
  { id: 'Option 6', brand: 'Vans' }
];

    function updateInputValue() {
        
  var selectElement = document.getElementById("brandID");
  var inputElement = document.getElementById("brand");
  var selectedId = selectElement.value;
  
  var brandSelect;
   for (var i = 0; i < data.length; i++) {
    if (data[i].id === selectedId) {
      brandSelect= data[i].brand;
      break;
    }
  }

   
      inputElement.value = brandSelect;
    
   
//   selectBrand.value = selectedData ? selectedData.brand : '';
    console.log(brandSelect);
//  inputElement.value = selectedOption;
}

</script>
<script>
    const urlParams = new URLSearchParams(window.location.search);
    const cateId = urlParams.get("cateId");
    console.log(cateId);

    let url = "https://localhost:8080/FreshMarket/api/products/get-by-cate/?id=" + cateId;
          fetch(url)
        .then(response => response.json())
        .then(data => {
            let productList = document.getElementById("product-list");

            data.forEach(product => {
                let id = product.id;
                let categoryId = product.cateId;
                let brandId = product.brandId;
                let productName = product.productName;
                let color = product.color;
                let price = product.price;
                let brand = product.brand;
                let description = product.description;
                let transportMethod = product.transportMethod;
                let status = product.Status;
                let quantity = product.quantity;
                let image = product.image;

                let row = document.createElement("tr");
                row.classList.add('item');
                row.onclick = function () {
                    showProductsInfo(this);
                };

                let idCell = document.createElement("td");
                idCell.innerText = id;
                row.appendChild(idCell);

                let categoryIdCell = document.createElement("td");
                categoryIdCell.innerText = categoryId;
                row.appendChild(categoryIdCell);

                let brandIdCell = document.createElement("td");
                brandIdCell.innerText = brandId;
                row.appendChild(brandIdCell);

                let productNameCell = document.createElement("td");
                productNameCell.innerText = productName;
                row.appendChild(productNameCell);

                let colorCell = document.createElement("td");
                colorCell.innerText = color;
                row.appendChild(colorCell);

                let priceCell = document.createElement("td");
                priceCell.innerText = price;
                row.appendChild(priceCell);

                let brandCell = document.createElement("td");
                brandCell.innerText = brand;
                row.appendChild(brandCell);

                let descriptionCell = document.createElement("td");
                descriptionCell.classList.add('desss');
                descriptionCell.innerText = description;            
                row.appendChild(descriptionCell);

                let transportMethodCell = document.createElement("td");
                transportMethodCell.innerText = transportMethod;
                row.appendChild(transportMethodCell);

                let statusCell = document.createElement("td");
                statusCell.innerText = status ? "Active" : "Inactive";
                row.appendChild(statusCell);

                let quantityCell = document.createElement("td");
                quantityCell.innerText = quantity;
                row.appendChild(quantityCell);

                let imageCell = document.createElement("td");
                imageCell.innerText = image;
                row.appendChild(imageCell);
                imageCell.style.display = "none";

                productList.appendChild(row);
            });
        }
                
        
            
            )
        .catch(error => {
            console.error(error);
        });
        
        
        
</script>
