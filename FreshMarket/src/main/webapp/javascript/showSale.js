// Tạo một mảng chứa thông tin của các sản phẩm
var products = [
  {
    name: "Apple",
    price: 1.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
  {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
  
  {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
  {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
  {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
    {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
    {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
    {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
    {
    name: "Banana",
    price: 0.99,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  },
  {
    name: "Orange",
    price: 1.49,
    image: "./assests/img/nam_cac_loai/npd1.webp"
  }
];

// Lặp qua mảng sản phẩm và tạo các phần tử HTML động
var productList = document.getElementById("product-list");

for (var i = 0; i < products.length; i++) {
  // Tạo một div chứa thông tin của một sản phẩm
  var productDiv = document.createElement("div");
  productDiv.classList.add("product");
  
  // Thêm hình ảnh của sản phẩm
  var productImage = document.createElement("img");
  productImage.src = products[i].image;
  productDiv.appendChild(productImage);
  
  // Thêm tên của sản phẩm
  var productName = document.createElement("h3");
  productName.textContent = products[i].name;
  productDiv.appendChild(productName);
  
  // Thêm giá của sản phẩm
  var productPrice = document.createElement("p");
  productPrice.textContent = "$" + products[i].price.toFixed(2);
  productDiv.appendChild(productPrice);
  
  // Thêm sản phẩm vào danh sách sản phẩm
  productList.appendChild(productDiv);
}
