///* 
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
// */
//
const products = [
      {
        productName: "Regular White T-Shirt",
        category: "Topwear",
        price: "30",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Beige black Skirt",
        category: "Bottomwear",
        price: "49",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Sporty SmartWatch",
        category: "Watch",
        price: "99",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Basic Knitted Top",
        category: "Topwear",
        price: "29",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Black Leather Jacket",
        category: "Jacket",
        price: "129",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Stylish Pink Trousers",
        category: "Bottomwear",
        price: "89",
        image: "./assests/img/npd1.webp"
      },
      {
        productName: "Brown Men's Jacket",
        category: "Jacket",
        price: "189",
       image: "./assests/img/npd1.webp"
      },
      {
        productName: "Comfy Gray Pants",
        category: "Bottomwear",
        price: "49",
        image: "./assests/img/npd1.webp"
      }
    ];
  
  
  
const params = new URLSearchParams(window.location.search);
    const searchTerm = params.get('search');

    const productlist = document.getElementById('products-list');
    
    const filteredProducts = products.filter(function(product) {
      return product.productName.toLowerCase().includes(searchTerm.toLowerCase());
    });


    
    
    
    filteredProducts.forEach(function(product) {
  const productDiv = document.createElement('div');
  productDiv.classList.add('product');

  const img = document.createElement('img');
  img.src = product.image;

  const productInfo = document.createElement('div');
  productInfo.classList.add('product-info');

  const productName = document.createElement('div');
  productName.classList.add('product-name');
  productName.textContent = product.productName;

  const productPrice = document.createElement('div');
  productPrice.classList.add('product-price');
  productPrice.textContent = '$' + product.price;
  
  const buttonWrapper = document.createElement('div');
  buttonWrapper.classList.add('home__product-item-btn-wrap');
  
  const myLink = document.createElement('a');
  myLink.setAttribute("href", "productInformation.jsp");
  
  
  const viewButton = document.createElement('button');
  viewButton.classList.add('view-button');
  viewButton.textContent = 'Xem chi tiết';


  buttonWrapper.appendChild(viewButton);
  myLink.appendChild(buttonWrapper);
  productInfo.appendChild(productName);
  productInfo.appendChild(productPrice);
  productInfo.appendChild(myLink);

  productDiv.appendChild(img);
  productDiv.appendChild(productInfo);

  productlist.appendChild(productDiv);
});










//const searchParams = new URLSearchParams(window.location.search);
//    const searchKeyword = searchParams.get('search');
//
//    // Hiển thị kết quả tìm kiếm
//    const searchResults = document.getElementById('search-results');
//    searchResults.innerHTML = `Kết quả tìm kiếm cho: ${searchKeyword}`;


 // Lấy tham số tìm kiếm từ URL
//    const searchParams = new URLSearchParams(window.location.search);
//    const searchKeyword = searchParams.get('search');
//
//    // Hiển thị kết quả tìm kiếm
//    const searchResults = document.getElementById('search-results');
//    searchResults.innerHTML = `Kết quả tìm kiếm cho: ${searchKeyword}`;
