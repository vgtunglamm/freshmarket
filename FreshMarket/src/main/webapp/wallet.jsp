<%-- 
    Document   : profile
    Created on : May 17, 2023, 7:11:47 PM
    Author     : ThanhNgoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <style>
            body{
                font-family: Helvetica, Arial, sans-serif;
               background-image: linear-gradient(0, #80BB35, #80BB35);
            }
            .container{
                position: fixed;
                height:650px;
                width:1400px;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: #fff;
                padding: 10px 20px 20px 0px;
                display:flex;
            }
            .list-menu{
                width:22%;
                height:100%;
                border-right: 2px solid #ccc;
            }
            .list-menu .logo2{
                height: 150px;
                margin:30px 0px 0px 80px;
            }
            .list-menu h1{
                margin-bottom:50px;
                padding-left:60px;
            }
            .account, .password, .noti, .wallet{
                display:flex;
                padding-left:30px;
                /*border: 2px solid #ccc;*/
            }
            .account{
                 border-top: 2px solid #ccc;
            }
             .password{
                 border-top: 2px solid #ccc;
            }
            .wallet{
                 border-top: 2px solid #ccc;
                  background-image: linear-gradient(0, #80BB35, #80BB35);
                  color:white;
            }
            .noti{
                 border-top: 2px solid #ccc;
                  border-bottom: 2px solid #ccc;
            }
            .account{
                 border-top: 2px solid #ccc;
            }
            .account:hover{
                background-image: linear-gradient(0, #80BB35, #80BB35);
                cursor: pointer;
                color:white;
            }
            .password:hover{
               background-image: linear-gradient(0, #80BB35, #80BB35);
                cursor: pointer;
                color:white;
            }
            .noti:hover{
                background-image: linear-gradient(0, #80BB35, #80BB35);
                cursor: pointer;
                color:white;
            }
            .noti i{
                margin-top:20px;
                padding-right:15px;
            }
            .account i{
                margin-top:20px;
                padding-right:15px;
            }
            .password i{
                margin-top:20px;
                padding-right:15px;
            }
            .wallet i{
                margin-top:20px;
                padding-right:15px;
            }
            
            
           
    .function{
        padding-left: 30px;
        width:78%;
    }
    .account-settings{
        padding: 100px 0px 30px 10px;
    }
    a{
         text-decoration: none;
         color:black;
    }
    
    
    .change-password-form {
        width: 100%;
        margin: 0 auto;
      }

      .form-group {
        margin-bottom: 20px;
      }

      label {
        display: block;
        margin-bottom: 5px;
      }

      input[type="password"] {
        width: 50%;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 4px;
        margin:10px;
      }

      .change-password-form{
          padding-left:200px;
          padding-top:50px;
      }
      .submit{
           position:absolute;
          right:60px;
          margin-top:100px;
      }
      .update{
       
            padding:15px;
       border: none;
       color: white;
        width: 100px;
        border-radius: 10px;
         background-image: linear-gradient(0, #80BB35, #80BB35);
        
      }
      .update:hover{
           cursor: pointer;
      }
       .cancel{
         
          
            padding:15px;
       border: none;
       color: Black;
        width: 100px;
        border-radius: 10px;
         background-color: #ccc;
        margin-left:20px;
      }
      .cancel:hover{
           cursor: pointer;
             background-image: linear-gradient(0, #80BB35, #80BB35);
             color:white;
      }
      .logo{
           padding-right: 50px;
            display:flex;
             /*padding-top: 10px;*/
             padding-left: 30px;
      }
      .logo i{
          color:#80BB35;
          margin-top:23px;
                padding-right:15px;
                height:40px;
      }
      .logo .logo1{
          height:50px;
          /*padding-left: 50px;*/
          color: #80BB35;
         
       
       
      }
       .submit button{
           padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px; 
        height: 40px;
        margin:5px;
      }
      
      
      .wallet1 {
            /*display: flex;*/
            align-items: center;
            justify-content: space-between;
            /*border: 1px solid black;*/
            padding: 10px;
            margin-top:50px;
          }

          .main-account, .promo-account {
            display: flex;
            align-items: center;
          }

          .account-title {
            font-weight: bold;
            margin:20px 30px;
            width:300px;
          }

          .account-balance {
            font-size: 18px;
          }

          .add-funds-btn {
              display:flex;
            background-color: #388e3c;
            color: white;
            border: none;
            padding: 2px 8px;
            border-radius: 5px;
            cursor: pointer;
            font-weight: bold;
            margin:40px 0px 0px 30px;
          }
          .add-funds-btn img{
              height:38px;
              margin-right: 10px;
          }

          .add-funds-btn:hover {
            background-color: #1b5e20;
          }
          .contaner{
              display:flex;
              align-items: center;
            /*justify-content: center;*/
          }
        .modal {
           position: fixed;
           top: 0;
           bottom: 0;
           left: 0;
           right: 0;
           display: flex;
           animation: fadeIn 0.3s;
           }
            .modal_overlay {
                position: absolute;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.3);
                z-index: 0;
            }
            .modal_body {
                margin: auto;
                background-color: #fff;
                position: relative;
                z-index: 1;
                width: 800px;
                height:600px;
            }
            .add-funds-frm{
                display: none;
            }
            .fund-header{
                border-bottom: 1px solid #ccc;
                height: 60px;
                display: flex;
                justify-content: space-between;
                align-items: center;
                
            }
            .fund-header i{
                padding-right: 10px;
                font-size: 20px;
                cursor: pointer;
            }
            .fund-header h2{
                padding-left: 10px;
            }
            .fund-main{
                
            }
            .fund-main h2{
                text-align: center;
            }
            .fund-main-1{
                padding: 10px 5px;
            }
            .fund-main-1 input{
                margin-left: 100px;
                width: 50%;
                height: 20px;
                border-color: #ccc;
                border-radius: 3px;
                outline: none;
            }
            .fund-main-1 i{
                padding-left: 10px;
            }
            #remain{
                margin-left: 56px;
                font-weight: bold;
            }
            #sum{
                margin-left: 56px;
                color: #f33a58;
            }
            .payment{
                margin: auto;
                margin-top: 20px;
                padding-top: 10px;
                padding-left: 40px;
                width: 80%;
                height: 220px;
                background-color: #ebebeb;
            }
            .payment div{
                width: 90%;
                height: 40px;
                margin: 10px 10px;
                background-color: #fff;
                border-radius: 5px;
                box-shadow: 0px 3px 10px rgba(60,64,67,.3);
                align-items: center;
                display: flex;
            }
            .payment div img{
                padding:15px;
            }
            .fund-title{
                padding-left: 10px;
                margin: 10px 0;
            }
            #funds-account{
                
                margin:20px auto;
            }
        </style>
        
    </head>
    <body>
        <div class="container">
            <div class="list-menu">
                <a href="./">
                        <div class="logo">
                            <i class="fa fa-home"></i>
                            <h2 class="logo1"> G10 MARKET</h2>
                           
                        </div>
                </a>
                <img class="logo2" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABYlBMVEWPj8z/////xpU6LCRMPzjh9/nylVX/s38zMzGMjMuHh8n/yJfxklH/ypiRkc//x5STk86mptby8vnm/f/T0+ro6PQ1JRM4KR5CMSj6+v28vOCystubm9He3u/NzeeKj9D/yZGZmdAmHBkyJiD7toDExOOrq9h4daE0JBAmKyz9v4yFg7ltaIz2lU3/tXobJSn0nF/3p26clcZkXnzjsIUiGRfOoHmxn71PQj1IOSs+OTYmEABPRlQ+MS1YUGRGOz+Afq9ZRDWshWV+YUtwVkOzkafoq5GafGHRk4TJn3rTsK30wJummsKtubmopKQuGxKFf37c2tpuZmQgBABbUk/DwMGRi4pKQEjbqoCRcFZ2aoC0lIwTCgHCnIchFgnclHXmkmCihZDIko/IjWXtrY3HoaneqJnilG22obrjuKTGqrPbtKlgTkJYTVLQgEvh5d7t0Lnm3tHpzbXQ4uNoWWS1sbExoclrAAAS50lEQVR4nM2diVvbRhbAZYvYkWXLBxjbYDD44D5sYohNYg5DSik0ENIk0N222S7pJg1ptt3k/9/RYVuyZqT3RpLp+9qvDanU+eW9edeMZoRQ4FIoZGdmZ2tT0wvpdDElpIrp9ML0VG12diZbKAT/vxeCfHkmOzu9UIzFYpIhgi69X5LfKS5Mz2YzQQ4iKMLCTC1dlGImLLpooFIxXZsJSp1BEBYmplIqnCOaHTQ1NREEpd+EhWyNmCUGzoRJjLaW9ZvSV8LCzHQKpzu7LmOpaX8N1kdCDc8D3UCVBNK/YflFmKm5+RQUpCTV/HKwvhAWJtK+aM8CGUv743h8ICzU5n3n0xnnaz4weibMTPlonTZIacqzsXokzCwEoj4TY2zBI6Mnwsx0wHw647QnRg+EmekA7dPCKHlh5CesjUB/fcZYbeSEE6mYL2NXFP1vN4mlJkZKmE37xbe4t7//dE2AMKazoyOs+TMBFWVtfylHZGn/AIBI8pwREWZ9MVBimmviUk7UJLe0BkAkpsqhRjzhlD98q486S+JAlvYglirEpgInzBZ9ACTT72luMieaZfIpDLGIVSOScNZ7iCDm+agzhKchvgEhSrHZAAkLnl0owTvYW6LwqZNxfxWCSJwqKh/HEGZTHhVIZp/qPWl4GmIHhiihHA6CcMJbjCDqs8++IUQRiCghwj+ccNqLhSqKcrDnjIdBFGLTvhMWvPhQPTa44WEMlfhU6GQEEmbmuS1UDe1PJycBeDoiyKMSS50H1hswQm4fo1mn6GqdZsR9KCLQ34AIZzgtFG6dZkRYXCQSA/UcIYR8gJrvZIQ+R5l8CiSEIQIIJ3gAudRnyNIjMCIgargTznIAwmIDGxFUaWiI7imcKyEHoGae3HiqTELqRSCiGyHeRElwcMjMYAIOiwBDdSFEOxlG4YBGfAMldHU3zoRYQKK/HDS2O8vknl+IjoRZHCDh6/jDR2RpEY7oGPqdCDMpHODBG6/zzySIqSiknBI4B8ICKhdVhD0f5p8ZEW6n0rxDGu5AWEQBLvo0AQcCj4qCVOQhxNSDxIMuuQ8ZjQi3U4d6kUmICYTKwb7fClQlB05QncIiizCLMFFlzUcPYxaEnQoSy6EyCAvwgjAYC9WElMNwwhTD2zAI0wjAp0FYqC6T4CqDIKYxhPB0W1kNZAr2BOFsWEk4lRCeyyirYjBT0BBEfsrKbaiE4EiorHYCBUQ6G2pUpBGCV5eUg1zAgKKIcDb0lSkKIdhGldXgAcVJhBKpdkohhObbQc9BXTARg+TgEMIa2M3sjwAQNxMFyp4NGyE4mVECjINWgQPSUhsbITTWK4FlMsOCmon2uD9MCE24lYNRaVDEKdGWgg8Tgsv60UxCTVBKtDmbIUKom1EejVCFuX0EoM3ZWAkzUMDVUU1CTVDuVIhlHAinoW7m6ehsVMQqUZpmE2aggAcjVSFWiVKGSQhW4ZuRqpBIDkU4zSIEz8LFEasQ1R8WhmaimXABms6MXIWophRR4gKdEKzCUc9CDRFR7FuVaCKcgs7C0RsprmNDlDhFIyyAU+69kRspNmAIUoFCWAMT3gMgUSLKTE3bifuEhXko4H1MQ2xyKgzWavqE4C7+SFPSgSCWolQZlBh9QnAPeMQZW186KMJBndgjhIYKQti5F0BxaRVDOAgYPUK4n1m9HxUi05qBr+kRwtcp7iMaqoKLiATRSjgDJ6Q4mkqlXN7e3i4TqVQ4AbR3VLR/0N+BdDWCNGMhhFYVlHhf2c7fdLs7zWbzemfntHt0WNkuIzEr5JHjE/KOMJHmTveoTHkDaglDGFQYOmEBvuvC6kor5cNuM6GKTCShS7N71K6AlVkpi3fda7n3Du0t8s4R5XkcoWAsKOqEmJ1B5hZU+XCHDCc8JOoYr08OKxRF2PHaR6dN2zuS5I/pZpgxhwPs7SQSkEYqCKbVpnJXTgzj9SnlZvfYGbKy3b7ZCVP+iPQXNO/Klv8cl7f1zVRAGqmw2g+HFfE6kWQAGqoMnx6pnoMCR37cPrlm4WmSSHQtiPANi4boZqoRInZ3DcJhJd9kKdAMmbjuHreJLivazKyoaOVK/lBXntMfkMp4na94INRXogRUuFcJe8Gi4g7YV2XztHtzfEjk+OZE9bvhhJPyTIjNwwHiJC7k94K+RojY/dTX4fYpDFCD1DylPmbdZ0KfTIQHiHjCYo+wgNmCaMzDyg0csCdJIthn5AEimlCIFQxC3D5gPVrkw2A9eBM53K5wE04YhNAGjS5ao63cxauQUxJNbkKtXaMSoraR6jlN3s0N+ol4us1JqK1DCYgWlE6o5qUjVKGKeFPmihZ6Q0rA1BUa4RoJF+VRzUJNDG/DQagmbgIqGgr64m/laJQq7E1FbNYm6BFRwOzS0whJQCwjYqE/iGr+hs28Bb1bQwiLuKeUTm5koaInSTkvoqsnVYoqIXTRsE/4NHc8YhUSJe6Uc/schFKGECI/qhCUH38+GTlhOHGM7WJoQpJvITSL0WGJyD/+uXMPhD/9jGt66yLNEkJE9VtKPfv2l7fh7eaIpyGRZPjwX1sl9Le6pAoW4OuiJeHXt+vr8Uhi9NNQRfx3dOO38xKWcIEQQkun0vlbgheJ1BP3YKREokQ2vkMikgJKgJZOpWcaXyTegFd3vhNGN75BflUeK0AJCWBEk/jG/QDKUR3xFgWoEgKDxZYBGIlX74mwaiD+hjLUWFaA5d2lX+I9wpX7JYxuvMMgSjMCKBz2bfRvQBiNbiGmojQLIxTe9lQYidw7IcpOCSGkdio97quQEN4LYFhe6etwA6FEqSZAmjSDWUjC4f0AWggRM1GaEkBJ20CDfwfCaBShw2kBkLRJ5yYjrd/PNLQQbpyDEaUFAVDhl95BCfVVQE4E52cthHAzldJC2v2/Kv0KI0wmmt2Tk27TcUWKJYnwqfYs6/UWK0V407QA6GGUvo1DCGX5ZHlzmfx1wpG5JroPybPLm0esBomF8BZOWBQA7WCzK2UTJuXjzYeabB6j+8WJE+PZ5V0GIi8hqN0NI+wPkiBiG8aJnf6zy3f0Z005DRFM4gbRocVKGYOUww9NgmzGJe4Gj25e01e9OQlToHlo8TSsQZ5umkaJK5LlsOnZZXqfi5ewCPKl7yCE3WXTKHFmKl+bn6U31C2E32B8KSAeWiJ+JBDCTXdCEyAi9ybxENSIqpsIGbWFZZSMucSSZBjwp2MmRET8BVBeanY1rPowGW4NRtkK48JF4miAuMzoVZoJt6CAal4Kqi1M1ROzAja5mk3syo3c7BNushrqfOGQ1BagtbXSf/pKZPdpEt1NbZzL6HCoBsSHOuMmc+GOy0jV+hDdxYgyp1ji9G6TyB3P2luiebRMnm11WUmtKaWpIpblwV0MUyeq4bBPK9w8PW0y9rrJfaH/dqK5c9oMM/fJ8RXAKiFwjTu1DiDUSiAGQHilasgKK/OUHbaBmcIhIhhqvTZgv3Rgp1wlsByNxAfSwK+wmghRW0diWXRXnxnyHcfXMGW2xA44WiF9QHh9rxGCu/rqykx93TFcOACuWAC5GufGHLzdwq3NEEL4tr1S6dvIOrEx/Ojk+hAh3hBUR7OxEX2HXEJU157g64eEcevZL2/rjq6GTjjMhzaE5Mrt7e1vz1Jc64eYHdBEj0LqfA4LOGykeDOde08KvRKWz1gDRq3ja5RIQF8IOQ+j1tbxsXsxhNIHbBfGs5UmzzhPo9b2YmD30wil39GEdRsi7gVJ7Op2T7T9NNg9UaQg9jwRHZJbqsw95jwxvcixr02VM7QSPUb8JO4k1b4Y+9pwexOJlL7Db9dueMnakh94jbTGsb9Ufe4xvm0v9xNvknpje+Jz7zkJjf2luD3CmqDN1Fw8cfT8MevaZjH2COP2eavCYaZehNtIe/u8kXv1iUhbIyWc+513Gk5xfW+hSgwd9D0JpycdfG+B+mZGE+kxOCSyZx10PnKH+8E3M7hTnzUpAX2NHF5heU55hdXOGJI5bj9T5Pl2rUf4HqREeaUeZ4R3kgTEI5D8m9/PmL5dQyffAjBgaKUvtXclV+PA9JQ7YzN/f4j5htQQmBLlOCvN1pNxQInBr0LzN6S4KthAhLhTmV0qxaGE3Co0fwfMc1cOqMLQGzQUMzWM1N1KPajQ8i03h5mSxMYd0QCxVfRyWFeue8tnDtc8NIvle3weM4U5G6NeslqjHK4DjXTuO+7L0KxnKuDrC/UV5+6EctQoDKuDqCirMQSmwuQZL5/tXAz42SYmKf0OmIpGAyPeqPYqi5VeOQxQIbebsZ1twhH0BZA/7TcwSHxvRKvVaIPkANB+W/Ijt5uxn08DP2PIIu5Tsec1NUhV+r8C2Chvyi1Qzhji6NYIahnlvr9LrtoabRqg+1cNSd6EVKCdE8V3u5pQOnffk9BzLBY+QEt47jG/jdLO+gKf18aBSNQYN0OSKeleVnDXvZpQzmvj8zUqImAjohyuDpptkXoV0PGe464KVaGducfTkNIRtyCRX1aXuaONxkZVLRiDBqSfm4hv1/TelvoAKxYRrTavgNSzL3kDBpEYIEVFibc5yDy/FLNWOiSl98htXo6STHoEZJ1B60GJZDJ+mPOLMXmG/lR0SJjnCPNVGLpIvqlx7qPg8fpv9lnQ+KVEs5RSH/EH7NgkGX7v9f52p/O8vShRZdzyyphMfkTuJqEBOpzJ7mUmau8mjHO838wQvrkP5/gP0m3ieK4+4goWmiiKEjv4KZzg+WhGTiTOHpdKHKcKDAM63o3AsQ5lols9WNtrtTu77IP0HPjCO0edN3trq+qLPBG63G/Be4Gzsrr46FOr3R7TZPfuBANJ1Nc8ueuoT7bbrS9rB14oXe8owdeJOt1Yj86QztjR6QogR1OPcmt2j3d3B4+SN31aW4RePz4k7vfMoC4+1AAXieqsdIbsrkfqUXU3KR1U/+lK9Wass2t7lrzyE+7c4B6h+11BWGejHFDxVNH7FvV6g3CuWJa5dbZqo16P//GQ8XSLBxBy3xPe2TgQDvozkUidoDbUAkqVer334z/sCtTlC4+Zgu7sQq9EtRhD3LX3Zvoy+BmLsI08GFkT4L1r8LvzNFH2wIQ0YRLiz58D352HvQl4jWGmHgk5zp8D33+Is1NlMRDCMSwf6g5LxD2k6gFugRB+wh+wh7iHFBn3Ga5mN75u65MOS3ydQYh3NLi7ZDH3AQvKFzph5/MHdee7E956479f83RC7Mle2PuAMamN8ohupvkH4w/+PIus0yhJwFiPN/76/GD8ewYh1tFg73TGpOAsV5N/QGT8wee/mg2irfVBMCS/qEfP/vw8rv4+ixDHx3EvN+JudeWAbqX5/z3QhVD++d8PZ7ckoYlWb8/O/vqT6G7c+D0GIdLR8NytDo+KyqoLoY4yPvwvjoRjuItzqJHQlbAwD0JUVhljtBIyhEWYbyMQpSLDy7gQhjKQHJwA5rwQPmcQ5hCIqYwDhRMhJLdRtto5kTFIL4SimDvcAiLScxkQoftOIjIH1aOhgyAkiCkQorEziI/QDdEADIZQzLUgiC6AboTOYVFJtfRT9n0nzGuvzX1yR2QHQiChYxKe+mRcIxAQoTjpGhYZ6TaG0AEx9aV3EwR9lB4I+/chuPQy3AEBhExDVX7s3/zU4Sd87UgoLv3ohOhqojBChrtR1kxXWwVG6HiavpuTARNSEa2XBNIJx30gFNvMsAgChBGGsrZykVT2YnCEedOrc226Q5VSjoEeSRjK2HLUT5aru2gTMf89N6H51SRm0AileadUDU8YKhQtlqp8sd6+RlWET4RUhxpzSra5CK31osmNss3UN0LKxYAO9SA/YWhC6lkq5SrSQAnFoTpDkgBRgoPQ5G9atlseuQkvKYS2WzKtUxHqY/CEoUI6ptuo/QpEXwltb7cE/lgaOgXxhGoKJ9FvsaQ4UyAhwEhF0xUsEiBR80IYyhZjqTZlBBQl+kpIKinDh2IslIcwFJqi36weMKERMqirS34Thl5dXARLmKe8nkzFAyWGcjH8hKHC5YVdjYET5sYk+xJ2QIREja0nwwOwuxoY4RXMSEXxSesF11j5CEOh58Omanf6IMIHQMKLi+ecI+UlJKb6xGqqQRLmnlyiYqAvhKHQiyvLdPSN0DYNcxdXfAbqlVBlfJlzIHzORziUs+VeeuHzSEgYf+gz2iYiL+EQ3w+e+DwTarZ6ERjhhSf79ImQML4WL2itfa+EuQvxtWc+XwhDoczz1ssLTsIfGIS5l63nwD6Fs/hCSOTF5ZNhxPxrLsK8pr4nlz6oTxO/CEmAfHU1lveBkAS/ytVX7vBnE/8IiWS+XnXyeU+E+fzF1VdfrLMnvhKGVE2+buXynIT5XOv1K/+0p4vfhKq8+HrVyhMBEbZ6usu3rr76NffMEgShKi9eXV7BCAmb2Lq6fBUEnSpBEapSKBTcCa8uX7964evEG5IgCXtSyGQKdnWOqz8ewf/9/+g0/jDinovrAAAAAElFTkSuQmCC"> 
                <h1>Profile</h1>
                <div class="menu">
                    
                     <a href="./profile.jsp">
                        <div class="account">
                     <i class="fas fa-user"></i>

                        <h3>Account</h3>
                    </div>
                    </a>
                    
                    <a href="./profile_pw.jsp">
                        <div class="password">
                        <i class="fa fa-key"></i>
                        <h3>Password</h3>                       
                    </div>
                    </a>
                    <a href="#">
                        <div class="wallet">
                        <i class="fa fa-wallet"></i>
                        <!--<i class="fa fa-key"></i>-->
                        <h3>Wallet</h3>                       
                    </div>
                    </a>

                </div>
            </div>
            <div class="function">
                <h1>Your Wallet </h1>
                <hr>
                <div class="contaner">
                    <div class="wallet1">
                        <div class="main-account">
                            <span class="account-title">Main account:</span>
                            <span class="account-balance" id="mainAccount"></span>
                        </div>
                        <div class="promo-account">
                          <span class="account-title">Promo account:</span>
                          <span class="account-balance">$0</span>
                        </div>
                        
                        <button class="add-funds-btn">
                            <img src="https://cdn-icons-png.flaticon.com/512/3757/3757881.png">
                            <h3> Add funds</h3>
                        </button>
                      </div>

                </div>

            </div>
        </div>
         <div class="modal add-funds-frm">
            <div class ="modal_overlay">
            </div>
            <div class ="modal_body ">
                 <div class=" fund-header" >
                            <h2 class="">NẠP TIỀN VÀO TÀI KHOẢN</h2>
                            <i onclick="hideForm()" class="fas fa-times"></i>
                           
                 </div>
                <div class="fund-main">
                    <h4 class="fund-title" >Thông tin nạp tiền</h4>
                    <div class="fund-main-1">                        
                        <i class="fas fa-comments-dollar"></i>
                        <span>Số tiền cần nạp</span>
                        <input type="text" id="fund">
                    </div>
                     <div class="fund-main-1">
                        <i class="fas fa-wallet"></i>
                        <span>Số dư tài khoản chính</span>
                        <span id="remain"></span>
                        <span style="font-weight:bold">$</span>
                    </div>
                    <div class="fund-main-1">
                        <i class="far fa-money-bill-alt"></i>
                        <span>Tổng tiền sau khi nạp</span>
                        <span id="sum"></span>
                        <span style="color: #f33a58">$</span>
                    </div>
                     <h4 class="fund-title">Phương thức nạp tiền</h4>
                    <div class="payment">
                        
                        <div>
                            <input checked  name="payment" type="radio" style="height:20px; width: 20px">
                            <img style="height:20px; width: 70px" src="https://cdn.haitrieu.com/wp-content/uploads/2022/10/Logo-VNPAY-QR-1.png">
                            <span>Nạp tiền qua VNPAY</span>
                        </div>
                        <div>
                            <input type="radio" name="payment" style="height:20px; width: 20px">
                            <img style="height:40px; width: 50px" src="https://png.pngtree.com/png-vector/20220719/ourmid/pngtree-atm-card-png-image_6006939.png">
                            <span>Thẻ ngân hàng ATM</span>
                        </div>
                        <div>
                            <input type="radio" name="payment" style="height:20px; width: 20px">
                            <img style="height:12px; width: 50px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Visa_Inc._logo.svg/2560px-Visa_Inc._logo.svg.png">
                            <span>Thẻ thanh toán quốc tế</span>
                        </div>
                        <div>
                            <input type="radio" name="payment" style="height:20px; width: 20px">
                            <img style="height:12px; width: 50px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/PayPal_logo.svg/2560px-PayPal_logo.svg.png">
                            <span>Nạp tiền qua PayPal & Credit Card</span>
                        </div>
                        
                    </div>
                    
                           
                 </div>
                <button class="add-funds-btn" id="funds-account" onclick="addFund()">
                            <img  src="https://cdn-icons-png.flaticon.com/512/3757/3757881.png">
                            <h3> Add funds</h3>
                        </button>
            </div>
        </div>
    </body>
<script>
        var x = document.getElementsByClassName("add-funds-btn");
        var y = document.getElementsByClassName('add-funds-frm');
        x[0].addEventListener('click', function (){
            y[0].style.display = "flex";
        });
        var sum = document.getElementById('sum');
        var fund = document.getElementById('fund');
        var remain = document.getElementById('remain');
        fund.addEventListener('input', function() {
            let a = +fund.value;
            let b = +remain.innerHTML;
            sum.innerHTML = a+b;
        });
    async function addFund() {
        var fun = document.getElementById('fund').value;
        console.log(fun);
        const updatedData = {
            total: fun
        };
        const response = await fetch('https://localhost:8080/FreshMarket/api/wallets/add-fund', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(updatedData)
        });
        const res = await response.json();
        if(res == "1"){
            alert("Add successfully!!");
            hideForm();
            window.location.href = './wallet.jsp';
        }
        else {
            alert("Something is wrong!!");
        }

    }
    
    function hideForm(){
        var y = document.getElementsByClassName('add-funds-frm'); 
        y[0].style.display = "none";
        document.getElementById("fund").value="";
    }
</script>
</html>
<script>

    let url = "https://localhost:8080/FreshMarket/api/wallets/detail";
    fetch(url)
        .then(response => response.json())
        .then(data => {

            document.getElementById('mainAccount').textContent = "$" + data.mainAccount;
            document.getElementById('remain').textContent = data.mainAccount;
        })
        .catch(error => {
            console.error("Error retrieving user information:", error);
        });


    async function callUpdateAPI(event) {

        let oldpw = document.getElementById("current-password").value;
        let newpw = document.getElementById("new-password").value;
        let confirmpw = document.getElementById("confirm-password").value;

        if(newpw.toString() != confirmpw.toString()){
            alert("password does not match!!");
        }
        else{
            const updatedData = {
                password: oldpw,
                newpassword: newpw
            };
            const response = await fetch('https://localhost:8080/FreshMarket/api/users/change-password', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(updatedData)
            });
            const res = await response.json();
            if (res == "1") {
                alert("Update successful");
                window.location.href = './';
            }
            else if(res == "-1"){
                alert("the password is not correct!!");
            } else{
                alert("Update failed");
            }
        }

    }
    async function callLogoutAPI(event) {
        const logoutUrl = 'https://localhost:8080/FreshMarket/api/authen/logout';
        await fetch(logoutUrl);
        window.location.href = './';
    }
    
</script>