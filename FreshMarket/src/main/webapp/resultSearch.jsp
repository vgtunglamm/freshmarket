<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>G10 MARKET</title>
  <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png" />
  <!--reset css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
  <link rel="stylesheet" href="./assests/css/grid.css" />
  <link rel="stylesheet" href="./assests/css/base.css" />
  <link rel="stylesheet" href="./assests/css/main.css" />
  <link rel="stylesheet" href="./assests/css/responsive.css" />
  <link rel="stylesheet" href="./modal/modal.css" />
  <link rel="stylesheet" href="./contact_page/contact.css" />
  <link rel="stylesheet" href="./header/header.css">
  <link rel="stylesheet" href="./footer/footer.css">
  <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
  <link rel="stylesheet" href="./assests/css/categoryProduct.css">
  <link rel="stylesheet" href="./chatbox/chatbox.css">
  <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
  <link rel="stylesheet" href="./assests/responsive/categoryProduct_responsive.css">
  <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css" />
  <!--Nhúng font-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
    rel="stylesheet" />
  <style>
      #products-list{
         display: flex;
        flex-wrap: wrap;
          width:auto;
          
      }
/*    .product {
        display: inline-block;
        position:absolute;
        overflow-wrap: break-word;
      display: flex;
      background-color: white;
      align-items: center;
      margin-bottom: 10px;
      margin-right: 20px;
      margin-top:20px;
      padding: 10px;
      border: 1px solid #ccc;
      margin:20px;
      height:310px;
      width:150px;
    }*/
    .product:hover{
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.4);
        transform: translateY(-5px);
    }

    .product img {
      /*width: 200px;*/
      height: 200px;
      object-fit: contain;
      margin-right: 10px;
      padding-bottom: 10px;
    }

    .product-info {
      flex: 1;
    }

    .product-name {
      font-size: 14px;
      /*font-weight: bold;*/
      margin-bottom: 5px;
      display: block;
  text-align: center;
  color:black;
    }

    .product-price {
      font-size: 16px;
      color: green;
      padding-top:10px;
      display: block;
  text-align: center;
  color:orange;
    }
    img{
        height:100px;
    }
    .home__product-item-btn-wrap{
        margin-top:5px;
        padding:5px;
    }
    .view-button{
        padding:10px 30px;
        border-radius: 50px;
        border:none;
        font-size:12px;
        background-color: #80BB35;
        color:white;
    }
    a{
        text-decoration: none;
    }
  </style>
</head>

<body>
<c:if test="${sessionScope.role == 'user'}">
  <header class="header" id="header_user">
    <!-- được render từ file header.js  -->
  </header>
</c:if>
<c:if test="${sessionScope.role == 'admin'}">
  <header class="header" id="header_admin">
    <!-- được render từ file header.js  -->
  </header>
</c:if>
<c:if test="${sessionScope.role == null}">
  <header class="header" id="header">
    <!-- được render từ file header.js  -->
  </header>
</c:if>

  <!-- category mobile -->
  <div class="menu_mobile" id="menu_mobile">
    <div class="menu_close">
      <i class="menu_close-icon fa-solid fa-xmark" onclick="closeMenuMobile()"></i>
    </div>

    <div class="header__search-mobile">
      <div class="header__search-input-wrap">
        <input type="text" class="header__search-input" placeholder="Tìm kiếm trên G10 Market">
      </div>

      <button class="header__search-btn">
        <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
      </button>
    </div>

    <nav class="category_mobile">
      <div class="category__heading">
        <h3 class="category__heading-tittle">DANH MỤC</h3>
      </div>
      <ul class="category__list mobile">
        <c:forEach items="${categories}" var="cate">
          <li class="category__list-tittle">
            <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
          </li>
        </c:forEach>
      </ul>
    </nav>
  </div>

  <div class="app_container">
    <div class="grid wide">
      <!-- category + slider  -->
      <div class="row category-product_row">
        <div class="col l-12 m-12 c-12 mobile_filter-wrap">
          <p class="mobile_filter" onclick="toggleFilter()">
            Bộ lọc
            <i class="fa-solid fa-sliders"></i>
          </p>
        </div>

        <div class="category_wrap col l-3 m-12 c-12" id="category_wrap">
          <!-- Danh mục  -->
          <nav class="categoryPr tier1">
            <div class="category__heading">
              <h3 class="category__heading-tittle">DANH MỤC</h3>
            </div>
            <ul class="category__list">
              <c:forEach items="${categories}" var="cate">
                <li class="category__list-tittle">
                  <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
                </li>
              </c:forEach>
            </ul>
          </nav>

          <!-- Lọc theo màu sắc  -->
          <nav class="categoryPr tier2">
            <div class="category__heading">
              <h3 class="category__heading-tittle">MÀU SẮC</h3>
            </div>
            <ul class="category__list list-color">
              <li class="category__list-color">
                <a href="category?color=red" class="category__list-tittle-link">
                  <div class="category__list-box type-red"></div>
                </a>
              </li>
              <li class="category__list-color">
                <a href="category?color=green" class="category__list-tittle-link">
                  <div class="category__list-box type-green"></div>
                </a>
              </li>
              <li class="category__list-color">
                <a href="category?color=purple" class="category__list-tittle-link">
                  <div class="category__list-box type-purple"></div>
                </a>
              </li>
              <li class="category__list-color">
                <a href="category?color=white" class="category__list-tittle-link">
                  <div class="category__list-box type-white"></div>
                </a>
              </li>
              <li class="category__list-color">
                <a href="category?color=yellow" class="category__list-tittle-link">
                  <div class="category__list-box type-yellow"></div>
                </a>
              </li>
            </ul>
          </nav>

          <!-- lọc theo thương hiệu  -->
          <nav class="categoryPr tier3">
            <div class="category__heading">
              <h3 class="category__heading-tittle">THƯƠNG HIỆU</h3>
            </div>
            <ul class="category__list">
              <c:forEach items="${brands}" var="brand">
                <li class="category__list-tittle">
                  <a href="category?brand=${brand.id}" class="category__list-tittle-link">
                    <p class="category_brand" style="font-size: 1.6rem; margin: 0;cursor: pointer;">
                      ${brand.name}
                    </p>
                  </a>
                </li>
              </c:forEach>
            </ul>
          </nav>
        </div>

        <div class="col l-9">
          <!-- tiêu đề category (một vài thẻ dùng chung với phần contact)  -->
          <div class="contact_nav category_nav">
            <a href="./" class="back_homepage-nav" style="font-size: 2.1rem">Trang chủ</a>
            <h3 class="contact_seperate" style="font-size: 2.1rem"> / ${nameFix}</h3>
            <a href="" class="category_page-nav"
              style="font-size: 2.1rem;text-decoration: none;color: var(--primary-color);"></a>
          </div>

         <div id="products-list">
           <c:forEach items="${productSearches}" var="product">
             <div class="col l-3 m-4 c-6">
               <a href="productInformation?productId=${product.id}" class="home__product-item">
                 <img src="${product.image}" alt='' class="home__product-item-img">
                 <h3 class="home__product-item-tittle">${product.productName}</h3>
                 <h4 class="home__product-item-price">${product.price} ₫</h4>
                 <div class="home__product-item-btn-wrap">
                   <button class="home__product-item-btn btn btn--primary ">Xem chi tiết</button>
                 </div>
               </a>
             </div>
           </c:forEach>
         </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Ngăn cách content với footer -->
  <div style="padding-bottom: 70px; background-color: #f5f5f5"></div>

  <footer class="footer" id="footer">
    <!-- được render từ file footer.js  -->
  </footer>

  <!-- modal  -->
  <div id="modal_wrap">
    <!-- render từ file modal.js  -->
  </div>
  <!-- chat message  -->
  <div id="chat-box">
    <!-- render từ file chatbox.js  -->
  </div>

  <!-- render header -->
<script src="./header/header.js"></script>
<script src="./header/header_admin.js"></script>
<script src="header/header_user.js"></script>
  <!-- render toast message  -->
  <script src="./toast_messages/toast.js"></script>
  <!-- render modal  -->
  <script src="./modal/modal.js"></script>
  <!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
  <script src="./javascript/logIn_and_Signup.js"></script>
  <!-- import validator đăng ký, đăng nhập  -->
  <script src="./javascript/validator.js"></script>
  <!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
  <script>
    const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
    const headerMenu = document.querySelector('.header_menu');
    var emailReg;
    var passwordReg;
    var emailLog;
    var passwordLog;
    // Đăng ký 
    Validator({
      form: "#form-1",
      formGroupSelector: ".form-group",
      errorSelector: ".form-message",
      rules: [
        // Validator.isRequired('#fullname'),
        Validator.isRequired("#email"),
        Validator.isEmail("#email"),
        Validator.minLength("#password", 6),
        Validator.isRequired("#password_confirmation"),
        Validator.isConfirmed(
                "#password_confirmation",
                function () {
                  return document.querySelector("#form-1 #password").value;
                },
                "Mật khẩu nhập lại không khớp"
        ),
      ],
      onSubmit: async function (evt) {

        // console.log(evt);
        emailReg = evt.email;
        passwordReg = evt.password;

        let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
        const response = await fetch(url);
        const res = await response.json();

        if (res != "0") {
          showSuccessSignUpExistEmailToast();
          logModal.classList.add('open');
          regModal.classList.remove('open');
        } else {
          const data = {
            email: emailReg,
            passWord: passwordReg
          };
          const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          });
          const res = await response.json();
          console.log(response);
          console.log(res);
          if (res == "1") {
            showSuccessSignUpToast();
            logModal.classList.add('open');
            regModal.classList.remove('open');
            // window.location.reload();
          } else {
            showFaiedSignUpToast();
            logModal.classList.add('open');
            regModal.classList.remove('open');
          }
        }
      },
    });
    //Đăng nhập
    Validator({
      form: "#form-2",
      formGroupSelector: ".form-group",
      errorSelector: ".form-message",
      rules: [
        Validator.isRequired("#email"),
        Validator.isEmail("#email"),
        Validator.minLength("#password", 6),
      ],
      onSubmit: function (data) {
        //Call api
        console.log(data);
        emailLog = data.email;
        passwordLog = data.password;

        if (emailReg === emailLog && passwordReg === passwordLog) {
          showSuccessLogInToast();
        }
        else {
          showErrorLogInToast();
        }
      },
    });
  </script>
  <!-- xử lý trượt slider  -->
  <script src="./javascript/slider.js"></script>
  <!-- Time sale countDown -->
  <script src="./javascript/countDownTime.js"></script>
  <!-- object product 1  -->
  <script src="./javascript/mainProduct.js"></script>
  <!-- Đóng mở chat hỗ trợ trực tuyến -->
  <script>
    function openChat() {
      document.getElementById("myChat").style.display = "block";
    }
    function closeChat() {
      document.getElementById("myChat").style.display = "none";
    }
  </script>
  <!-- Đóng mở menu mobile -->
  <script>
    function openMenuMobile() {
      document.getElementById("menu_mobile").style.display = "block";
    }
    function closeMenuMobile() {
      document.getElementById("menu_mobile").style.display = "none";
    }
  </script>
  <!-- render chatbox  -->
  <script src="./chatbox/chatbox.js"></script>
  <!-- render footer -->
  <script src="./footer/footer.js"></script>
  <!-- xử lý click đóng mở bộ lọc  -->
  <script>
    function toggleFilter() {
      document.getElementById('category_wrap').classList.toggle("show");
    }
  </script>
<%--  <script src="javascript/search.js"></script>--%>
 

 
</body>

</html>