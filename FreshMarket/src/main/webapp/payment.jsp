<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>G10 MARKET</title>
    <link rel="icon" type="image/x-icon" href="./assests/img/icon_main.png" />
    <!--reset css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
    <link rel="stylesheet" href="./assests/css/grid.css" />
    <link rel="stylesheet" href="./assests/css/base.css" />
    <link rel="stylesheet" href="./assests/css/main.css" />
    <link rel="stylesheet" href="./assests/css/responsive.css" />
    <link rel="stylesheet" href="./modal/modal.css" />
    <link rel="stylesheet" href="./header/header.css">
    <link rel="stylesheet" href="./footer/footer.css">
    <link rel="stylesheet" href="./chatbox/chatbox.css">
    <link rel="stylesheet" href="./toast_messages/toast.css">
    <link rel="stylesheet" href="./contact_page/contact.css">
    <link rel="stylesheet" href="./assests/css/cart.css">
    <link rel="stylesheet" href="./assests/responsive/header_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/modal_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/index_responsive.css">
    <link rel="stylesheet" href="./assests/responsive/cart_responsive.css">
    <link rel="stylesheet" href="./assests/fonts/fontawesome-free-6.2.0-web/css/all.min.css" />
    <!--Nhúng font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400, 500, 700&display=swap&subset=vietnamese"
          rel="stylesheet" />
    <style>

        .payment-button {
            width: 100%;
            padding-right: 12px;
            margin-top: 12px;
            margin: 12px 0;
            margin-bottom: 30px;
            justify-content: space-between;
            padding-top: 20px;
        }

        .payment-button p {
            font-family: var(--main-text-font);
            font-size: 16px;
            display: inline-block;
        }

        .payment-button>p {
            font-weight: bold;
        }

        .payment-button label {
            font-family: var(--main-text-font);
            font-size: 16px;
            margin-bottom: 6px;
            display: block;
        }

        .payment-button input {
            width: 100%;
            height: 35px;
            font-size: 16px;
            border: 1px solid #ddd;
            padding-left: 6px;
            margin-right: 12px;
        }

        .payment-button span {
            margin-right: 12px;
        }

        .payment-method-payment{
            font-size: 16px;
            display: flex;
            justify-content: space-between;
        }

        thead tr{
            height:60px;
        }
        button{
            padding:10px 20px;
            border:1px sloid #ccc;
            display: flex;
            height: 45px;
            justify-content: center;
            align-items: center;
            border-radius: 10px;

        }
        button:hover{
            color:white;
            background-image: linear-gradient(0, #80BB35, #80BB35);
            border:none;
            cursor: pointer;

        }
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table, th, td {
            border: none;
            padding: 8px;
            font-size:20px;
            text-align: center;

        }
        tr{
            border-bottom: 2px solid #f9f9f9;
        }
        td input{
            width:50px;
        }
        th {
            background-color: #f2f2f2;
        }

        #total {
            font-weight: bold;
        }

        .quantity-input {
            width: 50px;
        }
        td img{
            height:200px;

        }
        #total{
            height:100px;
        }
        #myCheckbox{
            height:50px;
            transform: scale(1.5);
        }

        #myToast {
            font-size:28px;

            display: none;
            position: fixed;

            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #80BB35;
            color: white;
            border:3px solid white;
            border-radius: 5px;
            z-index: 9999;


        }
        .myToast1 {
            height:200px;
            width:500px;
            padding-left: 100px;
            padding-top:85px;



        }
        .myToast2 {

            height:200px;
            width:680px;

            padding-left: 50px;
            padding-top:90px;

        }
        .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 9999;
            display: none;
        }

    </style>
</head>

<body>
<div class="app">
    <c:if test="${sessionScope.role != null && sessionScope.role != 'admin'}">
        <header class="header" id="header_user">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>
    <c:if test="${sessionScope.role == 'admin'}">
        <header class="header" id="header_admin">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>
    <c:if test="${sessionScope.role == null}">
        <header class="header" id="header">
            <!-- được render từ file header.js  -->
        </header>
    </c:if>

    <!-- category mobile -->
    <div class="menu_mobile" id="menu_mobile">
        <div class="menu_close">
            <i class="menu_close-icon fa-solid fa-xmark" onclick="closeMenuMobile()"></i>
        </div>

        <div class="header__search-mobile">
            <div class="header__search-input-wrap">
                <input type="text" class="header__search-input" placeholder="Tìm kiếm trên G10 Market">
            </div>

            <button class="header__search-btn">
                <i class="header__search-btn-icon fa-solid fa-magnifying-glass"></i>
            </button>
        </div>

        <nav class="category_mobile">
            <div class="category__heading">
                <h3 class="category__heading-tittle">DANH MỤC</h3>
            </div>
            <ul class="category__list">
                <c:forEach items="${categories}" var="cate">
                    <li class="category__list-tittle">
                        <a href="category?id=${cate.id}" class="category__list-tittle-link">${cate.name}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </div>

    <div class="app_container">
        <!-- Ngăn cách với header  -->
        <div style="padding-top: 15px; background-color: #f5f5f5"></div>

        <div class="grid wide">
            <div class="col l-12">
                <div class="contact_nav">
                    <a href="./" class="back_homepage-nav cart_nav">Trang chủ</a>
                    <h3 class="contact_seperate" style="font-size: 2.3rem; margin: 0 8px 26px 8px;">/</h3>
                    <a href="" class="contact_page-nav" style="font-size: 2.3rem; margin: 0 0 26px 0;">Thanh toán</a>
                </div>
            </div>


            <div class="row cart_wrap">
                <!--                    <div class="cart_info">
                                        <img src="./assests/img/empty-order.png" alt="" class="cart_img">
                                        <p class="cart_title">Không có sản phẩm nào trong giỏ hàng</p>
                                        <a href="./" class="cart_link">
                                            <div class="btn btn--primary cart_btn">Tiếp tục mua sắm</div>
                                        </a>
                                    </div>-->
                <table>
                    <thead>
                    <tr>
                        <th>Select</th>
                        <th>Hình ảnh</th>
                        <th>Sản phẩm</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Tổng cộng</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="cart-items">
                    <!-- Các mục giỏ hàng được tạo bằng JavaScript -->
                    </tbody>
                    <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="2">Tổng giá trị đơn hàng:</td>
                        <td  id="total" >$0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="2">Phí vận chuyển:</td>
                        <td  id="total" >$0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="2">Tổng số tiền:</td>
                        <td  id="total1" style="color: crimson; font-size: 24px">$0</td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="payment-button">
                <p style="color: #689E49;">Vui lòng chọn địa chỉ giao hàng</p>
                <div style="display: flex;">
                    <div class="payment-button">
                        <label for="">Họ tên <span style="color: red;">*</span></label>
                        <input type="text">
                    </div>
                    <div class="payment-button">
                        <label for="">Điện thoại <span style="color: red;">*</span></label>
                        <input type="text">
                    </div>
                    <div class="payment-button">
                        <label for="">Tỉnh/Thành phố <span style="color: red;">*</span></label>
                        <input type="text">
                    </div>
                </div>
                <div style="display: flex;">
                    <div class="payment-button" style="width: 50%;">
                        <label for="">Quận/Huyện <span style="color: red;">*</span></label>
                        <input type="text">
                    </div>
                    <div class="payment-button">
                        <label for="">Địa chỉ cụ thể <span style="color: red;">*</span></label>
                        <input type="text">
                    </div>
                </div>

            </div>
            <div class="payment-method-payment"><p style="font-weight: bold; color: #689E49;">Phương thức thanh toán</p></div>
            <div class="payment-method-payment">


                <div class="payment-method-payment-item">
                    <input name="method-payment" type="radio">
                    <label for="">Thanh toán bằng thẻ ATM</label>
                </div>
                <div class="payment-method-payment-item">
                    <input id="myCheckbox" name="method-payment" type="radio">
                    <label for="">Thanh toán bằng Ví fresh</label>
                </div>
                <div class="payment-method-payment-item">
                    <input checked name="method-payment" type="radio">
                    <label for="">Thanh toán khi giao hàng</label>
                </div>
            </div>
            <div class="payment-button" style="display: flex;">
                <a href="cart.jsp"><p style="color: #689E49;">Quay lại giỏ hàng</p></a>
                <button id="myButton"><p style="font-weight: bold; font-size: 20px;">Giao Hàng</p></button>
                <div id="overlay" class="overlay"></div>
                <div id="myToast"></div>
            </div>
        </div>
    </div>

    <!-- Ngăn cách content với footer -->
    <div style="padding-bottom: 70px; background-color: #f5f5f5"></div>

    <footer class="footer" id="footer">
        <!-- được render từ file footer.js  -->
    </footer>
</div>

<!-- modal  -->
<div id="modal_wrap">
    <!-- render từ file modal.js  -->
</div>
<!-- chat message  -->
<div id="chat-box">
    <!-- render từ file chatbox.js  -->
</div>
<!-- toast message  -->
<div id="toast">
    <!-- render từ file toast.js  -->
</div>

<!-- render header -->
<script src="./header/header.js"></script>
<script src="./header/header_admin.js"></script>
<script src="header/header_user.js"></script>
<!-- render toast message  -->
<script src="./toast_messages/toast.js"></script>
<!-- render modal  -->
<script src="./modal/modal.js"></script>
<!-- Xử lý sự kiện click đăng ký/ Đăng nhập  -->
<script src="./javascript/logIn_and_Signup.js"></script>
<!-- import validator đăng ký, đăng nhập  -->
<script src="./javascript/validator.js"></script>
<!-- xử lý các rule, sự kiện đăng ký, đăng nhập, check đăng ký, đăng nhập thành công-->
<script>
    const headerNavbarListTwo = document.querySelector('.header__navbar-list-two');
    const headerMenu = document.querySelector('.header_menu');
    var emailReg;
    var passwordReg;
    var emailLog;
    var passwordLog;
    // Đăng ký
    Validator({
        form: "#form-1",
        formGroupSelector: ".form-group",
        errorSelector: ".form-message",
        rules: [
            // Validator.isRequired('#fullname'),
            Validator.isRequired("#email"),
            Validator.isEmail("#email"),
            Validator.minLength("#password", 6),
            Validator.isRequired("#password_confirmation"),
            Validator.isConfirmed(
                "#password_confirmation",
                function () {
                    return document.querySelector("#form-1 #password").value;
                },
                "Mật khẩu nhập lại không khớp"
            ),
        ],
        onSubmit: async function (evt) {

            emailReg = evt.email;
            passwordReg = evt.password;

            let url = "https://localhost:8080/FreshMarket/api/user/?email=" + emailReg;
            const response = await fetch(url);
            const res = await response.json();

            if (res != "0") {
                showSuccessSignUpExistEmailToast();
                regModal.classList.add('open');
            } else {
                const data = {
                    email: emailReg,
                    passWord: passwordReg
                };
                const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const res = await response.json();
                if (res == "1") {
                    showSuccessSignUpToast();
                    logModal.classList.add('open');
                    regModal.classList.remove('open');
                } else {
                    showFaiedSignUpToast();
                    // regModal.classList.add('open');
                }
            }
        },
    });
    //Đăng nhập
    Validator({
        form: "#form-2",
        formGroupSelector: ".form-group",
        errorSelector: ".form-message",
        rules: [
            Validator.isRequired("#email"),
            Validator.isEmail("#email"),
            Validator.minLength("#password", 6),
        ],
        onSubmit: async function (data) {
            emailLog = data.email;
            passwordLog = data.password;

            const obj = {
                email: emailLog,
                passWord: passwordLog
            };
            const response = await fetch('https://localhost:8080/FreshMarket/api/authen/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(obj)
            });
            const res = await response.json();
            if (res == "0") {
                showErrorLogInToast();
                logModal.classList.add('open');
            }
            else {
                showSuccessLogInToast();
                window.location.href = "https://localhost:8080/FreshMarket/";
            }
        },
    });
</script>
<!-- xử lý trượt slider  -->
<script src="./javascript/slider.js"></script>
<!-- Time sale countDown -->
<script src="./javascript/countDownTime.js"></script>
<!-- object product 1  -->
<script src="./javascript/mainProduct.js"></script>
<!-- Đóng mở chat hỗ trợ trực tuyến -->
<script>
    function openChat() {
        document.getElementById("myChat").style.display = "block";
    }
    function closeChat() {
        document.getElementById("myChat").style.display = "none";
    }
</script>
<!-- Đóng mở menu mobile -->
<script>
    function openMenuMobile() {
        document.getElementById("menu_mobile").style.display = "block";
    }
    function closeMenuMobile() {
        document.getElementById("menu_mobile").style.display = "none";
    }
</script>
<!-- render chatbox  -->
<script src="./chatbox/chatbox.js"></script>
<!-- render footer -->
<script src="./footer/footer.js"></script>
<script>




    // Dữ liệu mẫu giỏ hàng
    var cartItems = [];
    let catrUrl = "https://localhost:8080/FreshMarket/api/carts/get-all" ;
    fetch(catrUrl, {
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
        .then(response => response.json())
        .then(data => {
            data.forEach(cart => {
                let id = cart.id;
                let quantity = cart.quantity;
                let image = cart.image;
                let price = cart.price;
                let productName = cart.productName;
                cartItems.push({id:id, name: productName, price: price, quantity: quantity ,image : image,buy: false});
            });
            displayCart();
        })
        .catch(error => {
            console.error(error);
        });
    // Hiển thị giỏ hàng
    function displayCart() {
        var cartTable = document.getElementById("cart-items");
        var cartTotal = document.getElementById("total");
        var cartTotal1 = document.getElementById("total1");
        var totalPrice = 0;

        // Xóa các dòng hiện tại trong bảng
        while (cartTable.firstChild) {
            cartTable.removeChild(cartTable.firstChild);
        }

        // Thêm dữ liệu từ giỏ hàng vào bảng
        cartItems.forEach(function(item, index) {
            var row = document.createElement("tr");
            var buyCell = document.createElement("td");
            var imageCell = document.createElement("td");
            var nameCell = document.createElement("td");
            var priceCell = document.createElement("td");
            var quantityCell = document.createElement("td");
            var totalCell = document.createElement("td");
            var removeCell = document.createElement("td");


            imageCell.innerHTML = "<img src='" + item.image + "' alt='Hình ảnh sản phẩm'>";
            nameCell.textContent = item.name;
            priceCell.textContent = item.price;

            // Input số lượng
            var quantityInput = document.createElement("input");
            quantityInput.classList.add("quantity");
            quantityInput.type = "number";
            quantityInput.value = item.quantity;
            quantityInput.min = 0;
            quantityInput.dataset.index = index;
            quantityInput.addEventListener("input", updateQuantity);
            quantityCell.appendChild(quantityInput);

            // Tính toán và hiển thị tổng tiền cho sản phẩm
            var total = 0;
            if (item.buy) {
                total = item.price * item.quantity;
            }
            totalCell.textContent = total;
            totalPrice += total;

            // Checkbox mua
            var buyCheckbox = document.createElement("input");
            buyCheckbox.type = "checkbox";
            buyCheckbox.checked = item.buy;
            buyCheckbox.dataset.index = index;
            buyCheckbox.addEventListener("change", updateBuy);
            buyCell.appendChild(buyCheckbox);

            // button remove sản phẩm
            var removeSp= document.createElement("button");
            removeSp.textContent ="Xóa";
            removeSp.id ='remove';
            removeCell.appendChild(removeSp);


            row.appendChild(buyCell);
            row.appendChild(imageCell);
            row.appendChild(nameCell);
            row.appendChild(priceCell);
            row.appendChild(quantityCell);
            row.appendChild(totalCell);


            cartTable.appendChild(row);
        });

        // Cập nhật tổng tiền
        cartTotal.textContent = totalPrice;
        cartTotal1.textContent = totalPrice;

    }

    // Cập nhật số lượng
    function updateQuantity(event) {
        var index = event.target.dataset.index;
        var quantity = parseInt(event.target.value);

        if (quantity >= 0) {
            cartItems[index].quantity = quantity;
            displayCart();
        }
    }

    // Cập nhật trạng thái mua
    function updateBuy(event) {
        var index = event.target.dataset.index;
        var buy = event.target.checked;

        cartItems[index].buy = buy;
        displayCart();
    }
</script>

<script>
    document.getElementById("myButton").addEventListener("click", async function () {
        var checkbox = document.getElementById("myCheckbox");
        var toast = document.getElementById("myToast");

//            Nếu chọn thanh toán bằng ví fresh
        if (checkbox.checked) {
//              Nếu mua hàng thành công, thêm đk mua hàng thành công
            var totalCost = document.getElementById("total1").textContent;
            var productIds = [];
            cartItems.forEach(function(item){
                if(item.buy){
                    productIds.push(item.id);
                }
            });
            const updatedData = {
                total: totalCost,
                productIds: productIds
            };
            const response = await fetch('https://localhost:8080/FreshMarket/api/carts/payment', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                body: JSON.stringify(updatedData)
            });
            const res = await response.json();
            if (res != "0") {
                toast.innerHTML = "Mua hàng thành công!";
                toast.style.display = "block";
                toast.classList.add("myToast1");
                var overlay = document.getElementById("overlay");
                overlay.style.display = "block";

                setTimeout(function () {
                    toast.style.display = "none";
                    overlay.style.display = "none";
                }, 4000); // Đóng toast sau 4 giây

                window.location.href = "https://localhost:8080/FreshMarket/payment.jsp";
            } else {
                toast.innerHTML = "Số dư tài khoản ví Fresh của bạn không đủ!";
                toast.classList.add("myToast2");
                toast.style.display = "block";
                var overlay = document.getElementById("overlay");
                overlay.style.display = "block";

                setTimeout(function () {
                    toast.style.display = "none";
                    overlay.style.display = "none";
                }, 2000); // Đóng toast sau 2 giây
            }
        }
//            Nếu chọn thanh toán bằng phương thứckhacs
        else {

            toast.innerHTML = "Mua hàng thành công!";
            toast.style.display = "block";
            toast.classList.add("myToast1");
            var overlay = document.getElementById("overlay");
            overlay.style.display = "block";

            setTimeout(function () {
                toast.style.display = "none";
                overlay.style.display = "none";
            }, 2000); // Đóng toast sau 2 giây
        }
    });

</script>

</body>

</html>