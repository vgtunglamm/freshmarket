document.getElementById('chat-box').innerHTML = `
 <div class="open_chat"  onclick="openChat()" >
    <i class="open_chat-icon fa-regular fa-comments"></i>
    </div>
    <div class="chat-popup" id="myChat">
    <div class="chat-popup_title-wrap">
        <i class="chat-popup-close fa-solid fa-xmark" onclick="closeChat()"></i>
        <h1 class="chat-popup_title">
            CSKH G10 Market
            <i class="fa-solid fa-phone-flip" style="font-size: 1.6rem; color: #f5f5f5"></i>
        </h1>
    </div>
     <div class="chat-container">
      <div class="send-mes">
        <div id="message-container"></div>
        <input type="text" onkeydown="handleKeyPress(event)" id="message-input" placeholder="Type your message..." />
        <button type="submit" onclick="sendMessage()">Send</button>
      </div>
    </div>
 </div>
`
function sendMessage() {
    var chatContainer = document.getElementById("message-container");
    const messageInput = document.getElementById("message-input");
    const message = messageInput.value;

    var currentTime = new Date();

    var year = currentTime.getFullYear();
    var month = currentTime.getMonth() + 1; // Tháng trong JavaScript đếm từ 0, nên cần +1
    var day = currentTime.getDate();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();

    var timeReal=year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

    var timeDiv = document.createElement("div");
    timeDiv.classList.add("time");
    timeDiv.textContent = timeReal;

    var messageContentDiv = document.createElement("div");
    messageContentDiv.classList.add("message-content");
    messageContentDiv.classList.add("fixBug");
    messageContentDiv.textContent = message;

    // Tạo phần tử tin nhắn
    const messageElement = document.createElement("div");
    messageElement.className = "message1";
    messageElement.className = "from-bob";
    messageElement.classList.add('message');

    timeDiv.classList.add('acc');

    messageElement.appendChild(messageContentDiv);
    messageElement.appendChild(timeDiv);

    if(message !==''){
        chatContainer.appendChild(messageElement);
    }
    // Xóa nội dung trong input
    messageInput.value = "";
    chatContainer.scrollTop = chatContainer.scrollHeight;

    /// add mess vao database
    // default the id of admin
    let adminId = '194a5198-6dfe-4a37-89d8-4babe9c5b484';
    const requestBody = {
        from: userId,
        to: adminId,
        mess : message
    };
    let url = "https://localhost:8080/FreshMarket/api/chats/send-message"  ;
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(requestBody)
    });
}

 function handleKeyPress(event) {
      if (event.keyCode === 13) { // Kiểm tra nếu phím nhấn là Enter (keyCode = 13)
        event.preventDefault(); // Ngăn chặn hành vi mặc định của Enter (chuyển dòng mới)
        sendMessage(); // Gọi hàm gửi tin nhắn
      }
    }

let chatContainer = document.getElementById("message-container");
let adminId = '194a5198-6dfe-4a37-89d8-4babe9c5b484';
const requestBody = {
    from: userId,
    to: adminId
};
let url = "https://localhost:8080/FreshMarket/api/chats/get-conversation"  ;
fetch(url, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestBody)
})
    .then(function(response) {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error("Failed to fetch user data");
        }
    })
    .then(function(data) {
        data.forEach(function(chat) {
            let time = chat.createDate;
            let from = "";
            if(chat.from.toString() === userId){
                from = "Bob";
            }
            else{
                from = "Alice";
            }
            let message = chat.message;

            /// gen messs
            var messageDiv = document.createElement("div");
            messageDiv.classList.add("message");

            var fromDiv = document.createElement("div");
            fromDiv.classList.add("from");
            fromDiv.textContent = "Admin";

            var timeDiv = document.createElement("div");
            timeDiv.classList.add("time");
            timeDiv.textContent = time.toString();

            var messageContentDiv = document.createElement("div");
            messageContentDiv.classList.add("message-content");
            messageContentDiv.textContent = message.toString();

            if (from.toString() === 'Alice') {
                messageDiv.appendChild(fromDiv);
                messageDiv.appendChild(messageContentDiv);
                messageDiv.appendChild(timeDiv);
               
                messageDiv.classList.add('from-alice');
            }
            else
            {
                messageDiv.appendChild(messageContentDiv);
                messageDiv.appendChild(timeDiv);
                messageDiv.classList.add('from-bob');
                timeDiv.classList.add('acc');
            }
            if(message !==''){
                    chatContainer.appendChild(messageDiv);
                }
//            chatContainer.appendChild(messageDiv);
        });
    })
    .catch(function(error) {
        console.error("Error:", error);
    });

