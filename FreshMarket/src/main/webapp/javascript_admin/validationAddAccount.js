/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
function validateForm() {
    // Lấy giá trị các trường input
    console.log("Test");
    var name = document.getElementById('name1').value;
    var email = document.getElementById('email1').value;
    var password = document.getElementById('pass1').value;
    var confirmPassword = document.getElementById('cfpass1').value;
    var role = document.getElementById('role1').value;
    
    
    var errorEmail =document.getElementById('notiEmail');
    var errorPw=document.getElementById('notiPw');
    var errorCfPw= document.getElementById('notiCfPw'); 
   var errorUser=document.getElementById('notiUser');
   var errorRole=document.getElementById('notiRole');
   
   
    if (name === '' && email===''&& password===''&& confirmPassword==='' && role==='') {
      errorUser.innerHTML="Please enter Username!";
       errorEmail.innerHTML="Please enter Email!";
         errorPw.innerHTML="Please enter Password!";
          errorCfPw.innerHTML="Please enter Confirm Password!";
          errorRole.innerHTML="Please enter Role!";
      return false;
    }
   
   
   
    // Kiểm tra các trường input nếu trống
    if (name === '') {
      errorUser.innerHTML="Please enter Username!";
      return false;
    }
     else{
        errorUser.style.display="none";
    }
    
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
     if (email === '') {
      errorEmail.innerHTML="Please enter Email!";
      return false;
    }
     else if (!emailPattern.test(email)) {
      errorEmail.innerHTML="Please enter a valid Email!";
      return false;
    }
     else{
        errorEmail.style.display="none";
    }
     if (password === '') {
      errorPw.innerHTML="Please enter Password!";
      return false;
    }
     else if (password.length < 6) {
//      alert('Mật khẩu phải lớn hơn hoặc bằng 8 kí tự!');
      errorPw.innerHTML="Password must be greater than or equal to 8 characters!";
      return false;
    }
    else{
        errorPw.style.display="none";
    }
    
    if (confirmPassword === '') {
      errorCfPw.innerHTML="Please enter Confirm Password!";
      return false;
    }
     else if (password !== confirmPassword) {
        errorCfPw.innerHTML="Password and confirm password do not match!";
      
      return false;
    }
     else{
        errorCfPw.style.display="none";
    }
    
    // if (role === '') {
    //   errorRole.innerHTML="Please enter Role!";
    //   return false;
    // }
    //  else{
    //     errorRole.style.display="none";
    // }
//     ||  === '' ||  === '' ||  === '' ||  === ''
    
    // Kiểm tra tính hợp lệ của email
   
    
    // Kiểm tra tính hợp lệ của mật khẩu
    
    
    
    // Thực hiện gọi hàm API tạo người dùng
    callCreateAPI();
  }
  
 async function callCreateAPI() {
        let name = document.getElementById("name1").value;
        let email = document.getElementById("email1").value;
        let password = document.getElementById("pass1").value;
        let cfpassword = document.getElementById("cfpass1").value;
        let role = document.getElementById("role1").value;

        let url = "https://localhost:8080/FreshMarket/api/user/?email=" + email;
        const response = await fetch(url);
        const res = await response.json();

        if(cfpassword.toString() != password.toString()){
            alert("The password does not match!!");
        }
        else{
            if (res != "0") {
                alert("Email has been existed!!");
            } else {
                const data = {
                    email: email,
                    passWord: password,
                    userName: name,
                    role: role
                };
                const response = await fetch('https://localhost:8080/FreshMarket/api/user/', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset:UTF-8'
                    },
                    body: JSON.stringify(data)
                });
                const res = await response.json();
                if (res == "1") {
                    alert("Create successful!!");
                    location.reload();
                } else {
                    alert("Create failed!!");
                }
            }
        }
//    alert('Người dùng đã được tạo thành công.');
  }
  
 