function showProductsInfo(row) {
    var idProduct=document.getElementById('id');
    // var cateID  =document.getElementById('cateID');
    // var brandID=document.getElementById('brandID');
    var color=document.getElementById('color');
    var name=document.getElementById('name');
    var price=document.getElementById('price');
    var des=document.getElementById('des');
    var brand=document.getElementById('brand');
    var method=document.getElementById('method');
    var status=document.getElementById('status');
    var quantity=document.getElementById('quantity');
    var image=document.getElementById('image');
    var cells = row.getElementsByTagName('td');
    var rowData = [];
    for (var i = 0; i < cells.length; i++) {
        rowData.push(cells[i].innerHTML);
    }
    idProduct.value=rowData[0];
    // cateID.value=rowData[1];
    // brandID.value=rowData[2];
    color.value=rowData[4];
    name.value=rowData[3];
    price.value=rowData[5];
    des.value=rowData[7];
    brand.value=rowData[6];
    for (var i = 0; i < brand.options.length; i++) {
        if (brand.options[i].value === rowData[6]) {
            brand.options[i].selected = true;
            break;
        }
    }
    method.value=rowData[8];
    status.value=rowData[9];
    for (var i = 0; i < status.options.length; i++) {
        if (status.options[i].value === rowData[9]) {
            status.options[i].selected = true;
            break;
        }
    }
    quantity.value=rowData[10];
    image.src = rowData[11];
    var overlay = document.getElementById("overlay");
    var formContainer = document.getElementById("formContainer");
    overlay.style.display = "block";
    formContainer.style.display = "block";
}
function hideForm() {
    var overlay = document.getElementById("overlay");
    var formContainer = document.getElementById("formContainer");
    overlay.style.display = "none";
    formContainer.style.display = "none";
}
