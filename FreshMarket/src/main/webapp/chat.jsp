<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 80px;*/
            }
            .main{
                width:82.5%;
                margin-left: 50px;
                /*background-color: #fff;*/
            }
             .logo{
                margin-left:10px;
                padding-top:18px;
                padding-bottom:28px;
                color:#80BB35;
            }
            
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:85%;
                  border-radius:10px;
                  margin-top:30px;
                  margin-left:20px;
                  border:1px solid #ccc;
                  padding: 5px 10px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                 .noti-icon{
                    display:flex;
                     position: absolute;
                    right:10px;
                    /*margin-left:700px;*/
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                 .report{
                    width:101%;
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
               .report button{
                    margin-top:30px;
                     position: absolute;
                    right:60px;
                    /*margin-left:850px;*/
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 300px;
                    height:auto;
                }
              
              
                .co-left-top{
                   
                }
                .co-right{
                    border-radius: 5px;
                    height: 650px;
                    /*width: 100%;*/
                    
                    margin-left: 300px;
                    
                    background-color: #F5F5F5;
                    padding:20px;
                    /*margin:-40px 0px ;*/
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 150px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                .product{
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  20px;
                    padding: 0;
                    display: block;
                    background-color: white;
/*                    height:300px;
                    */width:100%;
                    margin-right:15px;
                    display:block;
                    justify-content: center;
                    text-align: center;
                }
                .product-list{
                    
                 background-color: #F5F5F5;
                    width:1490px;
                    border-radius: 5px;
                    left:0px;
                    margin:-40px 30px ;
                    /*padding-left:  50px;*/
                    padding:20px;
                    height: 400px;
                }
                #product-list img{
                    height:200px;
                    width:200px;
                }
                .co-right button{
                    width:92%;
                    height:50px;
                    margin:10px;
                    border-radius: 50px;
                    border:none;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    color:white;
                    font-weight: bold;
                    font-size: 20px;
                }
                .co-right p{
                    
                }
                .product{
                    cursor: pointer;
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  10px;
                    border-radius: 10px;
                    padding-top: 5px;
                    display: flex;;
                    background-color: white;
                    height:60px;
                    /*width:100px;*/
                    margin-right:20px;
               
                }
                .product:hover{
                     background-image: linear-gradient(0, #80BB35, #80BB35);
                     color:white;
                }
              
                
                  button:hover{
                     cursor: pointer;
                 }
                .title{
                    font-weight: bold;
                    font-size:14px;
                    display:flex;
                    background-color: #F5F5F5;
                    width:1440px;
                    border-radius: 5px;
                    left:0px;
                    
                    margin:-40px 30px 20px 30px ;
                    /*margin-right: 300px;*/
                    padding:10px 50px 20px 50px;
                }
                .title p{
                      width: 23.1%;
                    /*padding-right: 250px;*/
                }

                td{
                    text-align: center;
                    /*margin-left: -20px;*/
/*                    display:block;
                    justify-content: center;
                    align-items: center;*/
                    width: 300px;
                    padding-top:10px;
                    height:40px;
                   
                    background-color: white;
                }
              
              

                 
  
  
    .ctr-button1{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button1 input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
      .ctr-button1 button{
           padding: 5px;
    height: 36px;
    border: 2px solid red;
    color: red;
    /* margin-top: 10px; */
    margin-right: 60px;
    width: 100px;
    border-radius: 10px;
        
    }
    .ctr-button1 button:hover{
        border:none;
    }
    .ctr-button1 input:hover{
        cursor: pointer;
    }
    .manager-user1{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload1{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload1{
        display:none;
        padding-bottom:5px;
    }
    .desss{
         width: 300px; /* Độ rộng của ô (cell) */
         height:auto;
   display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
    }
     li a:hover{
        color:#80BB35;
        font-size:20px;
        transition: 0.3s;
        /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
        /*color: white;*/
        
    }
     #options {
        
            display: none;
        }
      
      .container-one{
/*                    background-color: #F0DFDF;*/
                    
                    /*margin-left: -5px;*/
                    /*position: ;*/
                    top:0;
                }
                .container-one{
                    display:flex;
                    margin-left: 40px;
                }
                .co-left{
                    background-color: #F5F5F5;
                    width:65%;
                    border-radius: 5px;
                    left:0px;
                        height: 650px;
                    
           
/*                    left
                    */margin:-40px 0px ;
                    padding:20px;
                }
                .co-left-top{
                    /*height:800px;*/
                    display:flex;
                    
                }
                .co-right{
                    border-radius: 5px;
                    height: 650px;
                    width:75%;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 200px ;
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 130px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                .primary{
                    display:flex;
                }
                .co-left-top p{
                    margin-left: 15px;
                }
                table{
                    padding-top:40px;
                }
                td{
                    padding:5px 30px;
                }
                .status-1{
                    display:flex;
                    font-weight: bold;
                    font-size: 18px;
                }
                .status-2:hover{
                    background-color: #B6A8A8;
                }
                .status-2{
                    display:flex;
                }
                .status-1-title{
                    display:flex;
                    padding-top:35px;
                    /*margin-left: -60px;*/
                    
                }
                .status-check{
                    height:30px;
                    margin-right: 40px;
                    /*margin-left:-100px;*/
                }
                .name-title{
                    width:200px;
                    display:flex;
                }
                .name-title img{
                    margin-top:-5px;
                }
                .name-title p{
                    margin:1px -5px;
                }
                .status-title{
                    width:480px;
                }
                .detail-title,.date-title{
                    /*padding-right: 220px;*/
                    width:150px;
                }
                .chat-box{
                    display:flex;
                    height:70px;
                    padding-top:5px;
                }
                
                .chat-box .inbox{
                    margin-top:-15px;
                    margin-left: -15px;
                    width:300px;
                }
                .chat-box img{
                    margin-left: 10px;
                }
                .chat-box:hover{
                    background-color: #B6A8A8;
                }
                .time {
                    color:#ccc;
                }
                .name-cus{
                    font-weight: bold;
                }
                .acc{
                    display:flex;
                }
                .acc1{
                    display: flex;
                    /*float:left;*/
                    margin-left: 450px;
                }
                .mes{
                    background-color: #4c6ef8;
                    color:white;
                    padding:20px;
                    width:70%;
                    border-radius: 10px;
                }
                .mes1{
                    background-color: #4c6ef8;
                    color:white;
                    padding:20px;
                    /*width:70%;*/
                    border-radius: 10px;
                }
                .ava img{
                    padding-top:15px;
                }
                .acc{
                    margin-bottom: 20px;
                }
                .acc-time{
                    color:#bbb;
                    margin-top:-10px;
                    font-size:15px;
                }
                .manager{
                    margin-left: 300px;
                    margin-top:100px;
                }
                .send{
                    padding:12px 22px;
                    background-color: #4c6ef8;
                    color:white;
                    border:none;
                    border-radius: 10px;
                }
                .send:hover{
                    cursor: pointer;
                    box-shadow: rgba(80, 143, 244, 0.3) 4px 4px;
                }
                .form-input1{
                    margin-top:0px;
                    margin-bottom:40px;
                }
                .inb{
                    height: 15%;
                }
                .chats{
                    height:480px;
                    width:100%;
                     overflow-x:hidden;
                     overflow-y:auto;
                }
                #list-chat{
                     height:480px;
                    width:100%;
                     overflow-x:hidden;
                     overflow-y:auto;
                }
                
                .chat-container {
     
                    padding: 10px;
                    max-width: 90%;
                    margin: 0 auto;
                    height:auto;
                  }
                  .chat-container .message{
                       display:block;
                    width:fit-content;
                  }

                  .message .sender {
                    font-weight: bold;
                  }

                  .message {
                      display:block;
                    width:fit-content;
                    margin-bottom: 30px;
                    position:relative;
                  }

                  .message.from-alice {
                    /*width:100%;*/
                    float:left;
                  }

                  .message.from-bob {

                    float:right;
                  }

                  .from {
                    font-weight: bold;
                    margin-right: 5px;
                  }

                  .time {
                    font-size: 0.8em;
                    color: #999;
                  }

                  .message-content {
                    background-color: #f1f0f0;
                    padding: 5px 10px;
                    border-radius: 5px;
                  }

                  .from-alice .message-content {
                    background-color: #e2f0fd;
/*                    display:block;
                    width:fit-content;*/
                    margin: 6px 400px 6px 0px;;;
                    width:auto;
                    height:20px;
                  }

                  .from-bob .message-content {
                      display:block;
                    width:fit-content;
                    background-color:#00BFA5;
                      margin: 5px 5px 5px 800px;;
                        height:20px;
                  }
                  .acc{
                    position:absolute;
                    right:0px;

                  }
                  .message1{
                      position:absolute;
                    right:0px;

                  }
                  .customer{
                      display:flex;
                      height: 60px;
                      /*background-color: #9ca3af;*/
                      border-bottom: 1px solid #ccc;
                      padding:5px;
                      border-radius: 10px;
                      margin-bottom: 20px;
                  }
                  li a:hover{
                            transition: all 0.5s ease;
                            display: flex;
                            padding: 12px 12px;
                            font-weight: bold;
                            font-size: 18px;
                            color: #80bb35;
                            background-color: rgba(0, 0, 0, 0.05);
                            border-left: 3px solid #80bb35;
                    }
        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <h2> G10 MARKET </h2>
                    <!--<img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>-->
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Users</a>
                    </li>
                </ul>

            </div>
            
            
            <!--list Cate-->
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="products.jsp">Categories</a>
                </div>                
                <ul>
                    <c:forEach items="${categories}" var="cate">
                        <li><a class="active" href="products.jsp?cateId=${cate.id}">${cate.name}</a></li>
                    </c:forEach>
                </ul>
            </div>
            
            
             <div class="brands">
                <div class="b-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="listBrand.jsp">Brand</a>
                </div>                
           
            </div>
            
            
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                     <li>
                        <a class="active" href="wallet_admin.jsp">Wallet</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">

                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                            <h1>Messages</h1>
                        </div>
                    </div>
                </div>
                <div class="container-one">
                        <div class="co-right">
                            <h2>Chat List</h2>
                            <input type="text" class="form-input1" placeholder="Search content here...">
                            <div id="list-chat"></div>
                        </div>
                    <div class="co-left" style="display: none">
                        <div class="customer">
                            <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png">
                            <h3 id="customerName"></h3>
                        </div>
                        <div class="chats">
                            <div class="chat-container" id="chatContainer"></div>
                        </div>

                        <div class="inb">
                            <input type="text" onkeydown="handleKeyPress(event)" class="form-control form-input-inbox"  id="message-input" placeholder="Type..."> 
                            <button onclick="sendMessage()" class="send">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  <script>

    // Dữ liệu chat giữa hai người
    function handleChatBoxClick(userId,userName) {
        var chatContainer = document.getElementById("chatContainer");
        chatContainer.innerHTML = "";
        sessionStorage.setItem("tempUserId",userId);

        var chatData = [];
        var coLeft = document.querySelector(".co-left");
        var customerName = document.getElementById("customerName");
        customerName.textContent = userName;

        coLeft.style.display = "block";

        let adminId = '<%= session.getAttribute("userId") %>';
        const requestBody = {
            from: userId,
            to: adminId
        };
        let url = "https://localhost:8080/FreshMarket/api/chats/get-conversation"  ;
        fetch(url, {
            method: 'POST',
                headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(requestBody)
        })
            .then(function(response) {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("Failed to fetch user data");
                }
            })
            .then(function(data) {
                data.forEach(function(chat) {
                    let time = chat.createDate;
                    let from = "";
                    if(chat.from.toString() === userId){
                        from = "Alice";
                    }
                    else{
                        from = "Bob";
                    }
                    let message = chat.message;

                    /// gen messs
                    var messageDiv = document.createElement("div");
                    messageDiv.classList.add("message");

                    var fromDiv = document.createElement("div");
                    fromDiv.classList.add("from");
                    fromDiv.textContent = userName.toString();

                    var timeDiv = document.createElement("div");
                    timeDiv.classList.add("time");
                    timeDiv.textContent = time.toString();

                    var messageContentDiv = document.createElement("div");
                    messageContentDiv.classList.add("message-content");
                    messageContentDiv.textContent = message.toString();

                    if (from.toString() === 'Alice') {
                        messageDiv.appendChild(fromDiv);
                        messageDiv.appendChild(messageContentDiv);
                        messageDiv.appendChild(timeDiv);
                        messageDiv.classList.add('from-alice');
                    }
                    else
                    {
                        messageDiv.appendChild(messageContentDiv);
                        messageDiv.appendChild(timeDiv);
                        messageDiv.classList.add('from-bob');
                        timeDiv.classList.add('acc');
                    }

                    chatContainer.appendChild(messageDiv);
                });
            })
            .catch(function(error) {
                console.error("Error:", error);
            });
    }
  </script>
  <script>
      function sendMessage() {

          var chatContainer = document.getElementById("chatContainer");


          const messageInput = document.getElementById("message-input");
          const message = messageInput.value;

          var currentTime = new Date();

          var year = currentTime.getFullYear();
          var month = currentTime.getMonth() + 1; // Tháng trong JavaScript đếm từ 0, nên cần +1
          var day = currentTime.getDate();
          var hours = currentTime.getHours();
          var minutes = currentTime.getMinutes();
          var seconds = currentTime.getSeconds();

          var timeReal=year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;

          var timeDiv = document.createElement("div");
          timeDiv.classList.add("time");
          timeDiv.textContent = timeReal;

          var messageContentDiv = document.createElement("div");
          messageContentDiv.classList.add("message-content");
          messageContentDiv.textContent = message;

          // Tạo phần tử tin nhắn
          const messageElement = document.createElement("div");
          messageElement.className = "message1";
          messageElement.className = "from-bob";
          messageElement.classList.add('message');

          timeDiv.classList.add('acc');

          messageElement.appendChild(messageContentDiv);
          messageElement.appendChild(timeDiv);

          // Thêm tin nhắn vào vùng hiển thị

         if(message !==''){
                    chatContainer.appendChild(messageElement);
                }
          // Xóa nội dung trong input
          messageInput.value = "";
          chatContainer.scrollTop = chatContainer.scrollHeight;
          /// add mess vao database
          let userId = sessionStorage.getItem("tempUserId");
          let adminId = '<%= session.getAttribute("userId") %>';
          const requestBody = {
              from: adminId,
              to: userId,
              mess : message
          };
          let url = "https://localhost:8080/FreshMarket/api/chats/send-message"  ;
          fetch(url, {
              method: 'POST',
              headers: {
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify(requestBody)
          });
      }
       function handleKeyPress(event) {
      if (event.keyCode === 13) { // Kiểm tra nếu phím nhấn là Enter (keyCode = 13)
        event.preventDefault(); // Ngăn chặn hành vi mặc định của Enter (chuyển dòng mới)
        sendMessage(); // Gọi hàm gửi tin nhắn
      }
    }
  </script>
    </body>

    <script>
        let url = "https://localhost:8080/FreshMarket/api/users/get-all" ;
        fetch(url)
            .then(function(response) {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("Failed to fetch user data");
                }
            })
            .then(function(data) {
                var chatContainer = document.getElementById("list-chat");

                data.forEach(function(user) {
                    var chatBox = document.createElement("div");
                    chatBox.className = "chat-box";

                    var img = document.createElement("img");
                    // img.src = user.avatar;
                    img.src = "https://demo.dashboardpack.com/sales-html/img/client_img.png";
                    img.height = "45";

                    var inbox = document.createElement("div");
                    inbox.className = "inbox";
                    var nameCus = document.createElement("p");
                    nameCus.className = "name-cus";
                    nameCus.textContent = user.userName;

                    var defaultText = document.createElement("p");
                    defaultText.textContent = "i know you are doing great";

                    var time = document.createElement("div");
                    time.className = "time";
                    time.textContent = "Last seen";

                    inbox.appendChild(nameCus);
                    inbox.appendChild(defaultText);
                    chatBox.appendChild(img);
                    chatBox.appendChild(inbox);
                    chatBox.appendChild(time);
                    chatContainer.appendChild(chatBox);

                    var hr = document.createElement("hr");
                    chatContainer.appendChild(hr);

                    chatBox.addEventListener("click", function() {
                        handleChatBoxClick(user.id,user.userName);
                                                 var coRight = document.querySelector('.co-right');
                        coRight.style.margin="-40px 40px";
                        coRight.style.width="35%";
                        coRight.style.transition="1s";
                    });
                });
            })
            .catch(function(error) {
                console.error("Error:", error);
            });
    </script>

</html>
