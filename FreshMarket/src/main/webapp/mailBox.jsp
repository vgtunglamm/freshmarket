<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 800px;*/
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
             .logo{
                margin-left:10px;
                padding-top:18px;
                padding-bottom:28px;
                color:#80BB35;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                    padding-right: 10px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
            .form input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
              .form{

                    position: relative;
                }

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                 .noti-icon{
                    display:flex;
                     position: absolute;
                    right:10px;
                    /*margin-left:700px;*/
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                 .report{
                    width:101%;
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
               .report button{
                    margin-top:30px;
                     position: absolute;
                    right:60px;
                    /*margin-left:850px;*/
                    padding:15px 25px;
                    background-color:white;
                    
                    border:none;
                    border-radius:10px;
                    color:black;
                }
                .report button:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .report button{
                    display: flex;
                    align-items: center;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 300px;
                    height:1080px;
                }
                .container-one{
                   background-color:#ccc;
                    top:0;
                  margin-top: -40px;
                  margin-left: 2%;
                  border-radius: 10px;
                  width:96%;
                  
                }
              
                .co-left-top{
                   
                }
                .co-right{
                    border-radius: 5px;
                    height: 640px;
                    width:450px;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 0px ;
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 150px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                .mail{
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  20px;
                    padding: 0;
                    display: block;
                    background-color: white;
/*                    height:300px;
                    */width:100%;
                    margin-right:15px;
                    display:block;
                    justify-content: center;
                    text-align: center;
                }
                #mail-list{
/*                    background-color: #fff;
                    width: 100%;*/
                    background-color: #F5F5F5;
                    padding:20px;
                    height: 50px;
                    cursor: pointer;
                }
                .mail-list-item:hover{
                    background-color:#f2f6fc;
                    box-shadow: 0px 3px 10px rgba(60,64,67,.3);
                    
                }
                #product-list img{
                    height:200px;
                    width:200px;
                }
                .co-right button{
                    width:92%;
                    height:50px;
                    margin:10px;
                    border-radius: 50px;
                    border:none;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    color:white;
                    font-weight: bold;
                    font-size: 20px;
                }
                .product{
                    cursor: pointer;
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  10px;
                    border-radius: 10px;
                    padding-top: 5px;
                    display: flex;;
                    background-color: white;
                    height:60px;
                    /*width:100px;*/
                    margin-right:20px;
               
                }
                .product:hover{
                     background-image: linear-gradient(0, #80BB35, #80BB35);
                     color:white;
                }
              
                
                  button:hover{
                     cursor: pointer;
                 }
                .title{
                    font-weight: bold;
                    font-size:14px;
                    display:flex;
                    background-color: #F5F5F5;
                    width:1440px;
                    border-radius: 5px;
                    left:0px;
                    
                    margin:-40px 30px 20px 30px ;
                    /*margin-right: 300px;*/
                    padding:10px 50px 20px 50px;
                }
                .title p{
                      width: 23.1%;
                    /*padding-right: 250px;*/
                }

                td{
                    text-align: center;
                }
                .product-list tr{
                    /*width:1450px*/
                    padding-right:100px;
                }
/*                .phone{
                    margin-left: -40px;
                }*/
                .phonem{
                    /*padding-right: 100px;*/
                    margin-left: -60px;
                 }
                 .name{
                     padding-left: 60px;
                 }
                 .email{
                     padding-left: 45px;
                 }
                 thead{
                     padding-bottom: 20px;
                 }
                 .ma{
                     padding-right:250px;
                     /*border-right: 1px solid #ccc;*/
                 }
                  .gia{
                      width:100px;
                 }
                  .mt{
                      width:100px;
                 }
                 #product-title{
                       height: auto;
                    background-color: #F5F5F5;
                    width:94%;
                    border-radius: 5px;
                    left:0px;
/*                    left
                    */margin:-40px 30px ;
                    padding:20px;
                    /*position: relative;*/
                    display:flex;
                    font-weight: bold;
                }
                .idtitle{
                     padding:  0 250px 0 180px;
                }
                .nametitle{
                     padding-right: 190px;
                }
                .pricetitle{
                     width:380px;
                     
                }
                .mttitle{
                     width:400px;
                }
                .highlight{
                    padding: 5px 5px;
                    
                    width: 100%;
                   
                }
                
                
                .select{
                    display: flex;
                    justify-content: right;
                    align-items: center;
                    padding: -20px;
                    padding-bottom: 10px;
                }

                .select button{
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    padding: 10px;
                    border:none;
                    color:white;
                    border-radius:12px;
                    margin-right: 30px;
                    width: 70px;
                    height: 45px;
                }

                .done-button:hover{
                    /*padding:20px;*/
                    /*display:block;*/
                    cursor: pointer;
                }
                

                button:hover{
                    cursor: pointer;
                }

                                thead tr{
                    height:40px;
                    font-weight: bold;
                }
               
                 
                /*///////////////////*/
                 .overlay {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }
    
     .overlay1 {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }

    /* CSS cho form */
    #formContainer {
      position: fixed;
      height:800px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      box-shadow: 0px 3px 10px rgba(60,64,67,.3);
      display: none;
    }
    
        #formContainer1 {
      position: fixed;
      height:800px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
 
    /* CSS cho nút đóng */
    #closeButton {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input{
        display:flex;
    }
  
    .form-left{
        width:500px;
    }
    .form-right{
        width:500px;
    }
    .title-product{
        display:flex;
    }
    .ctr-input input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    #brandID{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    
    #cateID{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    .ctr-button{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
    .ctr-button input:hover{
        cursor: pointer;
    }
    .manager-user{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload{
        display:none;
        padding-bottom:5px;
    }
    
    /**CSS cho add product*/
     #closeButton1 {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input1{
        display:flex;
    }
  
    .form-left1{
        width:500px;
    }
    .form-right1{
        width:500px;
    }
    .title-product1{
        display:flex;
    }
    .ctr-input1 input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    .ctr-button1{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button1 input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
      .ctr-button1 button{
           padding: 5px;
    height: 36px;
    border: 2px solid red;
    color: red;
    /* margin-top: 10px; */
    margin-right: 60px;
    width: 100px;
    border-radius: 10px;
        
    }
    .ctr-button1 button:hover{
        border:none;
    }
    .ctr-button1 input:hover{
        cursor: pointer;
    }
    .manager-user1{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload1{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload1{
        display:none;
        padding-bottom:5px;
    }
    .desss{
         width: 300px; /* Độ rộng của ô (cell) */
         height:auto;
   display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
    }
     li a:hover{
        color:#80BB35;
        font-size:20px;
        transition: 0.3s;
        /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
        /*color: white;*/
        
    }
     #options {
            display: none;
        }
                .fix{
                    width: 600px;
                    position: fixed;
                    top: 0;
                    bottom: 0;
                    right: 0;
                     display: none;
                    
                }
                .compose-box{
                    position: absolute;
                    border-radius: 10px;
                    height: 610px;
                    width: 600px;
                    background-color: #fff;
                    bottom: 10px;
                    right: 10px;
                   box-shadow: 0px 3px 10px rgba(60,64,67,.3);

                }
                .compose-box-title{
                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                    height: 50px;
                    border-radius: 10px 10px 0 0;
                    background-color: #f2f6fc;
                }
                .compose-box-title h4{
                    padding:0 5px;
                    
                }
                .compose-box-title img{
                    cursor: pointer;
                }
                .compose-box input{
                    border: none;
                    width: 590px;
                    padding: 5px;
                }
                .send-to{
                    height: 30px;
                    outline: none;
                }
                .test{
                    border-bottom: 1px solid #F5F5F5;
                }
                .mail-title{
                    height: 36px;
                }
                .mail-title:focus-visible{
                    outline: none;
                }
                .Note_text{
                    height: 400px;
                    background: #fff;
                    border:none;
                    -webkit-box-sizing: border-box; 
                    box-sizing: border-box;
                    resize: none;
                    width: 100%;
                    padding: 5px;
                }
                .Note_text:focus-visible{
                    outline: none;
                }
                .compose-box--footer{
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                }
                .compose-box--footer h4{
                    height: 20px;
                    width: 50px;
                    border: 1px solid #2064a0;
                    background-color: #2064a0;
                    border-radius: 10px;
                    text-align: center;
                    padding: 5px 20px;
                    margin-left: 10px;
                    color: #fff;
                    cursor: pointer;
                }
                .compose-box--footer img{
                    height: 25px;
                    cursor: pointer;
                }
                .from,  .time{
                   display: flex; 
                }
    li a:hover{
            transition: all 0.5s ease;
            display: flex;
            padding: 12px 12px;
            font-weight: bold;
            font-size: 18px;
            color: #80bb35;
            background-color: rgba(0, 0, 0, 0.05);
            border-left: 3px solid #80bb35;
    }
/*        
        .dropdown {
  position: relative;
  display: inline-block;
}

#optionList {
  display: none;
  position: absolute;
  z-index: 1;
}

#optionList li {
  list-style-type: none;
  background-color: #f9f9f9;
  padding: 5px;
  cursor: pointer;
}

#optionList li:hover {
  background-color: #e5e5e5;
}*/

        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <h2>G10 MARKET</h2>
                    <!--<img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>-->
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Users</a>
                    </li>
                </ul>

            </div>
            
            
            <!--list Cate-->
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="products.jsp">Categories</a>
                </div>                
                <ul>
                    <c:forEach items="${categories}" var="cate">
                        <li><a class="active" href="products.jsp?cateId=${cate.id}">${cate.name}</a></li>
                    </c:forEach>
                </ul>
            </div>
            
            
             <div class="brands">
                <div class="b-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="listBrand.jsp">Brand</a>
                </div>                
                <ul>
                    <!--Render list brand-->
                    
                        <li><a class="active" href="listBrand.jsp">Viet Nam</a></li>
                   
                </ul>
            </div>
            
            
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Edittor</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>
                  <input type="text" id="search" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                        <h1>Mail Box</h1>
                        <p>Tất cả thư của bạn</p>
                    </div>
                    <div>
                        <!--Button thêm product-->
                        <button onclick="showForm()" type="button">
                             <img  src="https://www.gstatic.com/images/icons/material/system_gm/1x/create_black_24dp.png" style="height: 20px;">
                             Add Email
                        </button>
                    </div>
                        
                        
                        <div id="overlay1" class="overlay1"></div>

                <div id="formContainer1">
                    <div class="title-product1">
                        <h2> Add Product</h2>
                        <span id="closeButton1" onclick="hideForm1()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user1">
                      <div class="ctr-input1">
                          <div class="form-left1">
                            
                              <label for="file-upload1">
                                  <img src="">
                                  <i class="fas fa-camera"></i>
                              </label>
                            <input type="file" id="file-upload1"  />
                            <br>
                            <br>
                            <br>
                             <label for="cateID1">Cate Id</label>

                            <input type="text" id="cateID1" name="name"><br><br>

                            <label for="brandID1">Brand Id</label>

                            <input type="text" id="brandID1" name="number"><br><br> 

                             <label for="color1">Color</label>

                            <input type="text" id="color1" name="date"><br><br> 
           
                           </div>
                       <div class="form-right1">
                            <label for="name1">Product Name</label>

                            <input type="text" id="name1" name="date"><br><br> 
                            <label for="price1">Price</label>

                            <input type="text" id="price1" name="name"><br><br>
                           
                            <label for="des1">Description</label>

                            <input type="text" id="des1" name="text1"><br><br>

                            <label for="brand1">Brand</label>

                            <input type="text" id="brand1" name="brand"><br><br>
                            
                            <label for="method1">Transport Method</label>

                            <input type="text" id="method1" name="phone"><br><br>
                            
                             <label for="status1">Status</label>

                            <input type="text" id="status1" name="phone"><br><br>
                            
                            <label for="quantity1">Quantity</label>

                            <input type="text" id="quantity1" name="name"><br><br>
                            
                            


                            
                      </div>
                      </div>
                      <div class="ctr-button1">
                          <button onclick="callCreateAPI()" type="button">Add</button>
                     
                          <input onclick="hideForm1()" type="submit" value="Cancel">
                      </div>


                  </form>
                </div>
                        
                        
                    </div>
                   </div>
                </div>

                <div class="container-one">
                <table class="highlight">
                    <thead>
                        <tr>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Content</th>
                            <th>Time</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody id="mail-list" class="list">
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        <tr class="mail-list-item">
                            <td><input type="checkbox" class="status-check" style="cursor:pointer"></td>
                            <td><p>Mark Wiyns</p></td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipsicing elit, se...</td>
                            <td> 12:22 PM</td>
                            <td><img src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png" class = "delete"></td>
                        </tr>
                        
                          
                    </tbody>
                </table>
                     <!--<ul class="listPage"></ul>-->

                                             
                    
             
            </div>
            <div id="overlay" class="overlay"></div>
            <div id="formContainer">
                    <div class="title-product">
                        <h2>Mail Content</h2>
                        <span id="closeButton" onclick="hideMail()">&#x2716;</span>

                    </div>
                    <hr>
                    <div class="from">
                        <p style="padding-right:5px; font-size: 14px">From: </p>
                        <p style="font-weight: bold; font-size: 14px">Mark Wiyns</p>
                    </div> 
                    <div class="time">
                        <p style="padding-right:5px; font-size: 14px">Time: </p>
                        <p style="font-weight: bold; font-size: 14px"> 12:22 PM</p>
                    </div>
                    <div class="Content">
                        <p id ="mail-title" style="text-transform: uppercase; font-weight: bold">Lorem ipsum dolor sit amet, consectetur adipsicing elit </p>
                        <p id ="mail-content"> Tất cả mọi người đều được chào đón tại
                                                Apple Store trực tuyến mới. Kể từ hôm nay, bạn có thể
                                                mua sắm sản phẩm Apple yêu thích
                                                và nhận được sự trợ giúp phù hợp khi cần.
                        </p>
                    </div>
                    
                </div>
                                <div class="fix">
                                    <div class="compose-box">
                                        <div class="compose-box-title">
                                            <h4>Thư mới</h4>
                                            <img onclick="hideForm()" src="https://ssl.gstatic.com/ui/v1/icons/mail/rfr/ic_close_16px_1x.png">
                                        </div>
                                        <div id = "reset">
                                            <div class="test">
                                            <input type="text", placeholder="Đến" class="send-to">
                                            </div>
                                            <div class="test">
                                                <input type="text", placeholder="Tiêu đề" class="mail-title">
                                            </div>
                                            <div >
                                                <textarea class="Note_text"></textarea>
                                            </div>
                                        </div>
                                        <div class="compose-box--footer" >
                                            <h4 onclick="sendForm()">Send</h4>
                                            <img onclick="hideForm()" src="https://ssl.gstatic.com/ui/v1/icons/mail/gm3/1x/delete_baseline_nv700_20dp.png">
                                        </div>
                                    </div>
                                </div>
          <script src="./javascript_admin/showProductsInfo.js"></script>
           <script src="./javascript_admin/addProduct.js"></script>
           <script src="./javascript_admin/dynamicPagination.js"></script>
        </div>
               
    </body>
</html>
<script>
//      function showOptions() {
//  document.getElementById("optionList").style.display = "block";
//}
//
//function hideOptions() {
//  document.getElementById("optionList").style.display = "none";
//}
            var check = 0;
            function showForm(){
                var x = document.getElementsByClassName("fix");
                x[0].style.display = "flex";
            }
            function sendForm(){
                hideForm();
                alert("Email sent successfully");
            }
            function hideForm(){
                var x = document.getElementsByClassName("fix");
                x[0].style.display = "none";
            }
            var x = document.getElementsByClassName("mail-list-item");
            var deleted = document.getElementsByClassName("delete");
            for (let j = 0; j < deleted.length; j++) {
                deleted[j].addEventListener('click', function () {
                    x[j].style.display = "none";
                    check = 1;
                });
            }
            var tick = document.getElementsByClassName("status-check");
            for (let j = 0; j < tick.length; j++) {
                tick[j].addEventListener('click', function () {
                    check = 1;
                });
            }
            
            function showMail(){
                var x = document.getElementById("formContainer");
                x.style.display = "block";
            }
            function hideMail(){
                var x = document.getElementById("formContainer");
                x.style.display = "none";
            }
            var y = document.getElementsByClassName("mail-list-item");
            for (let j = 0; j < deleted.length; j++) {
                y[j].addEventListener('click', function () {
                    if (check !== 1) showMail();
                    check = 0;
                });
            }
           
</script>

