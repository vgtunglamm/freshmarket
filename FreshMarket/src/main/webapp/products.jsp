<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sales Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <style>
            
            body{
                padding:0;
                margin:0;
                display:flex;
                font-family: 'Roboto' , sans-serif;
            }
            .menu{
                box-shadow: rgba(80, 143, 244, 0.2) 0px 5px 35px;
                height:100%;
                width: 14%;
                background-color: white;
                font-size:18px;
                padding-left:45px;
            }
            .container{
                /*margin-left: 800px;*/
            }
            .main{
                width:85%;
                /*background-color: #fff;*/
            }
           .logo{
                margin-left:10px;
                padding-top:18px;
                padding-bottom:28px;
                color:#80BB35;
            }
            .dashboard ul{
                /*display:none;*/
            }
            .dashboard,.app,.chart,.forms,.products,.table,.pages{
                padding-bottom: 15px;
                color:#8890b5;
            }
            li a{
                
                color:#8890b5;
            }
       
            li{
                list-style:none;
                padding-bottom:20px;
            }
            img{
                padding-right:20px;
            }
            a{
                
                text-decoration: none;
            }
            .menu {
                background-color: #F5F5F5;
                float: left;
                height: 940px;
                border-radius: 10px;
                /*width: 65px;*/
                overflow-y: scroll;
                position: fixed;
            }
           .menu::-webkit-scrollbar-thumb {
                background: red; 
                border-radius: 10px;
              }
              input{
                  height: 35px;
                  width:300px;
                  border-radius:50px;
                  margin-top:30px;
                  margin-left:20px;
              }
              .header{
                  height:100px;
                  border-bottom: 1px solid #ccc;
                  display: flex; 
              }
              
/*              .form{

                    position: relative;
                }*/

                .form img{

                    position: absolute;
                    top:42px;
                    left: 30px;
                    color: #9ca3af;
                    margin-right:20px;

                }


                .form-input{

                    height: 40px;
                    text-indent: 33px;
                    border-radius: 50px;
                    border: 1px solid rgba(100,197,177,.6);
                }
                 .noti-icon{
                    display:flex;
                     
                    position: absolute;
                    right:10px;
                    margin-left:68%;
                    margin-top:40px;
                }

                .profile{
                    margin-top:-10px;
                    margin-left:20px;
                }
                 .report{
                    width:101%;
                    /*margin-left: -5px;*/
                    font-size:14px;
                    padding-left: 20px;
                    height: 160px;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                   
                    
                }
                .gop{
                    display:flex;
                }
                .in button{
                   margin-left: 500px;
                   margin-top:40px;
                    padding:15px 25px;
                    background-color:#567aed;
                    
                    border:none;
                    border-radius:10px;
                    color:white;
                }
                .in button:hover{
                    box-shadow: rgba(80, 143, 244, 0.2) 5px 5px 5px 5px;
                    cursor: pointer;
                }
               .report .addprd{
                    margin-top:30px;
                    position: absolute;
                    right:60px;
                    /*margin-left:1240px;*/
                    padding:15px 25px;
                    background-color:white;
                    width:130px;
                    border:none;
                    border-radius:10px;
                    color:black;
                    z-index:1;
                    display: flex;
                    justify-content: flex-end;


                }
                .report .addprd:hover{
                    background-color: #567aed;
                    color:white;
                    cursor: pointer;
                }
                .link{
                     color:white;
                     padding-left: 20px;
                }
                .main{
                    margin-left: 16%;
                    height:1080px;
                }
                .container-one{
                   background-color:#ccc;
                    top:0;
                  margin-top: -40px;
                  margin-left: 2%;
                  border-radius: 10px;
                  width:96%;
                  
                }
              
                .co-left-top{
                   
                }
                .co-right{
                    border-radius: 5px;
                    height: 640px;
                    width:450px;
                    background-color: #F5F5F5;
                    padding:20px;
                    margin:-40px 0px ;
                }
                .co-left-bottom{
                    display:flex;
                    border-radius: 5px;
                    height: 150px;
                    width:1000px;
                    background-color: #F5F5F5;
                    margin:60px 30px ;
                    padding:20px;
                }
                .product{
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  20px;
                    padding: 0;
                    display: block;
                    background-color: white;
/*                    height:300px;
                    */width:100%;
                    margin-right:15px;
                    display:block;
                    justify-content: center;
                    text-align: center;
                }
                .product-list{
                    
                 background-color: #F5F5F5;
                    width:1490px;
                    border-radius: 5px;
                    left:0px;
                    margin:-40px 30px ;
                    /*padding-left:  50px;*/
                    padding:20px;
                    height: 400px;
                }
                #product-list img{
                    height:200px;
                    width:200px;
                }
                .co-right button{
                    width:92%;
                    height:50px;
                    margin:10px;
                    border-radius: 50px;
                    border:none;
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    color:white;
                    font-weight: bold;
                    font-size: 20px;
                }
                .product{
                    cursor: pointer;
                    /*position: absolute;  Đặt phần tử con có vị trí tuyệt đối */
                   list-style-type: none;
                    margin-bottom:  10px;
                    border-radius: 10px;
                    padding-top: 5px;
                    display: flex;;
                    background-color: white;
                    height:60px;
                    /*width:100px;*/
                    margin-right:20px;
               
                }
                .product:hover{
                     background-image: linear-gradient(0, #80BB35, #80BB35);
                     color:white;
                }
              
                
                  button:hover{
                     cursor: pointer;
                 }
                .title{
                    font-weight: bold;
                    font-size:14px;
                    display:flex;
                    background-color: #F5F5F5;
                    width:1440px;
                    border-radius: 5px;
                    left:0px;
                    
                    margin:-40px 30px 20px 30px ;
                    /*margin-right: 300px;*/
                    padding:10px 50px 20px 50px;
                }
                .title p{
                      width: 23.1%;
                    /*padding-right: 250px;*/
                }
               
                td{
                    text-align: center;
                    /*margin-left: -20px;*/
/*                    display:block;
                    justify-content: center;
                    align-items: center;*/
                    width: 300px;
                    padding-top:10px;
                    height:40px;
                   
                    background-color: white;
                }
                .product-list tr{
                    /*width:1450px*/
                    padding-right:100px;
                }
/*                .phone{
                    margin-left: -40px;
                }*/
                .phonem{
                    /*padding-right: 100px;*/
                    margin-left: -60px;
                 }
                 .name{
                     padding-left: 60px;
                 }
                 .email{
                     padding-left: 45px;
                 }
                 thead{
                     padding-bottom: 20px;
                 }
                 .ma{
                     padding-right:250px;
                     /*border-right: 1px solid #ccc;*/
                 }
                  .gia{
                      width:100px;
                 }
                  .mt{
                      width:100px;
                 }
                 #product-title{
                       height: auto;
                    background-color: #F5F5F5;
                    width:94%;
                    border-radius: 5px;
                    left:0px;
/*                    left
                    */margin:-40px 30px ;
                    padding:20px;
                    /*position: relative;*/
                    display:flex;
                    font-weight: bold;
                }
                .idtitle{
                     padding:  0 250px 0 180px;
                }
                .nametitle{
                     padding-right: 190px;
                }
                .pricetitle{
                     width:380px;
                     
                }
                .mttitle{
                     width:400px;
                }
                .highlight{
                    padding-left: 8px;
                }
                .highlight td:hover {
                    cursor: pointer;
                    background-color: #F5F5F5;
                }
                
                .select{
                    display: flex;
                    justify-content: right;
                    align-items: center;
                    padding: -20px;
                    padding-bottom: 10px;
                }

                .select button{
                    background-image: linear-gradient(0, #80BB35, #80BB35);
                    padding: 10px;
                    border:none;
                    color:white;
                    border-radius:12px;
                    margin-right: 30px;
                    width: 70px;
                    height: 45px;
                }

                .done-button:hover{
                    /*padding:20px;*/
                    /*display:block;*/
                    cursor: pointer;
                }
                

                button:hover{
                    cursor: pointer;
                }
                .highlight td:hover {
                    cursor: pointer;
                    background-color: #80BB35;
                }
                                thead tr{
                    height:40px;
                    font-weight: bold;
                }
               
                 
                /*///////////////////*/
                 .overlay {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }
    
     .overlay1 {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }

    /* CSS cho form */
    #formContainer {
      position: fixed;
      height:860px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
    
        #formContainer1 {
      position: fixed;
      height:800px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
 
    /* CSS cho nút đóng */
    #closeButton {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input{
        display:flex;
    }
  
    .form-left{
        width:500px;
    }
    .form-right{
        width:500px;
    }
    .title-product{
        display:flex;
    }
    .ctr-input input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    #brandID{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    
    #cateID{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    .ctr-button{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button button{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        margin-top:30px;
        border-radius: 10px;
        margin-right: 20px;
        height:35px;
        
    }
    .ctr-button1 button{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        margin-top:30px;
        border-radius: 10px;
        margin-right: 20px;
        height:35px;
        
    }
    .ctr-button button:hover{
        cursor: pointer;
    }
    .manager-user{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload{
        display:none;
        padding-bottom:5px;
    }
    
    /**CSS cho add product*/
     #closeButton1 {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
                
    .ctr-input1{
        display:flex;
    }
  
    .form-left1{
        width:500px;
    }
    .form-right1{
        width:500px;
    }
    .title-product1{
        display:flex;
    }
    .ctr-input1 input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    .ctr-button1{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button1 input{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        border-radius: 10px;
        
    }
      .ctr-button1 button{
           padding: 5px;
    height: 36px;
    border: 2px solid red;
    color: red;
    /* margin-top: 10px; */
    margin-right: 60px;
    width: 100px;
    border-radius: 10px;
        
    }
    .ctr-button1 button:hover{
        border:none;
    }
    .ctr-button1 input:hover{
        cursor: pointer;
    }
    .manager-user1{
        padding-top:20px;
    }
    label{
        padding-left: 10px;
    }
    #file-upload1{
        border:none;
        padding:5px;
    }
    label img{
        height:202px;
        padding-left: 110px;
    }
    label i{
        padding-top:15px;
        padding-left: 15px;
        height:30px;
        width:30px;
        background-color: #ccc;
        border-radius: 50px;
        margin-top:-20px;
        margin-left: -50px;
    }
    #file-upload1{
        display:none;
        padding-bottom:5px;
    }
    .desss{
         width: 300px; /* Độ rộng của ô (cell) */
         /*height:auto;*/
         height:72px;
   display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
     justify-content: center;
  overflow: hidden;
  
    }
     li a:hover{
        color:#80BB35;
        font-size:20px;
        transition: 0.3s;
        /*background-image: linear-gradient(0, #80BB35, #80BB35);*/
        /*color: white;*/
        
    }
     #options {
        
            display: none;
        }

        li a:hover{
            transition: all 0.5s ease;
            display: flex;
            padding: 12px 12px;
            font-weight: bold;
            font-size: 18px;
            color: #80bb35;
            background-color: rgba(0, 0, 0, 0.05);
            border-left: 3px solid #80bb35;
    }
        select{
            height:30px;
            padding:5px 10px;
            margin-left: 20px;
            margin-top:5px;
            margin-bottom:10px;
            width:auto;
            border-radius: 0px;
            height:35px;
            border: 2px solid #ccc;
        }
        
        
        /*Add Cate*/
     .overlay2 {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: 9999;
      display: none;
    }
     #formContainer2 {
      position: fixed;
      height:410px;
      width:1000px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      background-color: #fff;
      padding: 10px 20px 20px 20px;
      z-index: 10000;
      display: none;
    }
    

    
    #closeButton2 {
        position:absolute;
      right:30px;
      top:30px;
      cursor: pointer;
      
    }
    
                
  
  
    .form-left2{
        width:500px;
    }
    .form-right2{
        width:500px;
    }
    .title-product2{
        display:flex;
    }
    .ctr-input2 input{
        padding-left:10px;
        margin-top:5px;
        margin-bottom:10px;
        width:90%;
        border-radius: 0px;
        height:30px;
        border: 2px solid #ccc;
    }
    #brandID2{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    
    #cateID2{
        height:35px;
         width:93%;
        margin-left: 20px;
         border: 2px solid #ccc;
    }
    .ctr-button2{
        display:flex;
         position:absolute;
        right:20px;
    }
    .ctr-button2 button{
        padding:5px;
       border: 2px solid red;
       color:red;
        width: 100px;
        margin-top:30px;
        border-radius: 10px;
        margin-right: 20px;
        height:35px;
        
    }
   
    .ctr-button2 button:hover{
        cursor: pointer;
    }
    .manager-user2{
        padding-top:20px;
    }
  
   
    .p-menu {
        position: relative;
        display: inline-block;
      }

      .form-container {
        display: none;
        position: absolute;
        top: 100%;
        left: 0;
      }

      .menu:hover .form-container {
        display: block;
      
      }
      .active:hover{
          cursor: pointer;
      }



    
    .link h1:hover #submenu {
      display: block;
    }
    
    #submenu {
        margin-top:-20px;
                margin-bottom:-25px;

      display: none;
      /*display:block;*/
      position: absolute;
      background-color: #f9f9f9;
      padding: 10px;
    }
    
    #submenu li {
      display: block;
    }
    .checker{
        height:50px;
        /*margin-top:-10px;*/
        /*position:relative;*/
    }
        </style>
    </head>
    <body>
        <div class="menu">
            <a href="index.jsp">
                <div class="logo">
                    <h2>G10 MARKET</h2>
                    
                    <!--<img src="https://demo.dashboardpack.com/sales-html/img/logo.png" height="20px" alt="alt"/>-->
                </div>
            </a>
            <div class="dashboard">
                <a class="d-menu" href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/dashboard.svg" alt="alt"/>
                    Account List
                </a>                
                <ul>
                    <li>
                        <a class="active" href="admin_site.jsp">Users</a>
                    </li>
                </ul>

            </div>
            
            
            <!--list Cate-->
            <div class="products">
                <div class="p-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active"  href="products.jsp">Categories</a>
                </div> 
 
                <ul class='menu1' >
                    <li class="active" onclick="addCate()">
                        <i class="fas fa-plus"></i>
                        New Cate
                        
                    </li>
                    <c:forEach items="${categories}" var="cate">
                        <div class='checker'>
                            <li id='myElement'><a class="active" href="products.jsp?cateId=${cate.id}">${cate.name}</a></li>
                        <ul id="submenu">
                        <button onclick="DeleteCate()"  type="button">Remove Cate</button>

                      </ul>
                        </div>
                    </c:forEach>
                </ul>

                 <!--Add Cate-->
                
                   <div id="overlay2" class="overlay2"></div>
                <div id="formContainer2" class="form-container">
                    <div class="title-product2">
                        <h2>New Categories</h2>
                        <span id="closeButton2" onclick="hideForm2()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user2">
                      <div class="ctr-input2">
                             <label for="cateName">Categories</label>
                             <br>
                            <input type="text" id="cateName" name="cate"><br><br>
                      </div>
                      <div class="ctr-button2">
                          <button  onclick="CreateCateAPI()" type="button">Add</button>
                          <button onclick="hideForm2()" type="submit" >Cancel</button>
                      </div>
                  </form>
                </div>
                   
            </div>
            
            
             <div class="brands">
                <div class="b-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/8.svg" alt="alt"/>
                    
                    <a class="active" href="listBrand.jsp">Brand</a>
                </div>                
                <ul>
                    <!--Render list brand-->
                    
                        <li><a class="active" href="listBrand.jsp">Viet Nam</a></li>
                   
                </ul>
            </div>
            
            
            <div class="app">
                <a class="a-menu"  href="admin_site.jsp">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/2.svg" alt="alt"/>
                    Apps
                </a>                
                <ul>
                    <li>
                        <a class="active" href="wallet_admin.jsp">Wallet</a>
                    </li>
                    <li>
                        <a class="active" href="mailBox.jsp">Mail Box</a>
                    </li>
                    <li>
                        <a class="active" href="chat.jsp">Chat</a>
                    </li>
                  
                </ul>
            </div>
            <div class="table">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/11.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Table</a>
            </div>
            <div class="chart">
                <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/13.svg" alt="alt"/>
                <a class="active" href="admin_site.jsp">Chart</a>
            </div>
             <div class="pages">
                <div class="pa-menu">
                    <img src="https://demo.dashboardpack.com/sales-html/img/menu-icon/16.svg" alt="alt"/>
                    <a class="active" href="admin_site.jsp">Pages</a>
                </div>                
                <ul>
                    <li><a class="active" href="admin_site.jsp">Login</a></li>
                    <li><a class="active" href="admin_site.jsp">Register</a></li>
                    <li><a class="active" href="admin_site.jsp">Forgot PassWord</a></li>
               
                </ul>
            </div>
        </div>
        <div class="main">
            <div class="header">
                
                <div class="form">
                  <!--<img src="https://icons-for-free.com/iconfiles/png/512/explore+find+magnifier+search+icon-1320185008030646474.png" height="25px" alt="alt"/>-->
                  <input type="text" id="search" class="form-control form-input" placeholder="Search anything...">
                </div>
                <div class="noti-icon" id="test1">
                    <div class="noti">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/bell.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="mail">
                        <a href="#">
                            <img src="https://demo.dashboardpack.com/sales-html/img/icon/msg.svg" height="20px" alt="alt"/>
                        </a>
                    </div> 
                    <div class="profile">
                        <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" height="45px" alt="alt"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="report">
                    <div class="gop">
                        <div class="link">
                        <h1 >Products</h1>
                        
                        <p> Tất cả sản phẩm</p>
                    </div>
                    <div>
                        <!--Button thêm product-->
                        <button onclick="addProduct()" id="test" class="addprd" type="button">Add Product</button>
                    </div>
                        
                        
                        <div id="overlay1" class="overlay1"></div>

                <div id="formContainer1">
                    <div class="title-product1">
                        <h2> Add Product</h2>
                        <span id="closeButton1" onclick="hideForm1()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user1">
                      <div class="ctr-input1">
                          <div class="form-left1">
                              <label for="file-upload1">
                                  <img src="">
                                  <i class="fas fa-camera"></i>
                              </label>
                            <input type="file" id="file-upload1"  />
                            <br>
                            <br>
                            <br>
                             <label for="cateID1">Cate Id</label>

                            <input type="text" id="cateID1" name="name"><br><br>

<%--                            <label for="brandID1">Brand Id</label>--%>

<%--                            <input type="text" id="brandID1" name="number"><br><br> --%>

                             <label for="color1">Color</label>

                            <input type="text" id="color1" name="date"><br><br> 
           
                           </div>
                       <div class="form-right1">
                            <label for="name1">Product Name</label>

                            <input type="text" id="name1" name="date"><br><br> 
                            <label for="price1">Price</label>

                            <input type="text" id="price1" name="name"><br><br>
                           
                            <label for="des1">Description</label>

                            <input type="text" id="des1" name="text1"><br><br>

                            <label for="brand1">Brand</label>
                            <br>

                           <select id="brand1" name="phone">
                               <option value="Việt Nam">Việt Nam</option>
                               <option value="Mỹ">Mỹ</option>
                               <option value="Rica">Rica</option>
                               <option value="Úc">Úc</option>
                           </select><br><br>
                            
                            <label for="method1">Transport Method</label>

                            <input type="text" id="method1" name="phone"><br><br>
                            
                             <label for="status1">Status</label>
                             <br>

                           <select id="status1" name="phone">
                               <option value="Active">Active</option>
                               <option value="Inactive">Inactive</option>
                           </select><br><br>
                            
                            <label for="quantity1">Quantity</label>

                            <input type="text" id="quantity1" name="name"><br><br>
                      </div>
                      </div>
                      <div class="ctr-button1">
                          <button onclick="callCreateAPI()" type="button">Add</button>
                          <input onclick="hideForm1()" type="submit" value="Cancel">
                      </div>

                  </form>
                </div>
                    </div>
                   </div>
                </div>

                <div class="container-one">
                <table class="highlight">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ProductName</th>
                            <th>Color</th>
                            <th>Price</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>TransportMethod</th>
                            <th>Status</th>
                            <th>quantity</th>
                           
                        </tr>
                    </thead>
                    <tbody id="product-list" class="list"></tbody>
                </table>
                <div id="overlay" class="overlay"></div>

                <div id="formContainer">
                    <div class="title-product">
                        <h2>Product Detail</h2>
                        <span id="closeButton" onclick="hideForm()">&#x2716;</span>

                    </div>
                    <hr>
                  <form class="manager-user">
                      <div class="ctr-input">
                          <div class="form-left">
                            
                              <label for="file-upload">
                                  <img id="image" src="">
                                  <i class="fas fa-camera"></i>
                              </label>
                            <input type="file" id="file-upload" required />
                            <br>
                            <br>
                            <br>
                           <label for="id">ID</label>

                            <input type="text" id="id" name="id"><br><br>

<%--                              <label for="cateID">Cate Id</label>--%>

<%--                              <input type="text" id="cateID" name="name"><br><br>--%>

<%--                              <label for="brandID">Brand Id</label>--%>

<%--                              <input type="text" id="brandID" name="number"><br><br>--%>

                             <label for="color">Color</label>

                            <input type="text" id="color" name="date"><br><br>

                              <label for="name">Product Name</label>

                              <input type="text" id="name" name="date"><br><br>
                              <label for="price">Price</label>

                              <input type="text" id="price" name="name"><br><br>
                           </div>
                       <div class="form-right">

                            <label for="des">Description</label>

                            <input type="text" id="des" name="email"><br><br>

                           <label for="brand">Brand</label>
                           <br>
                           <select id="brand" name="phone">
                               <option value="Việt Nam">Việt Nam</option>
                               <option value="Mỹ">Mỹ</option>
                               <option value="Rica">Rica</option>
                               <option value="Úc">Úc</option>
                           </select><br><br>

                            <label for="method">Transport Method</label>

                            <input type="text" id="method" name="phone"><br><br>
                            
                           <label for="status">Status</label>
                           <br>
                           <select id="status" name="phone">
                               <option value="Active">Active</option>
                               <option value="Inactive">Inactive</option>
                           </select><br><br>

                            <label for="quantity">Quantity</label>

                            <input type="text" id="quantity" name="name"><br><br>

                      </div>
                      </div>
                      <div class="ctr-button">
                          <button onclick="callUpdateAPI()" type="button">Update</button>
                          <button onclick="callDeleteAPI()" type="button">Delete</button>
                          <button onclick="hideForm()" type="submit" >Cancel</button>
                      </div>


                  </form>
                </div>
                    
             
            </div>
          <script src="./javascript_admin/showProductsInfo.js"></script>
           <script src="./javascript_admin/addProduct.js"></script>
             <script src="./javascript_admin/addCate.js"></script>
           <script src="./javascript_admin/dynamicPagination.js"></script>
        </div>
               
    </body>
</html>
<script>
//    var elements = document.querySelectorAll("#myElement");
//
//    elements.forEach(function(element){
//        element.addEventListener("mouseover", function(event) {
//            var active = document.getElementById('submenu');
//            active.style.display="block";
//          // Xử lý khi hover vào thẻ
//          console.log("Đã hover vào thẻ");
//        });
//
//    });
//    
    var checkedd =document.querySelectorAll('.checker');
    
    
    
              checkedd.forEach(function(check){
        check.addEventListener("mouseover", function(event) {
//            var active = document.getElementById('submenu');
            check.children[1].style.display="block";
          // Xử lý khi hover vào thẻ
          console.log("Đã hover vào thẻ");
        });

    });

    
            checkedd.forEach(function(check){
        check.addEventListener("mouseout", function(event) {
     check.children[1].style.display="none";
  // Xử lý khi di chuột ra khỏi thẻ
  console.log("Đã di chuột ra khỏi thẻ");
});

    });

    
//        elements.forEach(function(element){
//        element.addEventListener("mouseout", function(event) {
//     var active = document.getElementById('submenu');
////    active.style.display="none";
//  // Xử lý khi di chuột ra khỏi thẻ
//  console.log("Đã di chuột ra khỏi thẻ");
//});
//
//    });




</script>
<script>
    const urlParams = new URLSearchParams(window.location.search);
    const cateId = urlParams.get("cateId");
    let url = "https://localhost:8080/FreshMarket/api/products/get-by-cate/?id=" + cateId;
          fetch(url)
        .then(response => response.json())
        .then(data => {
            let productList = document.getElementById("product-list");

            data.forEach(product => {
                let id = product.id;
                let categoryId = product.cateId;
                let brandId = product.brandId;
                let productName = product.productName;
                let color = product.color;
                let price = product.price;
                let brand = product.brand;
                let description = product.description;
                let transportMethod = product.transportMethod;
                let status = product.Status;
                let quantity = product.quantity;
                let image = product.image;

                let row = document.createElement("tr");
                row.classList.add('item');
                row.onclick = function () {
                    showProductsInfo(this);
                };

                let idCell = document.createElement("td");
                idCell.innerText = id;
                row.appendChild(idCell);

                let categoryIdCell = document.createElement("td");
                categoryIdCell.innerText = categoryId;
                row.appendChild(categoryIdCell);
                categoryIdCell.style.display = "none";

                let brandIdCell = document.createElement("td");
                brandIdCell.innerText = brandId;
                row.appendChild(brandIdCell);
                brandIdCell.style.display = "none";

                let productNameCell = document.createElement("td");
                productNameCell.innerText = productName;
                row.appendChild(productNameCell);

                let colorCell = document.createElement("td");
                colorCell.innerText = color;
                row.appendChild(colorCell);

                let priceCell = document.createElement("td");
                priceCell.innerText = price;
                row.appendChild(priceCell);

                let brandCell = document.createElement("td");
                brandCell.innerText = brand;
                row.appendChild(brandCell);

                let descriptionCell = document.createElement("td");
                descriptionCell.classList.add('desss');
                descriptionCell.innerText = description;            
                row.appendChild(descriptionCell);

                let transportMethodCell = document.createElement("td");
                transportMethodCell.innerText = transportMethod;
                row.appendChild(transportMethodCell);

                let statusCell = document.createElement("td");
                statusCell.innerText = status ? "Active" : "Inactive";
                row.appendChild(statusCell);

                let quantityCell = document.createElement("td");
                quantityCell.innerText = quantity;
                row.appendChild(quantityCell);

                let imageCell = document.createElement("td");
                imageCell.innerText = image;
                row.appendChild(imageCell);
                imageCell.style.display = "none";

                productList.appendChild(row);
            });
        })
        .catch(error => {
            console.error(error);
        });

    async function DeleteCate(){
        const url = new URL(window.location.href);
        const searchParams = new URLSearchParams(url.search);
        let id =  searchParams.get("cateId");
        const response = await fetch('https://localhost:8080/FreshMarket/api/categories/delete/?id=' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await response.json();
        if (res == "1") {
            alert("Delete successful");
            location.reload();
        } else {
            alert("Delete failed");
        }
    }

    async function callDeleteAPI(event) {

        let id = document.getElementById("id").value;

        const response = await fetch('https://localhost:8080/FreshMarket/api/products/delete/?id=' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const res = await response.json();
        if (res == "1") {
            alert("Delete successful");
            location.reload();
        } else {
            alert("Delete failed");
        }
    }

    async function callUpdateAPI(event) {
        var id=document.getElementById('id').value;
        var color=document.getElementById('color').value;
        var name=document.getElementById('name').value;
        var price=document.getElementById('price').value;
        var des=document.getElementById('des').value;
        var brand=document.getElementById('brand').value;
        var method=document.getElementById('method').value;
        var status=document.getElementById('status').value;
        var quantity=document.getElementById('quantity').value;
        var image;
        if (document.getElementById('image').src != null){
            image=document.getElementById('image').src;
        }
        else{
            image = "default";
        }
        var boolSatus = false;
        if(status.toString() === "Active"){
            boolSatus = true;
        }
        const updatedData = {
            id: id,
            productName: name,
            price: price,
            description: des,
            brand: brand,
            transportMethod: method,
            quantity: quantity,
            image : image,
            Status : boolSatus,
            color : color
        };
        const response = await fetch('https://localhost:8080/FreshMarket/api/products/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(updatedData)
        });
        const res = await response.json();
        if (res == "1") {
            alert("Update successful");
            location.reload();
        } else {
            alert("Update failed");
        }
    }
    async  function callCreateAPI(){
        var cateID  =document.getElementById('cateID1').value;
        var color=document.getElementById('color1').value;
        var name=document.getElementById('name1').value;
        var price=document.getElementById('price1').value;
        var des=document.getElementById('des1').value;
        var brand=document.getElementById('brand1').value;
        var method=document.getElementById('method1').value;
        var status=document.getElementById('status1').value;
        var quantity=document.getElementById('quantity1').value;
        var image;
        if (document.getElementById('image').src != null){
            image=document.getElementById('image').src;
        }
        else{
            image = "default";
        }
        var boolSatus = false;
        if(status.toString() === "Active"){
            boolSatus = true;
        }
        const data = {
            cateId: cateID,
            productName: name,
            price: price,
            description: des,
            brand: brand,
            transportMethod: method,
            quantity: quantity,
            image : image,
            Status : boolSatus,
            color : color
        };
        const response = await fetch('https://localhost:8080/FreshMarket/api/products/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(data)
        });
        const res = await response.json();
        if (res == "1") {
            alert("Create successful!!");
            location.reload();
        } else {
            alert("Create failed!!");
        }
    }
    async  function CreateCateAPI(){
        var cateName  =document.getElementById('cateName').value;

        const data = {
            cateName: cateName,
        };
        const response = await fetch('https://localhost:8080/FreshMarket/api/categories/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(data)
        });
        const res = await response.json();
        if (res == "1") {
            alert("Create successful!!");
            location.reload();
        } else {
            alert("Create failed!!");
        }
    }
</script>
